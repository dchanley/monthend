﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Microsoft.Office.Interop.Excel;
using _Excel = Microsoft.Office.Interop.Excel;
using System.Windows;
using ARCHExport;
using Telerik.WinControls.UI;

namespace MonthEndSoftware
{
    public partial class MonthEnd : Form
    {
        string path = "server=198.143.98.122;database=veritas;User Id=sa;Password=NCC1701E";
        public MonthEnd()
        {
            InitializeComponent();
            DateTime day = DateTime.Today;
            day = new DateTime(day.Year, day.Month, 1);
            day = day.AddDays(-1);
            dtpEndDate.Value = day;
            day = new DateTime(day.Year, day.Month, 1);
            dtpStartDate.Value = day;
            creatFilePath();
            setSavedArch();
            setSaveAmTrust();
            setSaveOldRepublic();
            setSaveFortega();

        }
        //Removed Feature
        private void BtnExcel_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Hello");
        }

        //Misc Methods
        public void clearLables()
        {
            lblContract.Text = "Paid Contract:";
            lblDealer.Text = "Dealer: ";
            lbCancel.Text = "Cancel Contracts:";
            lblOther.Text = "Other:";
            lblClaim.Text = "Claims:";
        }
        public string setPath()
        {
            string filePath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            return filePath;

        }
        public string setMonth()
        {
            DateTime day = dtpEndDate.Value;
            string month = day.ToString("MMMM");
            return month;
        }        
        public string setMonthNo()
        {
            DateTime day = dtpEndDate.Value;
            string month = day.ToString("MM");
            return month;
        }
        public string setYear()
        {
            DateTime day = dtpEndDate.Value;
            string sYear = "";
            sYear = day.ToString("yyyy");
            return sYear;
        }
        public void creatFilePath()
        {
            string filePath = setPath() + "/MonthEnd/" + setMonth();
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            filePath = setPath() + "/MonthEnd/" + setMonth() + "/AmTrust";
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            filePath = setPath() + "/MonthEnd/" + setMonth() + "/Arch";
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            filePath = setPath() + "/MonthEnd/" + setMonth() + "/OldRep";
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            filePath = setPath() + "/MonthEnd/" + setMonth() + "/Fortega";
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            filePath = setPath() + "/MonthEnd/SuccessfullyTransmittedFiles";
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
        }
        private void ChkInception_CheckedChanged(object sender, EventArgs e)
        {
            if (chkInception.Checked)
            {
                dtpStartDate.Value = Convert.ToDateTime("01/01/1900");
                dtpStartDate.Enabled = false;
            }
            if (!chkInception.Checked)
            {
                DateTime date = dtpEndDate.Value;
                date = new DateTime(date.Year, date.Month, 1);
                dtpStartDate.Value = date;
                dtpStartDate.Enabled = true;

            }
        }
        private void chkOnlyAdded_CheckedChanged(object sender, EventArgs e)
        {
            if (chkOnlyAdded.Checked)
            {
                dtpStartDate.Value = Convert.ToDateTime("01/01/1900");
                dtpEndDate.Value = Convert.ToDateTime("01/01/1900");
                dtpStartDate.Enabled = false;
                dtpEndDate.Enabled = false;
            }
            if (!chkOnlyAdded.Checked)
            {
                DateTime day = DateTime.Today;
                day = new DateTime(day.Year, day.Month, 1);
                day = day.AddDays(-1);
                dtpEndDate.Value = day;
                day = new DateTime(day.Year, day.Month, 1);
                dtpStartDate.Value = day;
                dtpStartDate.Enabled = true;
                dtpEndDate.Enabled = true;
            }
        }
        private void BtnClear_Click(object sender, EventArgs e)
        {
            clearLables();
        }


        //User Input Functions
            //AmTrust
        public string setExcludeClaimstAmTrust()
        {
            string excludeAmTrustclaims = rtbRemoveClaimsAmTrust.Text.TrimEnd('\r', '\n');
            excludeAmTrustclaims = "'" + excludeAmTrustclaims.Replace("\n", "','") + "'";
            return excludeAmTrustclaims;
        }
        public string setAddClaimsAmTrust()
        {
            string addClaims = rtbAddClaimsAmTrust.Text.TrimEnd('\r', '\n');
            addClaims = "'" + addClaims.Replace("\n", "','") + "'";
            return addClaims;
        }
        public string setExcludeContractAmTrust()
        {
            string excludeAmTrustContracts = rtbRemoveContractAmTrust.Text.TrimEnd('\r', '\n');
            excludeAmTrustContracts = "'" + excludeAmTrustContracts.Replace("\n", "','") + "'";
            return excludeAmTrustContracts;
        }
        public string setAddContractAmTrust()
        {
            string addContracts = rtbContractsPaidAddAmTrust.Text.TrimEnd('\r', '\n');
            addContracts = "'" + addContracts.Replace("\n", "','") + "'";
            return addContracts;
        }
        public string setAddContractCancelAmTrust()
        {
            string addContracts = rtbCancelledContractsAddAmTrust.Text.TrimEnd('\r', '\n');
            addContracts = "'" + addContracts.Replace("\n", "','") + "'";
            return addContracts;
        }

            //Arch
        public string setExcludeClaimsArch()
        {
            string excludeAmTrustContracts = rtbRemoveClaimsArch.Text.TrimEnd('\r', '\n');
            excludeAmTrustContracts = "'" + excludeAmTrustContracts.Replace("\n", "','") + "'";
            return excludeAmTrustContracts;
        }
        public string setAddClaimsArch()
        {
            string addContracts = rtbAddClaimsArch.Text.TrimEnd('\r', '\n');
            addContracts = "'" + addContracts.Replace("\n", "','") + "'";
            return addContracts;
        }
        public string setExcludeContractArch()
        {
            string excludeAmTrustContracts = rtbRemoveContractArch.Text.TrimEnd('\r', '\n');
            excludeAmTrustContracts = "'" + excludeAmTrustContracts.Replace("\n", "','") + "'";
            return excludeAmTrustContracts;
        }
        public string setAddContractArch()
        {
            string addContracts = rtbPaidContractsArch.Text.TrimEnd('\r', '\n');
            addContracts = "'" + addContracts.Replace("\n", "','") + "'";
            return addContracts;
        }
        public string setAddCancelledContractArch()
        {
            string addContracts = rtbCancelledContractsArch.Text.TrimEnd('\r', '\n');
            addContracts = "'" + addContracts.Replace("\n", "','") + "'";
            return addContracts;
        }       

            //Primary User Input Method
        public void UserInputContract()
        {
            string excludeAmTrust = "";
            string includeAmTrust = "";
            string excludeArch = "";
            string includeArch = "";
            if (rtbRemoveContractAmTrust.Text.Length > 0)
            {
                excludeAmTrust = setExcludeContractAmTrust();
                string[] Array = excludeAmTrust.Split(',');
                int insuranceID = 1;
                string includeExclude = "E";
                InsertUserInputContract(Array, insuranceID, includeExclude);
            }
            if (rtbContractsPaidAddAmTrust.Text.Length > 0)
            {
                includeAmTrust = setAddContractAmTrust();
                string[] Array = includeAmTrust.Split(',');
                int insuranceID = 1;
                string includeExclude = "I";
                InsertUserInputContract(Array, insuranceID, includeExclude);
            }
            if (rtbCancelledContractsAddAmTrust.Text.Length > 0)
            {
                includeAmTrust = setAddCancelledContractArch();
                string[] Array = includeAmTrust.Split(',');
                int insuranceID = 1;
                string includeExclude = "I";
                InsertUserInputContract(Array, insuranceID, includeExclude);
            }
            if (rtbRemoveContractArch.Text.Length > 0)
            {
                excludeArch = setExcludeContractArch();
                string[] Array = excludeArch.Split(',');
                int insuranceID = 2;
                string includeExclude = "I";
                InsertUserInputContract(Array, insuranceID, includeExclude);
            }
            if (rtbPaidContractsArch.Text.Length > 0)
            {
                includeArch = setAddContractArch();
                string[] Array = includeArch.Split(',');
                int insuranceID = 2;
                string includeExclude = "I";
                InsertUserInputContract(Array, insuranceID, includeExclude);
            }
            if (rtbCancelledContractsArch.Text.Length > 0)
            {
                includeArch = setAddCancelledContractArch() ;
                string[] Array = includeArch.Split(',');
                int insuranceID = 2;
                string includeExclude = "I";
                InsertUserInputContract(Array, insuranceID, includeExclude);
            }

            if (rtbRemoveClaimsAmTrust.Text.Length > 0)
            {
                excludeAmTrust = setExcludeClaimstAmTrust();
                string[] Array = excludeAmTrust.Split(',');
                int insuranceID = 1;
                string includeExclude = "E";
                InsertUserInputClaims(Array, insuranceID, includeExclude);
            }
            if (rtbAddClaimsAmTrust.Text.Length > 0)
            {
                includeAmTrust = setAddClaimsAmTrust();
                string[] Array = includeAmTrust.Split(',');
                int insuranceID = 1;
                string includeExclude = "I";
                InsertUserInputClaims(Array, insuranceID, includeExclude);
            }
            if (rtbRemoveClaimsArch.Text.Length > 0)
            {
                excludeArch = setExcludeClaimsArch();
                string[] Array = excludeArch.Split(',');
                int insuranceID = 2;
                string includeExclude = "I";
                InsertUserInputClaims(Array, insuranceID, includeExclude);
            }
            if (rtbAddClaimsArch.Text.Length > 0)
            {
                includeArch = setAddClaimsArch();
                string[] Array = includeArch.Split(',');
                int insuranceID = 2;
                string includeExclude = "I";
                InsertUserInputClaims(Array, insuranceID, includeExclude);
            }

        }

            //Supporting User Input Method
        public void InsertUserInputContract(string[] array, int insurID, string includeExclude)
        {
            CSharpDBO.CSharpDBO insertUserInputDBO = new CSharpDBO.CSharpDBO();
            insertUserInputDBO.dboOpen(path);
            int counter = 0;
            string sql = "";
            while (counter < array.Length)
            {
                sql = "select * from VeritasReports.dbo.MonthEndContractIncludeExclude where contractNo = " + array[counter] + " and InsCarrierID = " + insurID + "" +
                    " and IncludeExclude = '" + includeExclude + "' and cycleDate = '" + dtpEndDate.Value + "'";

                if (!insertUserInputDBO.hasRows())
                {
                    sql = "insert into VeritasReports.dbo.MonthEndContractIncludeExclude(ContractNo,InsCarrierID,IncludeExclude,cycleDate)" +
                        "values(" + array[counter] + "," + insurID + ",'" + includeExclude + "', '" + dtpEndDate.Value + "')";
                    insertUserInputDBO.dboAlterDBOUnsafe(sql);
                }




                counter++;
            }
        }
        public void InsertUserInputClaims(string[] array, int insurID, string includeExclude)
        {
            CSharpDBO.CSharpDBO insertUserInputDBO = new CSharpDBO.CSharpDBO();
            insertUserInputDBO.dboOpen(path);
            int counter = 0;
            string sql = "";
            while (counter < array.Length)
            {
                sql = "select * from VeritasReports.dbo.MonthEndClaimsIncludeExclude where ClaimNo = " + array[counter] + " and InsCarrierID = " + insurID + "" +
                    " and IncludeExclude = '" + includeExclude + "' and cycleDate = '" + dtpEndDate.Value + "'";

                if (!insertUserInputDBO.hasRows())
                {
                    sql = "insert into VeritasReports.dbo.MonthEndClaimsIncludeExclude(ClaimNo,InsCarrierID,IncludeExclude,cycleDate)" +
                        "values(" + array[counter] + "," + insurID + ",'" + includeExclude + "', '" + dtpEndDate.Value + "')";
                    insertUserInputDBO.dboAlterDBOUnsafe(sql);
                }

                counter++;
            }
        }
        


        //Upload Transmitted Files
            //Button Contral
        private void btnUploadTransmittedFiles_Click(object sender, EventArgs e)
        {
            clearLables();
            btnClear.Enabled = false;
            btnExcel.Enabled = false;
            btnTransmit.Enabled = false;
            btnUploadTransmittedFiles.Enabled = false;
            bool selectedDate = false;
            DateTime transmiteDate = DateTime.Today;
            while (!selectedDate)
            {
                using (Form2 form2 = new Form2())
                {
                    if (form2.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        transmiteDate = form2.selectedDate;
                        transmiteDate = Convert.ToDateTime(transmiteDate.ToString("MM/dd/yyyy"));
                        selectedDate = true;
                    }
                }
            }

            if (chkAMT.Checked && chkArch.Checked)
            {
                readInAmTrust(transmiteDate);
                readInArch(transmiteDate);
                MessageBox.Show("Done");
            }
            else if (chkAMT.Checked)
            {
                readInAmTrust(transmiteDate);
                MessageBox.Show("Done");
            }
            else if (chkArch.Checked)
            {
                readInArch(transmiteDate);
                MessageBox.Show("Done");
            }
            else
            {
                MessageBox.Show("Please select either AmTrust or Arch");
            }
            btnClear.Enabled = true;
            btnExcel.Enabled = true;
            btnTransmit.Enabled = true;
            btnUploadTransmittedFiles.Enabled = true;
        }
            //AmTrust
        public void readInAmTrust(DateTime transmiteDate)
        {

            int iCounter = 0;
            string sFilePath = setPath() + @"\MonthEnd\SuccessfullyTransmittedFiles";
            string[] filename = Directory.GetFiles(sFilePath);
            string sReadLine = "";
            CSharpDBO.CSharpDBO dBO = new CSharpDBO.CSharpDBO();
            dBO.dboOpen(path);
            while (iCounter < filename.Length)
            {
                if (filename[iCounter].ToLower().Contains("sales") && filename[iCounter].ToLower().Contains("veritas") && !filename[iCounter].ToLower().Contains("control"))
                {
                    StreamReader reader = new StreamReader(filename[iCounter]);
                    while (!reader.EndOfStream)
                    {
                        sReadLine = reader.ReadLine();
                        string[] AmTrustLine = sReadLine.Split('|');
                        if (!AmTrustLine[5].ToLower().Contains("dealer"))
                        {
                            if (AmTrustLine[14] == "A")
                            {
                                string sSQL = "update contract " +
                                    "set INSSaleTransmissionDate = '" + transmiteDate + "' " +
                                    "where contractNo = '" + AmTrustLine[5] + "'";
                                dBO.dboAlterDBOUnsafe(sSQL);
                            }
                            else
                            {
                                string sSQL = "update contract " +
                                    "set INSCancelTransmissionDate = '" + transmiteDate + "' " +
                                    "where contractNo = '" + AmTrustLine[5] + "'";
                                dBO.dboAlterDBOUnsafe(sSQL);

                            }


                        }

                    }

                }
                iCounter++;
            }
            iCounter = 0;
            while (iCounter < filename.Length)
            {
                if (filename[iCounter].ToLower().Contains("claims") && filename[iCounter].ToLower().Contains("veritas") && !filename[iCounter].ToLower().Contains("control"))
                {
                    StreamReader reader = new StreamReader(filename[iCounter]);
                    while (!reader.EndOfStream)
                    {
                        string sUpdateSQL = "";
                        sReadLine = reader.ReadLine();
                        string claimDetailID = "";
                        string[] AmTrustLine = sReadLine.Split('|');
                        if (AmTrustLine.Length > 5)
                        {
                            string sSQL = "select ClaimDetailID from ClaimDetail cd " +
                            "left join Claim cl on cl.ClaimID = cd.ClaimID " +
                            "left join contract c on c.ContractID = cl.ContractID " +
                            "where c.InsCarrierID = 1 " +
                            "and cd.RateTypeID = 1 " +
                            "and cl.claimNo = '" + AmTrustLine[4] + "'";
                            System.Data.DataTable dt = dBO.dboInportInformation(sSQL);
                            foreach (DataRow dr in dt.Rows)
                            {
                                claimDetailID = Convert.ToString(dr["ClaimDetailID"]);
                                sUpdateSQL = "update ClaimDetail " +
                                    "set INSTransmissionDate = '" + transmiteDate + "' " +
                                    "where ClaimDetailID = " + claimDetailID + "";
                                dBO.dboAlterDBOUnsafe(sUpdateSQL);
                            }
                            sSQL = "select * from MiscDB.dbo.AmTrustClaimCounter where ClaimNo = '" + AmTrustLine[4] + "'";
                            dt = dBO.dboInportInformation(sSQL);
                            if (dBO.hasRows())
                            {
                                int iCounterTracker = 0;
                                foreach (DataRow dr in dt.Rows)
                                {
                                    iCounterTracker = Convert.ToInt32(dr["Counter"]) + 1;
                                }
                                sSQL = "update MiscDB.dbo.AmTrustClaimCounter set Counter = " + iCounterTracker + " where ClaimNo = '" + AmTrustLine[4] + "'";
                                dBO.dboAlterDBOUnsafe(sSQL);
                            }
                            else
                            {
                                sSQL = "insert into MiscDB.dbo.AmTrustClaimCounter(ClaimNo,Counter) values('" + AmTrustLine[4] + "',0)";
                                dBO.dboAlterDBOUnsafe(sSQL);
                            }
                        }
                    }
                }
                iCounter++;
            }
            dBO.dboClose();
        }
            //Arch
        public void readInArch(DateTime transmiteDate)
        {
            int iCounter = 0;
            string sFilePath = setPath() + @"\MonthEnd\SuccessfullyTransmittedFiles";
            string[] filename = Directory.GetFiles(sFilePath);
            string sReadLine = "";
            CSharpDBO.CSharpDBO dBO = new CSharpDBO.CSharpDBO();
            dBO.dboOpen(path);
            while (iCounter < filename.Length)
            {
                if (filename[iCounter].ToLower().Contains("contract"))
                {
                    StreamReader reader = new StreamReader(filename[iCounter]);
                    while (!reader.EndOfStream)
                    {
                        sReadLine = reader.ReadLine();
                        string[] AmTrustLine = sReadLine.Split('\t');
                        if (!AmTrustLine[2].ToLower().Contains("contract number"))
                        {
                            if (AmTrustLine[62].Length < 1)
                            {
                                string sSQL = "update contract " +
                                    "set INSSaleTransmissionDate = '" + transmiteDate + "' " +
                                    "where contractNo = '" + AmTrustLine[5] + "'";
                                dBO.dboAlterDBOUnsafe(sSQL);
                            }
                            else
                            {
                                string sSQL = "update contract " +
                                    "set INSCancelTransmissionDate = '" + transmiteDate + "' " +
                                    "where contractNo = '" + AmTrustLine[5] + "'";
                                dBO.dboAlterDBOUnsafe(sSQL);

                            }

                        }

                    }

                }
                iCounter++;
            }
            iCounter = 0;
            while (iCounter < filename.Length)
            {
                if (filename[iCounter].ToLower().Contains("claims") && !filename[iCounter].ToLower().Contains("veritas"))
                {
                    StreamReader reader = new StreamReader(filename[iCounter]);
                    while (!reader.EndOfStream)
                    {
                        string sUpdateSQL = "";
                        sReadLine = reader.ReadLine();
                        string claimDetailID = "";
                        string[] AmTrustLine = sReadLine.Split('\t');

                        string sSQL = "select ClaimDetailID from ClaimDetail cd " +
                            "left join Claim cl on cl.ClaimID = cd.ClaimID " +
                            "left join contract c on c.ContractID = cl.ContractID " +
                            "where c.InsCarrierID = 1 " +
                            "and cd.RateTypeID = 1 " +
                            "and c.contractNo = '" + AmTrustLine[2] + "'";
                        System.Data.DataTable dt = dBO.dboInportInformation(sSQL);
                        foreach (DataRow dr in dt.Rows)
                        {
                            claimDetailID = Convert.ToString(dr["ClaimDetailID"]);
                            sUpdateSQL = "update ClaimDetail " +
                                "set INSTransmissionDate = '" + transmiteDate + "' " +
                                "where ClaimDetailID = " + claimDetailID + "";
                            dBO.dboAlterDBOUnsafe(sUpdateSQL);
                        }
                    }
                }
                iCounter++;
            }
            dBO.dboClose();
        }
            //OldRep
        public void readInOldRep(DateTime transmiteDate)
        {

        }
            //Fortega
        public void readInFortega(DateTime transmiteDate)
        {

        }

        //Set Saved contracts into RichTextBoxes
            //Arch
        public void setSavedArch()
        {
            CSharpDBO.CSharpDBO dBO = new CSharpDBO.CSharpDBO();
            dBO.dboOpen(path);
            string sSQL = "select distinct * from ArchContractAdd where AddOrRemove = 'A' and Status = 'Paid'";
            System.Data.DataTable data = dBO.dboInportInformation(sSQL);
            foreach (DataRow dr in data.Rows)
            {
                rtbPaidContractsArch.AppendText(dr["ContractNo"].ToString() + "\n");
            }
            sSQL = "select distinct * from ArchContractAdd where AddOrRemove = 'A' and Status = 'Cancelled'";
            data = dBO.dboInportInformation(sSQL);
            foreach (DataRow dr in data.Rows)
            {
                rtbCancelledContractsArch.AppendText(dr["ContractNo"].ToString() + "\n");
            }
            sSQL = "select distinct * from ArchContractAdd where AddOrRemove = 'R'";
            data = dBO.dboInportInformation(sSQL);
            foreach (DataRow dr in data.Rows)
            {
                rtbRemoveContractArch.AppendText(dr["ContractNo"].ToString() + "\n");
            }

            sSQL = "select distinct * from ArchClaimAdd where AddOrRemove = 'A'";
            data = dBO.dboInportInformation(sSQL);
            foreach (DataRow dr in data.Rows)
            {
                rtbAddClaimsArch.AppendText(dr["ClaimNo"].ToString() + "\n");
            }
            sSQL = "select distinct * from ArchClaimAdd where AddOrRemove = 'R'";
            data = dBO.dboInportInformation(sSQL);
            foreach (DataRow dr in data.Rows)
            {
                rtbRemoveClaimsArch.AppendText(dr["ClaimNo"].ToString() + "\n");
            }
            dBO.dboClose();

        }

            //AmTrust
        public void setSaveAmTrust()
        {
            CSharpDBO.CSharpDBO dBO = new CSharpDBO.CSharpDBO();
            dBO.dboOpen(path);
            string sSQL = "select distinct * from AmTrustContractAdd where AddOrRemove = 'A' and Status = 'Paid'";
            System.Data.DataTable data = dBO.dboInportInformation(sSQL);
            foreach (DataRow dr in data.Rows)
            {
                rtbContractsPaidAddAmTrust.AppendText(dr["ContractNo"].ToString() + "\n");
            }
            sSQL = "select distinct * from AmTrustContractAdd where AddOrRemove = 'A' and Status = 'Cancelled'";
            data = dBO.dboInportInformation(sSQL);
            foreach (DataRow dr in data.Rows)
            {
                rtbCancelledContractsAddAmTrust.AppendText(dr["ContractNo"].ToString() + "\n");
            }
            sSQL = "select distinct * from AmTrustContractAdd where AddOrRemove = 'R'";
            data = dBO.dboInportInformation(sSQL);
            foreach (DataRow dr in data.Rows)
            {
                rtbRemoveContractAmTrust.AppendText(dr["ContractNo"].ToString() + "\n");
            }

            sSQL = "select distinct * from AmTrustClaimAdd where AddOrRemove = 'A'";
            data = dBO.dboInportInformation(sSQL);
            foreach (DataRow dr in data.Rows)
            {
                rtbAddClaimsAmTrust.AppendText(dr["ClaimNo"].ToString() + "\n");
            }
            sSQL = "select distinct * from AmTrustClaimAdd where AddOrRemove = 'R'";
            data = dBO.dboInportInformation(sSQL);
            foreach (DataRow dr in data.Rows)
            {
                rtbRemoveClaimsAmTrust.AppendText(dr["ClaimNo"].ToString() + "\n");
            }
            dBO.dboClose();
        }

            //Fortega
        public void setSaveFortega()
        {
            CSharpDBO.CSharpDBO dBO = new CSharpDBO.CSharpDBO();
            dBO.dboOpen(path);
            string sSQL = "select distinct * from fortegacontractadd where AddOrRemove = 'A' and Status = 'Paid'";
            System.Data.DataTable data = dBO.dboInportInformation(sSQL);
            foreach (DataRow dr in data.Rows)
            {
                rtbPaidContractsFortega.AppendText(dr["ContractNo"].ToString() + "\n");
            }
            sSQL = "select distinct * from fortegacontractadd where AddOrRemove = 'A' and Status = 'Cancelled'";
            data = dBO.dboInportInformation(sSQL);
            foreach (DataRow dr in data.Rows)
            {
                rtbCancelContractsFortega.AppendText(dr["ContractNo"].ToString() + "\n");
            }
            sSQL = "select distinct * from fortegacontractadd where AddOrRemove = 'R'";
            data = dBO.dboInportInformation(sSQL);
            foreach (DataRow dr in data.Rows)
            {
                rtbContractsRemoveFortega.AppendText(dr["ContractNo"].ToString() + "\n");
            }

            sSQL = "select distinct * from fortegaclaimadd where AddOrRemove = 'A'";
            data = dBO.dboInportInformation(sSQL);
            foreach (DataRow dr in data.Rows)
            {
                rtbClaimsAddFortega.AppendText(dr["ClaimNo"].ToString() + "\n");
            }
            sSQL = "select distinct * from fortegaclaimadd where AddOrRemove = 'R'";
            data = dBO.dboInportInformation(sSQL);
            foreach (DataRow dr in data.Rows)
            {
                rtbClaimsRemoveFortega.AppendText(dr["ClaimNo"].ToString() + "\n");
            }
            dBO.dboClose();
        }

            //OldRepublic
        public void setSaveOldRepublic()
        {
            CSharpDBO.CSharpDBO dBO = new CSharpDBO.CSharpDBO();
            dBO.dboOpen(path);
            string sSQL = "select distinct * from OldRepublicContractAdd where AddOrRemove = 'A' and Status = 'Paid'";
            System.Data.DataTable data = dBO.dboInportInformation(sSQL);
            foreach (DataRow dr in data.Rows)
            {
                rtbContractPaidAddOldRep.AppendText(dr["ContractNo"].ToString() + "\n");
            }
            sSQL = "select distinct * from OldRepublicContractAdd where AddOrRemove = 'A' and Status = 'Cancelled'";
            data = dBO.dboInportInformation(sSQL);
            foreach (DataRow dr in data.Rows)
            {
                rtbContractCancelledAddOldRep.AppendText(dr["ContractNo"].ToString() + "\n");
            }
            sSQL = "select distinct * from OldRepublicContractAdd where AddOrRemove = 'R'";
            data = dBO.dboInportInformation(sSQL);
            foreach (DataRow dr in data.Rows)
            {
                rtbContractRemoveOldRep.AppendText(dr["ContractNo"].ToString() + "\n");
            }

            sSQL = "select distinct * from OldRepublicClaimAdd where AddOrRemove = 'A'";
            data = dBO.dboInportInformation(sSQL);
            foreach (DataRow dr in data.Rows)
            {
                rtbClaimAddOldRep.AppendText(dr["ClaimNo"].ToString() + "\n");
            }
            sSQL = "select distinct * from OldRepublicClaimAdd where AddOrRemove = 'R'";
            data = dBO.dboInportInformation(sSQL);
            foreach (DataRow dr in data.Rows)
            {
                rtbClaimRemoveOldRep.AppendText(dr["ClaimNo"].ToString() + "\n");
            }
            dBO.dboClose();
        }


        //File Generation mapping
            //Transmit Button Control
        private void BtnTransmit_Click(object sender, EventArgs e)
        {
            creatFilePath();
            clearLables();


            btnClear.Enabled = false;
            btnExcel.Enabled = false;
            btnTransmit.Enabled = false;
            btnUploadTransmittedFiles.Enabled = false;

            btnClaimsResetArch.Enabled = false;
            btnClaimsSaveArch.Enabled = false;
            btnResetContractArch.Enabled = false;
            btnSaveContractsArch.Enabled = false;

            btnResetClaimsAmTrust.Enabled = false;
            btnSaveClaimsAmtrust.Enabled = false;
            btnResetContractsAmTrust.Enabled = false;
            btnSaveContractsAmtrust.Enabled = false;

            btnResetClaimsOldRep.Enabled = false;
            btnSaveClaimsOldRep.Enabled = false;
            btnSaveContractsOldRep.Enabled = false;
            btnResetContractsOldRep.Enabled = false;

            btnClaimSaveFortega.Enabled = false;
            btnContractSaveFortega.Enabled = false;
            btnResetClaimsFortega.Enabled = false;
            btnResetContractsFortega.Enabled = false;


            if (chkOldRep.Checked)
            {
                oldRepublicContract();
                oldRepublicCancel();
                oldRepublicClaimDetail();
                oldRepublicClaims();
                oldRepublicSeller();
            }
            if (chkAMT.Checked)
            {
                AmTrustContractMapping();
                AmTrustClaimsMapping();
                AmTrustDealerDump();
            }
            if (chkArch.Checked)
            {
                ArchContract();
            }
            if (chkFortega.Checked)
            {
                fortegaContract();
                fortegaCancel();
                fortegaClaimData();
                fortegaClaimDetail();
                fortegaSeller();
                fortegaReserve();
            }
            if (!chkAMT.Checked && !chkArch.Checked && !chkOldRep.Checked && !chkFortega.Checked)
            {
                MessageBox.Show("Please select either AmTrust, Arch, Old Republic, Fortega or any combination");
            }
            else
            {
                MessageBox.Show("Done");
                UserInputContract();
            }



            btnClear.Enabled = true;
            btnExcel.Enabled = true;
            btnTransmit.Enabled = true;
            btnUploadTransmittedFiles.Enabled = true;

            btnClaimsResetArch.Enabled = true;
            btnClaimsSaveArch.Enabled = true;
            btnResetContractArch.Enabled = true;
            btnSaveContractsArch.Enabled = true;

            btnResetClaimsAmTrust.Enabled = true;
            btnSaveClaimsAmtrust.Enabled = true;
            btnResetContractsAmTrust.Enabled = true;
            btnSaveContractsAmtrust.Enabled = true;

            btnResetClaimsOldRep.Enabled = true;
            btnSaveClaimsOldRep.Enabled = true;
            btnSaveContractsOldRep.Enabled = true;
            btnResetContractsOldRep.Enabled = true;

            btnClaimSaveFortega.Enabled = true;
            btnContractSaveFortega.Enabled = true;
            btnResetClaimsFortega.Enabled = true;
            btnResetContractsFortega.Enabled = true;

            lblProgressBar.Text = "";
            progressBar1.Value = 0;
        }

            //AmTrust
        public void AmTrustClaimsMapping()
        {
            CSharpDBO.CSharpDBO claim = new CSharpDBO.CSharpDBO();
            string claimHeader = "TPA|ProgramName|DealerPolicyNumber|DealerPolicyNumberSuffix|DealerClaimID|DealerClaimSequenceNumber|CustomerClaimNumber|WarrantySKU|CoverageType|CoverageSubType|ProductID|Currency|TransactionDate|DateofLoss|DateClaimisReported|DatePaid|ClaimState|ClaimCountry|ClaimStatus_Transcode|ClaimDenialReason|CauseofLoss|ClaimType|ClaimDescription|Majorcomponent|VIN|InsurerDeduction|InsurerSettlementAmount|Loanpayoffatdateofloss|Adjustedpayoff|Otherloanadjustments|NADAValue|MileageAtClaim|PrimaryInsurer|ClaimAdjuster|SKIPClaimAmount|DelinquentPaymentClaimAmount|GAPPlusClaimAmount|ContractFulfillment|PartsCost|LaborCost|DiagnosticFee|InspectionFee|ClaimFees|TotalTax|AppliedDeductible|DeductibleType|TotalPaidAmount|SalvageAmount|OutstandingClaims|RepairFacilityName|OutstandingClaims|RepairFacilityAddress|RepairFacilityCity|RepairFacilityAddress2|RepairFacilityState|RepairFacilityCountry|RepairFacilityZip|RepairFacilityPhone|RepairFacilityEmail|LaborHours|RepairTechnician|LaborRate|Parts|RepairDescription|RepairCompletionDate";
            StreamWriter streamWriterClaim = new StreamWriter(setPath() + "/MonthEnd/" + setMonth() + "/AmTrust/ " + setYear() + "-" + setMonthNo() + "_Veritas_Claims.txt");
            streamWriterClaim.WriteLine(claimHeader);
            claim.dboOpen(path);
            lblProgressBar.Text = "Executing Querry";
            lblProgressBar.Refresh();

            string sql = "select cl.claimid, c.ObligorID, ctpa.TPA as TPA, c.ProgramName as programName,c.ContractNo as dealerPolicyNumber, " +
                "'' as DealerPolicyNumberSuffix,cl.ClaimNo as DealerClaimID, '1' as DealerClaimSequenceNumber,'' as customerClaimNo, " +
                "conc.ContractCode as WarrantySKU,'VSC' as CoverageType,'' as CoverageSubType,conc.ContractCode as ProductID, " +
                "'USD' as Currency, dp.datepaid as TransactionDate,cl.LossDate as DateofLoss,cl.LossDate as DateClaimisReported, " +
                "dp.datepaid as DatePaid,clp.State as ClaimState, 'USA' as ClaimCountry,'P' as ClaimStatus_Transcode, '' as ClaimDenialReason, " +
                "cast((select top 1 Note from ClaimNote cn where cn.ClaimNoteTypeID = 3 and cn.ClaimID = cl.ClaimID)as nvarchar(max)) as CauseofLoss, " +
                "'' as ClaimType, cast((select top 1 Note from ClaimNote cn where cn.ClaimNoteTypeID = 1 and cn.ClaimID = cl.ClaimID)as nvarchar(max)) as ClaimDescription, " +
                "'' as Majorcomponent, c.VIN as VIN, '' as InsurerDeduction, '' as InsurerSettlementAmount, '' as Loanpayoffatdateofloss, '' as Adjustedpayoff, '' as Otherloanadjustments, " +
                "'' as NADAValue, cl.LossMile as MileageAtClaim, '' as PrimaryInsurer, '' as ClaimAdjuster, '' as SKIPClaimAmount, '' as DelinquentPaymentClaimAmount, " +
                "'' as GAPPlusClaimAmount, 'False' as ContractFulfillment, " +
                "case when part.part is null then 0 else part.part end as PartsCost, " +
                "case when l.labor is null then 0 else l.labor end as LaborCost, " +
                "'0.00' as DiagnosticFee, " +
                "case when i.inspection is null then 0 else i.inspection end as InspectionFee, " +
                "case when cf.carfax is null then 0 else cf.carfax end as ClaimFee, " +
                "sum(cd.TaxAmt) as TotalTax, " +
                "case when d.deduct is null then 0 else d.deduct end as AppliedDeductible, " +
                "'Standard' as DeductibleType, " +
                "sum(cd.PaidAmt) as TotalPaidAmount, '' as SalvageAmount, '' as OutstandingClaims, " +
                "clp.ServiceCenterName as RepairFacilityName, " +
                "'' as OutstandingClaims, " +
                "clp.Addr1 as RepairFacilityAddress, clp.City as RepairFacilityCity,clp.Addr2 as RepairFacilityAddress2, clp.State as RepairFacilityState,  " +
                "'USA' as RepairFacilityCountry, clp.Zip as RepairFacilityZip, clp.Phone as RepairFacilityPhone, clp.EMail as RepairFacilityEmail, '' as LaborHours,  '' as RepairTechnician, " +
                "'' as LaborRate, '' as Parts, '' as RepairDescription, '' as RepairCompletionDate " +
                "from claim cl " +
                "inner join contract c on c.ContractID = cl.ContractID " +
                "inner join claimdetail cd on cd.claimid = cl.claimid " +
                "left join ServiceCenter clp on clp.ServiceCenterID=  cl.ServiceCenterID " +
                "left join ContractCode conc on conc.ProgramID = c.ProgramID and conc.PlanTypeID = c.PlanTypeID " +
                "left join contracttpa ctpa on ctpa.TPAID = c.TPAID " +
                "left join (select claimid, Note from ClaimNote where ClaimNoteTypeID = 3) cn on cn.ClaimID = cl.ClaimID " +
                "left join (select * from ufnLaborClaim ('" + dtpStartDate.Value + "', '" + dtpEndDate.Value + "')) l on l.claimid = cl.claimid " +
                "left join (select * from ufnInspectionClaim ('" + dtpStartDate.Value + "', '" + dtpEndDate.Value + "')) I on I.claimid = cl.claimid " +
                "left join (select * from ufnCarFaxClaim ('" + dtpStartDate.Value + "', '" + dtpEndDate.Value + "')) cf on cf.claimid = cl.claimid " +
                "left join (select * from ufnDeductClaim ('" + dtpStartDate.Value + "', '" + dtpEndDate.Value + "')) d on d.claimid = cl.claimid " +
                "left join (select * from ufnDatePaidClaim ('" + dtpStartDate.Value + "', '" + dtpEndDate.Value + "')) DP on dP.claimid = cl.claimid " +
                "left join (select * from ufnPartClaim ('" + dtpStartDate.Value + "', '" + dtpEndDate.Value + "')) part on part.claimid = cl.ClaimID " +
                "where c.InsCarrierID = 1 " +
                "and cl.DatePaid >= '" + dtpStartDate.Value + "'  " +
                "and cl.DatePaid <= '" + dtpEndDate.Value + "' " +
                "and cd.RateTypeID = 1 " +
                "and c.contractid in (select contractid from contractamt where ratetypeid = 1)" +
                "and c.contractid in (select contractid from contract where status in ('Paid', 'Cancelled', 'Expired')) " +
                "and not cl.status = 'open' " +
                "and cd.INSTransmissionDate is null " +
                "and not cl.ClaimNo in (select ClaimNo from AmTrustClaimAdd where AddOrRemove = 'R')" +
                "and c.datepaid <= '" + dtpEndDate.Value + "' " +
                "and Exclude = 0 " +
                //"and not cl.claimid in (select distinct claimid from claimdetail where not ClaimDetailStatus in ('Authorized','Requested','transmitted'))" +
                "or cd.ClaimDetailID in (select ClaimDetailID from  AmTrustClaimAdd aca inner join claim cl on cl.ClaimNo = aca.ClaimNo inner join ClaimDetail cd on cd.ClaimID = cl.ClaimID where RateTypeID = 1 and Exclude = 0 and AddOrRemove = 'A') " +
                "group by cl.claimid, c.ObligorID, ctpa.TPA, c.ProgramName,c.ContractNo,cl.ClaimNo,conc.ContractCode,conc.PlanType,cl.LossDate,cl.DatePaid,clp.State, vin, " +
                "cl.LossMile, part.part, l.labor, i.inspection, cf.carfax, d.deduct, clp.ServiceCenterName, clp.Addr1, clp.Addr2, clp.City, clp.State, clp.EMail, clp.Phone, clp.Zip, dp.datepaid ";

            System.Data.DataTable dt = claim.dboInportInformation(sql);
            lblProgressBar.Text = "Processing AmTrust Claims";
            lblProgressBar.Refresh();
            bool exist = claim.hasRows();
            string writerString = "";


            string causeOfLoss = "";
            string claimDesc = "";



            double appliedTax = 0.00;

            string TPA = "Veritas Global Protection";
            string Currency = "USD";
            string ClaimCountry = "USA";
            string ContractFulfillment = "False";

            string serviceCenterPhoneNumber = "";
            string serviceCenterZip = "";
            string serviceCenterState = "";
            string serviceCenterCity = "";
            string serviceCenterAddress2 = "";
            string serviceCenterAddress = "";
            string serviceCenterName = "";
            string ProgramName = "";
            string DealerPolicyNumber = "";
            string DealerClaimID = "";
            string WarrantySKU = "";
            string WarrantyDescription = "";
            string CoverageType = "";
            string ProductID = "";
            string TransactionDate = "";
            string DateofLoss = "";
            string DateClaimisReported = "";
            string DatePaid = "";
            string ClaimState = "";
            string ClaimStatus_Transcode = "";
            string VIN = "";
            string MileageAtClaim = "";
            double PartsCost = 0.00;
            double LaborCost = 0.00;
            string DiagnosticFee = "0.00";
            double InspectionFee = 0.00;
            double AppliedDeductible = 0.00;
            double TotalPaidAmount = 0.00;
            double SalvageAmount = 0.00;
            double OutstandingClaims = 0.00;
            string DealerPolicyNumberSuffix = "";
            string DealerClaimSequenceNumber = "";
            string ClaimFee = "";
            string serviceCenterCountry = "";
            string serviceCenterEmail = "";

            double totalRecordCountClaim = 0;
            double claimscount = 0;
            double totalAppliedDeductable = 0;
            double totalPayments = 0;
            string ObligorID = "";

            string tempClaimNo = "";

            if (exist)
            {
                int tracker = dt.Rows.Count;
                lblClaim.Text = "Claims:" + tracker;
                lblClaim.Refresh();
                progressBar1.Value = 0;
                progressBar1.Maximum = tracker;
                progressBar1.Minimum = 0;
                progressBar1.Visible = true;
                progressBar1.Step = 1;
                foreach (DataRow dr in dt.Rows)
                {
                    progressBar1.PerformStep();

                    try
                    {
                        TPA = Convert.ToString(dr["TPA"]);
                    }
                    catch
                    {
                        TPA = "";
                    }//TPA
                    try
                    {
                        ProgramName = Convert.ToString(dr["programName"]);
                    }
                    catch
                    {
                        ProgramName = "";
                    }//ProgramName
                    try
                    {
                        DealerPolicyNumber = Convert.ToString(dr["DealerPolicyNumber"]);
                    }
                    catch
                    {
                        DealerPolicyNumber = "";
                    }//DealerPolicyNumber
                    try
                    {
                        DealerPolicyNumberSuffix = Convert.ToString(dr["DealerPolicyNumberSuffix"]);
                    }
                    catch
                    {
                        DealerPolicyNumberSuffix = "";
                    }//DealerPolicyNumberSuffix
                    try
                    {
                        DealerClaimID = Convert.ToString(dr["DealerClaimID"]);
                    }
                    catch
                    {
                        DealerClaimID = "";
                    }//DealerClaimID
                    try
                    {
                        DealerClaimSequenceNumber = Convert.ToString(dr["DealerClaimSequenceNumber"]);
                    }
                    catch
                    {
                        DealerClaimSequenceNumber = "";
                    }//DealerClaimSequenceNumber
                    try
                    {
                        WarrantySKU = Convert.ToString(dr["WarrantySKU"]);
                    }
                    catch
                    {
                        WarrantySKU = "";
                    }//WarrantySKU
                    try
                    {
                        WarrantyDescription = Convert.ToString(dr["WarrantyDescription"]);
                    }
                    catch
                    {
                        WarrantyDescription = "";
                    }//WarrantyDescription
                    try
                    {
                        CoverageType = Convert.ToString(dr["CoverageType"]);
                    }
                    catch
                    {
                        CoverageType = "";
                    }//CoverageType
                    try
                    {
                        ProductID = Convert.ToString(dr["ProductID"]);
                    }
                    catch
                    {
                        ProductID = "";
                    }//ProductID
                    try
                    {
                        Currency = Convert.ToString(dr["Currency"]);
                    }
                    catch
                    {
                        Currency = "";
                    }//Currency
                    try
                    {
                        TransactionDate = Convert.ToString(dr["TransactionDate"]);
                    }
                    catch
                    {
                        TransactionDate = "";
                    }//TransactionDate
                    try
                    {
                        DateofLoss = Convert.ToString(dr["DateofLoss"]);
                    }
                    catch
                    {
                        DateofLoss = "";
                    }//DateofLoss
                    try
                    {
                        DateClaimisReported = Convert.ToString(dr["DateClaimisReported"]);
                    }
                    catch
                    {
                        DateClaimisReported = "";
                    }//DateClaimisReported
                    try
                    {
                        DatePaid = Convert.ToString(dr["DatePaid"]);
                    }
                    catch
                    {
                        DatePaid = "";
                    }//DatePaid
                    try
                    {
                        ClaimState = Convert.ToString(dr["ClaimState"]);
                    }
                    catch
                    {
                        ClaimState = "";
                    }//ClaimState
                    try
                    {
                        ClaimCountry = Convert.ToString(dr["ClaimCountry"]);
                    }
                    catch
                    {
                        ClaimCountry = "";
                    }//ClaimCountry
                    try
                    {
                        ClaimStatus_Transcode = Convert.ToString(dr["ClaimStatus_Transcode"]);
                    }
                    catch
                    {
                        ClaimStatus_Transcode = "";
                    }//ClaimStatus_Transcode
                    try
                    {
                        causeOfLoss = Convert.ToString(dr["CauseofLoss"]);
                        causeOfLoss = causeOfLoss.Replace("\r", "");
                        causeOfLoss = causeOfLoss.Replace("\n", "");
                    }
                    catch
                    {
                        causeOfLoss = "";
                    }//causeOfLoss
                    try
                    {
                        claimDesc = Convert.ToString(dr["ClaimDescription"]);
                        claimDesc = claimDesc.Replace("\r", "");
                        claimDesc = claimDesc.Replace("\n", "");
                    }
                    catch
                    {
                        claimDesc = "";
                    }//claimDesc
                    try
                    {
                        VIN = Convert.ToString(dr["VIN"]);
                    }
                    catch
                    {
                        VIN = "";
                    }//VIN
                    try
                    {
                        MileageAtClaim = Convert.ToString(dr["MileageAtClaim"]);
                    }
                    catch
                    {
                        MileageAtClaim = "";
                    }//MileageAtClaim
                    try
                    {
                        ContractFulfillment = Convert.ToString(dr["ContractFulfillment"]);
                    }
                    catch
                    {
                        ContractFulfillment = "";
                    }//ContractFulfillment
                    try
                    {
                        PartsCost = Convert.ToDouble(dr["PartsCost"]);
                    }
                    catch
                    {
                        PartsCost = 0;
                    }//PartsCost
                    try
                    {
                        LaborCost = Convert.ToDouble(dr["LaborCost"]);
                    }
                    catch
                    {
                        LaborCost = 0;
                    }//LaborCost
                    try
                    {
                        DiagnosticFee = Convert.ToString(dr["DiagnosticFee"]);
                    }
                    catch
                    {
                        DiagnosticFee = "";
                    }//DiagnosticFee
                    try
                    {
                        InspectionFee = Convert.ToDouble(dr["InspectionFee"]);
                    }
                    catch
                    {
                        InspectionFee = 0;
                    }//InspectionFee
                    try
                    {
                        ClaimFee = Convert.ToString(dr["ClaimFee"]);
                    }
                    catch
                    {
                        ClaimFee = "";
                    }//ClaimFee
                    try
                    {
                        appliedTax = Convert.ToDouble(dr["TotalTax"]);
                    }
                    catch
                    {
                        appliedTax = 0;
                    }//appliedTax
                    try
                    {
                        AppliedDeductible = Convert.ToDouble(dr["AppliedDeductible"]);
                    }
                    catch
                    {
                        AppliedDeductible = 0;
                    }//AppliedDeductible
                    try
                    {
                        TotalPaidAmount = Convert.ToDouble(dr["TotalPaidAmount"]);
                    }
                    catch
                    {
                        TotalPaidAmount = 0;
                    }//TotalPaidAmount
                    try
                    {
                        SalvageAmount = Convert.ToDouble(dr["SalvageAmount"]);
                    }
                    catch 
                    {
                        SalvageAmount = 0;
                    }//SalvageAmount
                    try
                    {
                        OutstandingClaims = Convert.ToDouble(dr["OutstandingClaims"]);
                    }
                    catch
                    {
                        OutstandingClaims = 0;
                    }
                    try
                    {
                        serviceCenterName = Convert.ToString(dr["RepairFacilityName"]);
                    }
                    catch
                    {
                        serviceCenterName = "";
                    }//serviceCenterName
                    try
                    {
                        serviceCenterAddress = Convert.ToString(dr["RepairFacilityAddress"]);
                    }
                    catch
                    {
                        serviceCenterAddress = "";
                    }//serviceCenterAddress
                    try
                    {
                        serviceCenterCity = Convert.ToString(dr["RepairFacilityCity"]);
                    }
                    catch
                    {
                        serviceCenterCity = "";
                    }//serviceCenterCity
                    try
                    {
                        serviceCenterAddress2 = Convert.ToString(dr["RepairFacilityAddress2"]);
                    }
                    catch
                    {
                        serviceCenterAddress2 = "";
                    }//serviceCenterAddress2
                    try
                    {
                        serviceCenterState = Convert.ToString(dr["RepairFacilityState"]);
                    }
                    catch
                    {
                        serviceCenterState = "";
                    }//serviceCenterState
                    try
                    {
                        serviceCenterCountry = Convert.ToString(dr["RepairFacilityCountry"]);
                    }
                    catch
                    {
                        serviceCenterCountry = "";
                    }//serviceCenterCountry
                    try
                    {
                        serviceCenterZip = Convert.ToString(dr["RepairFacilityZip"]);
                    }
                    catch
                    {
                        serviceCenterZip = "";
                    }//serviceCenterZip
                    try
                    {
                        serviceCenterPhoneNumber = Convert.ToString(dr["RepairFacilityPhone"]);
                    }
                    catch
                    {
                        serviceCenterPhoneNumber = "";
                    }//serviceCenterPhoneNumber
                    try
                    {
                        serviceCenterEmail = Convert.ToString(dr["RepairFacilityEmail"]);
                    }
                    catch
                    {
                        serviceCenterEmail = "";
                    }//serviceCenterEmail
                    try
                    {
                        ObligorID = Convert.ToString(dr["ObligorID"]);
                    }
                    catch
                    {
                        ObligorID = "";
                    }//ObligorID

                    if (AppliedDeductible < 0)
                    {
                        AppliedDeductible = AppliedDeductible * -1;
                    }

                    try
                    {
                        int testString = Convert.ToInt16(serviceCenterZip);
                    }
                    catch
                    {
                        serviceCenterState = "KS";
                    }

                    totalRecordCountClaim = totalRecordCountClaim + 1;
                    if (DealerClaimID != tempClaimNo)
                    {
                        tempClaimNo = DealerClaimID;
                        claimscount = claimscount + 1;
                        string sSQL = "select * from MiscDB.dbo.AmTrustClaimCounter where claimno = '" + tempClaimNo + "'";
                        CSharpDBO.CSharpDBO dBO = new CSharpDBO.CSharpDBO();
                        System.Data.DataTable dataTable = dBO.dboInportInformation(sSQL);
                        if (dBO.hasRows())
                        {
                            AmTrustClaimCounter claimCounter = new AmTrustClaimCounter();
                            foreach (DataRow row in dataTable.Rows)
                            {
                                DealerClaimID = DealerClaimID + claimCounter.counter(Convert.ToInt32(row["Counter"]));
                            }
                        }
                    }

                    totalAppliedDeductable = totalAppliedDeductable + AppliedDeductible;
                    totalPayments = totalPayments + TotalPaidAmount;
                    writerString = "";
                    writerString = writerString + (TPA.Trim() + "|");
                    writerString = writerString + (ProgramName.Trim() + "|");
                    writerString = writerString + (DealerPolicyNumber.Trim() + "|");
                    writerString = writerString + ("|");
                    writerString = writerString + (DealerClaimID.Trim() + "|");
                    writerString = writerString + (DealerClaimSequenceNumber + "|");
                    writerString = writerString + ("|");
                    writerString = writerString + (WarrantySKU.Trim() + "|");
                    writerString = writerString + (CoverageType + "|");
                    writerString = writerString + ("|");
                    writerString = writerString + (ProductID + "|");
                    writerString = writerString + (Currency + "|");
                    writerString = writerString + (TransactionDate + "|");
                    writerString = writerString + (DateofLoss + "|");
                    writerString = writerString + (DateClaimisReported + "|");
                    writerString = writerString + (DatePaid + "|");
                    writerString = writerString + (ClaimState.Trim() + "|");
                    writerString = writerString + (ClaimCountry.Trim() + "|");
                    writerString = writerString + (ClaimStatus_Transcode.Trim() + "|");
                    writerString = writerString + ("|");
                    writerString = writerString + (causeOfLoss + "|");
                    writerString = writerString + ("|");
                    writerString = writerString + (claimDesc + "|");
                    writerString = writerString + ("|");
                    writerString = writerString + (VIN + "|");
                    writerString = writerString + ("|");
                    writerString = writerString + ("|");
                    writerString = writerString + ("|");
                    writerString = writerString + ("|");
                    writerString = writerString + ("|");
                    writerString = writerString + ("|");
                    writerString = writerString + (MileageAtClaim.Trim() + "|");
                    writerString = writerString + ("|");
                    writerString = writerString + ("|");
                    writerString = writerString + ("|");
                    writerString = writerString + ("|");
                    writerString = writerString + ("|");
                    writerString = writerString + (ContractFulfillment.Trim() + "|");
                    writerString = writerString + (PartsCost + "|");
                    writerString = writerString + (LaborCost + "|");
                    writerString = writerString + (DiagnosticFee + "|");
                    writerString = writerString + (InspectionFee + "|");
                    writerString = writerString + (ClaimFee + "|");
                    writerString = writerString + (appliedTax + "|");
                    writerString = writerString + (AppliedDeductible + "|");
                    writerString = writerString + ("|");
                    writerString = writerString + (TotalPaidAmount + "|");
                    writerString = writerString + (SalvageAmount + "|");
                    writerString = writerString + (OutstandingClaims+"|");
                    writerString = writerString + (serviceCenterName + "|");
                    writerString = writerString + ("|");
                    writerString = writerString + (serviceCenterAddress + "|");
                    writerString = writerString + (serviceCenterCity + "|");
                    writerString = writerString + (serviceCenterAddress2 + "|");
                    writerString = writerString + (serviceCenterState + "|");
                    writerString = writerString + (serviceCenterCountry + "|");
                    writerString = writerString + (serviceCenterZip + "|");
                    writerString = writerString + (serviceCenterPhoneNumber + "|");
                    writerString = writerString + (serviceCenterEmail + "|");
                    writerString = writerString + ("|");
                    writerString = writerString + ("|");
                    writerString = writerString + ("|");
                    writerString = writerString + ("|");
                    writerString = writerString + ("|");
                    writerString = writerString + ("|");
                    writerString = writerString.Replace("\r\n", "");
                    writerString = writerString.Replace(",", "");
                    streamWriterClaim.WriteLine(writerString);
                    Console.WriteLine(tracker);
                    tracker--;
                }
                StreamWriter streamWriterClaimControl = new StreamWriter(setPath() + "/MonthEnd/" + setMonth() + "/AmTrust/ " + setYear() + "-" + setMonthNo() + "_Veritas_ClaimsControl.txt");



                streamWriterClaimControl.Write("Description|Totals|Control Total Mapping");
                streamWriterClaimControl.Write("Total Record Count|");
                streamWriterClaimControl.WriteLine(totalRecordCountClaim + "|Overall Record Count of the claims file|");
                streamWriterClaimControl.Write("Claims Count|");
                streamWriterClaimControl.WriteLine(claimscount + "|");
                streamWriterClaimControl.Write("Total Applied Deductable|Count of claim records|");
                streamWriterClaimControl.WriteLine(totalAppliedDeductable + "|SUM of TotalAppliedDeductable column");
                streamWriterClaimControl.Write("TotalPayments|");
                streamWriterClaimControl.WriteLine(totalPayments + "|SUM of TotalPaidAmount column");
                streamWriterClaimControl.Close();
                claim.dboClose();
                progressBar1.Value = 0;
                lblProgressBar.Text = "";

            }

            streamWriterClaim.Close();
        }
        public void AmTrustContractMapping()
        {
            CSharpDBO.CSharpDBO contractMappingDBO = new CSharpDBO.CSharpDBO();
            contractMappingDBO.dboOpen(path);
            string header = "TPA|InsuranceCompanyName|ProgramName|InsuranceType|Currency|DealerPolicyNumber|DealerPolicyNumberSuffix|CustomerContractNumber|WarrantySKU|WarrantyDescription|CoverageGroup|CoverageType|CoverageSubType|ProductID|ContractStatus_Transcode|TermMileage|TermMonths|TermHours|ManufacturerTermMonths|StartOfRiskType|TransactionDate|ContractPurchaseDate|ProductPurchaseDate|VehicleInServiceDate|ManufacturerEffectiveDate|ManufacturerExpirationDate|LaborCoverageEffectiveDate|LaborCoverageExpirationDate|PartsCoverageEffectiveDate|PartsCoverageExpirationDate|ProductQuantity|ProductSKU|ProductType|ProductSubType|HoursAtPurchase|LoanOrLease|LoanToValue|Make|Model|ModelYear|Category |VIN|CC|RateClass|New_Used|NADA_MSRPValue|ProductPurchasePrice|AmountFinanced|MileageAtPurchase|MileageAtCoverageStart|LoanAPR|AgreementType|SkipBenefitQuantity|DeliquentPaymentBenefitQuantity|ContractExpireHours|ContractExpireMileage|UpperPriceBand|UseType|CompanyNameOfPurchaser|CustomerFirstName|CustomerLastName|CustomerAddress|CustomerAddress2|CustomerCity|CustomerState|CustomerZip|CustomerCountry|CustomerEmail|CustomerPhone|ProducerType|ProducerCode|ProducerName|ProducerAddress|ProducerAddress2|ProducerCity|ProducerState|ProducerZipCode|ProducerCountry|SalesChannel|BookID|ClaimLimitOfLiability|ContractLimitOfLiability|ClaimCountLimit|ClaimType|ContractPaymentStatus|PaidFlag|DeductibleType|Deductible|DeductibleAtProducer|ReInsuranceCompanyCode|ReInsuranceCompanyName|ReinsuranceType|ManagementFee|RiskPoolFee|FederalExciseTax|DealerCost|RetailAmount|ContractPrice|NetInsurancePremiumControl|InsurancePremiumChange|NetReserveAmountControl|ReserveAmountChange|PremiumTax|RiskFees|IPTRate|ServiceFees|Adminamount|BaseReserveAmount|SurchargeReserveAmount|BrokerFee|Ceding_Insurance_Fee|Commission|ContingencyFee|ObligorFee|BalanceInsuredAmount|CommercialSurcharge|Diesel_Hybrid_Surcharge|4WD_AWD_Surcharge|Turbo_SuperChargeSurcharge|OtherSurcharges|CancellationClaimAdjusment|CancellationDate|CancellationFormula|CancellationHours|CancellationMileage|CancellationPercentage|CancellationReason|ObligorID|ObligorName|ObligorState|ObligorAddress1|ObligorAddress2|ObligorCountry|FormNumber|TPAAccountManager|TPASalesAgent|TPASalesRep";
            StreamWriter streamWriterContract = new StreamWriter(setPath() + "/MonthEnd/" + setMonth() + "/AmTrust/ " + setYear() + "-" + setMonthNo() + "_Veritas_Sales.txt");
            streamWriterContract.WriteLine(header);



            string sql = "";

            string formNumber = "";
            string ClaimLoL = "";
            string ContractLoL = "";


            double CancellationMileage = 0.00;
            double ContingencyFee = 0.00;
            double ObligorFee = 0.00;
            double CommercialSurcharge = 0.00;
            double Diesel_Hybrid_Surcharge = 0.00;
            double FourWD_AWD_Surcharge = 0.00;
            double Turbo_SuperChargeSurcharge = 0.00;
            double OtherSurcharges = 0.00;

            string TPA = "Veritas Global Protection";
            string Currency = "USD";
            string StartOfRiskType = "Date of Purchase";
            string ProductPurchasePrice = "0.00";
            string CustomerCountry = "USA";
            string ProducerCountry = "USA";
            string CancellationFormula = "ProRate";
            string ObligorCountry = "USA";
            string DeductibleType = "Standard";
            string ProgramName = "";
            string DealerPolicyNumber = "";
            string WarrantySKU = "";
            string WarrantyDescription = "";
            string CoverageType = "";
            string ProductID = "";
            string TransactionDate = "";
            string VIN = "";
            string CustomerContractNumber = "";
            string InsuranceCompanyName = "";
            string InsuranceType = "";
            string ContractStatus_Transcode = "";
            string TermMileage = "";
            string TermMonths = "";
            string ContractPurchaseDate = "";
            string ProductPurchaseDate = "1/1/1111";
            string LaborCoverageEffectiveDate = "";
            string LaborCoverageExpirationDate = "";
            string PartsCoverageEffectiveDate = "";
            string PartsCoverageExpirationDate = "";
            string ProductSKU = "";
            string ProductType = "Auto";
            string Make = "";
            string Model = "";
            string ModelYear = "";
            string RateClass = "";
            string New_Used = "";
            string MileageAtPurchase = "";
            string MileageAtCoverageStart = "";
            string ContractExpireMileage = "";
            string UseType = "";
            string CustomerFirstName = "";
            string CustomerLastName = "";
            string CustomerAddress = "";
            string CustomerAddress2 = "";
            string CustomerCity = "";
            string CustomerState = "";
            string CustomerZip = "";
            string ProducerCode = "";
            string ProducerName = "";
            string ProducerCity = "";
            string ProducerState = "";
            string SalesChannel = "";
            string Deductible = "";
            string RetailAmount = "";
            double NetInsurancePremiumControl = 0.00;
            double InsurancePremiumChange = 0.00;
            double totalReserveAmount = 0.00;
            double NetReserveAmountControl = 0.00;
            double ReserveAmountChange = 0.00;
            double PremiumTax = 0.00;
            double Ceding_Insurance_Fee = 0;
            string CancellationDate = "";
            string CancellationPercentage = "";
            string ObligorID = "";
            string ObligorName = "";
            string ObligorState = "";
            string CC = "";
            double dealerCost = 0.00;
            string coverageSubType = "";
            int ProductQuantity = 1;
            string ProducerAddress = "";
            string ProducerAddress2 = "";
            string ReInsuranceCompanyCode = "";
            string ReInsuranceCompanyName = "";

            double totalRecordCountcontrol = 0.00;
            double saleCountcontrol = 0.00;
            double totalCancelsCountcontrol = 0.00;
            double retailAmountcontrol = 0.00;
            double insurancePremiumcontrol = 0.00;
            double commissionscontrol = 0.00;
            double premiumTaxcontrol = 0.00;
            double contigencyFeescontrol = 0.00;
            double serviceFeescontrol = 0.00;
            double adminAmountcontrol = 0.00;
            double commissions = 0.00;
            double contigency = 0.00;
            double admin = 0.00;
            double serviceFee = 0.00;
            string ProducerZipCode = "";
            double BalanceInsuredAmount = 0;
            string ObligorAddress1 = "";
            string ObligorAddress2 = "";
            string ReinsuranceType = "";
            int clipID = 0;
            DateTime today2 = DateTime.Today;
            DateTime converter = DateTime.Today;
            lblProgressBar.Text = "Executing Querry";
            lblProgressBar.Refresh();
            sql = "select distinct c.clipID as clipID, c.status as status, ctpa.TPA as TPA, 'Wesco Insurance Company' as InsuranceCompanyName,c.ProgramName, c.contractno as DealerPolicyNumber, " +
                "'' as DealerPolicyNumberSuffix, ccp.ClipType as InsuranceType, 'USD' as Currency, c.ContractNo as CustomerContractNo, " +
                "cc.ContractCode as WarrantySKU, cc.Program + ' ' + cc.PlanType as WarrantyDescription, '' as CoverageGroup, 'VSC' as CoverageType,'' as CoverageSubType, " +
                "'1' as ProductID, 'Paid' as ContractStatus_Transcode, c.TermMile as TermMileage, c.TermMonth as TermMonths, '' as TermHours, " +
                "'' as ManufacturerTermMonths, 'Date of Purchase' as StartOfRiskType, datepaid as TransactionDate, SaleDate as ContractPurchaseDate, " +
                "SaleDate as ProductPurchaseDate, '' as VehicleInServiceDate, '' as ManufacturerEffectiveDate, '' as ManufacturerExpirationDate, " +
                "c.EffDate as LaborCoverageEffectiveDate, c.ExpDate as LaborCoverageExpirationDate, c.EffDate as PartsCoverageEffectiveDate, " +
                "c.ExpDate as PartsCoverageExpirationDate, '1' as ProductQuantity, cc.ContractCode as ProductSKU, " +
                "case when c.programid = 10 or c.programid = 63 then " +
                "case when c.PlanTypeID = 79 or c.PlanTypeID = 80 then " +
                "'Trailers' " +
                "when c.plantypeid > 98 " +
                "or c.plantypeid < 109 then " +
                "'Trailers' else 'RV' end when c.programid = 9 or c.ProgramID = 62 " +
                "then 'Motorcycles' else 'Auto' end as ProductType, " +
                "'' as ProductSubType, '' as HoursAtPurchase, '' as LoanOrLease, '' as LoanToValue, c.Make, c.Model, c.year as ModelYear, " +
                "'' as Category, c.VIN, c.EngineCC as cc, c.Class as RateClass, c.NewUsed as New_Used, '' as NADA_MSRPValue, " +
                "'0.00' as ProductPurchasePrice, '' as AmountFinanced, SaleMile as MileageAtPurchase, c.EffMile as MileageAtCoverageStart, " +
                "'' as LoanAPR, '' as AgreementType, '' as SkipBenefitQuantity, '' as DeliquentPaymentBenefitQuantity, '' as ContractExpireHours, " +
                "c.ExpMile as ContractExpireMileage, '' as UpperPriceBand, case when commercial <> 0 then 'Commercial' else 'Personal' end as UseType, " +
                "'' as CompanyNameOfPurchaser, c.FName as CustomerFirstName, c.LName as CustomerLastName, c.Addr1 as CustomerAddress, " +
                "c.Addr2 as CustomerAddress2, c.city as CustomerCity, c.State as CustomerState, c.Zip as CustomerZip, 'USA' as CustomerCountry, " +
                "'' as CustomerEmail, '' as CustomerPhone, '' as ProducerType, d.DealerNo as ProducerCode, (d.DealerName + ' - ' + d.dealerNo) as ProducerName, " +
                "d.Addr1 as ProducerAddress, d.Addr2 as ProducerAddress2, d.city as ProducerCity, d.State as ProducerState, d.zip as ProducerZipCode, " +
                "'USA' as ProducerCountry, case when d.dealerno = '1001' then 'Call Center' when left(c.contractno, 3) = 'REP' then " +
                "'Call Center' else 'Dealer' end as SalesChannel, '' as BookID, cll.ClaimLOL as ClaimLimitOfLiability, " +
                "cll.ContractLOL as ContractLimitOfLiability, '' as ClaimCountLimit, '' as ClaimType, '' as ContractPaymentStatus, '' as PaidFlag, " +
                "'Standard' as DeductibleType, dd.DeductAmt as Deductible, '' as DeductibleAtProducer, " +
                "c.ReInsuranceCompanyCode as ReInsuranceCompanyCode, " +
                "c.ReInsuranceCompanyName as ReInsuranceCompanyName, " +
                "c.ReinsuranceType as ReinsuranceType, '' as ManagementFee, '' as RiskPoolFee, " +
                "" +
                "((case when r.Amt is null then 0 else r.amt end) + " +
                "(case when s.Amt is null then 0 else s.amt end) + " +
                "(case when pt.Amt is null then 0 else pt.Amt end) + " +
                "(case when cf.amt is null then 0 else cf.amt end) + " +
                "(case when cy.Amt is null then 0 else cy.amt end) + " +
                "(case when o.Amt is null then 0 else o.amt end)" +
                ") " +
                " as DealerCost, " +
                "" +
                "c.CustomerCost as RetailAmount, " +
                "" +
                "c.CustomerCost as ContractPrice, '', " +
                "" +
                "(case when s.amt is null then 0 else s.Amt end) + " +
                "(case when cf.amt is null then 0 else cf.amt end) + " +
                "(case when pt.Amt is null then 0 else pt.Amt end) + " +
                "(case when r.Amt is null then 0 else r.amt end) " +
                "as NetInsurancePremiumControl, " +
                "" +
                "(case when cf.amt is null then 0 else cf.amt end) + " +
                "(case when pt.Amt is null then 0 else pt.Amt end) " +
                "as XOLNetInsurancePremiumControl, " +
                "" +

                "((case when r.Amt is null then 0 else r.amt end) + " +
                "(case when s.Amt is null then 0 else s.amt end) + " +
                "(case when pt.Amt is null then 0 else pt.Amt end) + " +
                "(case when cf.amt is null then 0 else cf.amt end)) " +
                " as InsurancePremiumChange, " +
                "" +
                "(case when pt.Amt is null then 0 else pt.Amt end) + " +
                "(case when cf.amt is null then 0 else cf.amt end) " +
                " as XOLInsurancePremiumChange, " +

                "(case when r.Amt is null then 0 else r.amt end) + (case when s.Amt is null then 0 else s.amt end) as NetReserveAmountControl, " +
                "" +
                "(case when r.Amt is null then 0 else r.amt end) + (case when s.Amt is null then 0 else s.amt end) as ReserveAmountChange, " +
                "" +
                "(case when pt.Amt is null then 0 else pt.Amt end) as PremiumTax, '' as RiskFees, '' as IPTRate, '' as ServiceFees, '' as Adminamount, " +
                "'' as BaseReserveAmount, " +
                "'' as SurchargeReserveAmount, " +
                "'' as BrokerFee, " +
                "(case when cf.amt is null then 0 else cf.amt end) as Ceding_Insurance_Fee, " +
                "'' as Commission, " +
                "(case when cy.Amt is null then 0 else cy.amt end) as ContingencyFee, " +
                "(case when o.Amt is null then 0 else o.amt end) as ObligorFee, " +
                "'0' as BalanceInsuredAmount, (case when comm.Amt is null then 0 else comm.amt end) as CommercialSurcharge, " +
                "(case when DH.Amt is null then 0 else DH.amt end) as Diesel_Hybrid_Surcharge, " +
                "(case when awd.Amt is null then 0 else awd.amt end) as AWD_4WD_Surcharge, " +
                "(case when t.Amt is null then 0 else t.amt end) as Turbo_SuperChargeSurcharge, " +
                "((case when s.amt is null then 0 else s.Amt end) - " +
                "(case when comm.Amt is null then 0 else comm.amt end) - " +
                "(case when DH.Amt is null then 0 else DH.amt end) - " +
                "(case when awd.Amt is null then 0 else awd.amt end) - " +
                "(case when t.Amt is null then 0 else t.amt end)) as OtherSurcharges, " +
                "" +
                "'' as CancellationClaimAdjusment, '' as CancellationDate, " +
                "'' as CancellationFormula, '' as CancellationHours, '' as CancellationMileage, '' as CancellationPercentage," +
                " '' as CancellationReason, " +
                "" +
                "c.ObligorID as ObligorID, ob.ObligorName as ObligorName, ob.state as ObligorState, '' as ObligorAddress1, '' as ObligorAddress2, 'USA' as ObligorCountry, " +
                "c.FormNo as FormNumber, '' as TPAAccountManager, '' as TPASalesAgent, '' as TPASalesRep from contract c left join program p on " +
                "c.ProgramID = p.ProgramID left join inscarrier ic on ic.InsCarrierID = c.InsCarrierID " +
                "left join contractcode cc on cc.ProgramID = " +
                "c.ProgramID and cc.PlanTypeID = c.PlanTypeID left join dealer d on d.dealerid = c.dealerid left join deductible dd on dd.DeductID = " +
                "c.DeductID " +
                "left join (select contractid, sum(amt) as Amt from contractamt ca inner join ratetype rt on rt.RateTypeID = ca.RateTypeID " +
                "where rt.RateCategoryID = 4 group by contractid) as R on c.contractid = r.ContractID " +
                "left join (select contractid, sum(amt) as Amt from " +
                "contractamt ca inner join ratetype rt on rt.RateTypeID = ca.RateTypeID where rt.RateCategoryID = 3 group by contractid) as s on " +
                "c.contractid = s.ContractID " +
                "left join (select contractid, sum(amt) as Amt from contractamt ca where ca.RatetypeID = 8 group by contractid) " +
                "as CF on c.contractid = cf.ContractID " +
                "left join (select contractid, sum(amt) as Amt from contractamt ca where ca.RatetypeID = 6 group by contractid) as pt on c.contractid = pt.ContractID " +
                "left join (select contractid, sum(amt) as Amt from contractamt ca where ca.RatetypeID = 9 group by contractid) as o on c.contractid = " +
                "o.ContractID " +
                "left join (select contractid, sum(amt) as Amt from contractamt ca where ca.RatetypeID = 7 group by contractid) as cy on " +
                "c.contractid = cy.ContractID left join (select contractid, sum(amt) as Amt from contractamt ca where ca.RatetypeID = 21 group by contractid)" +
                " as comm on c.contractid = comm.ContractID " +
                "left join (select contractid, sum(amt) as Amt from contractamt ca where (ca.RatetypeID = 19 or ca.RateTypeID=39) " +
                "group by contractid) as DH on c.contractid = DH.ContractID left join (select contractid, sum(amt) as Amt from contractamt ca where" +
                " ca.RatetypeID = 18 group by contractid) as AWD on c.contractid = AWD.ContractID left join (select contractid, sum(amt) as Amt from contractamt ca where" +
                " ca.RatetypeID = 20 group by contractid) as t on c.contractid = t.ContractID left join obligor ob on c.ObligorID = ob.ObligorID " +
                "left join PlanType plt on plt.PlanTypeID = c.PlanTypeID " +
                "left join ContractResinsurance cri on cri.ReinsuranceID = c.ReinsuranceID " +
                "left join contractClip ccp on ccp.ClipId = c.clipID " +
                "left join ContractLimitofLiabilty  cll on cll.PlanTypeID = c.PlanTypeID and cll.ProgramID = c.ProgramID " +
                "left join contractTPA ctpa on ctpa.TPAID = c.TPAID " +
                "where (c.status = 'Paid' or c.Status = 'Expired' or c.status = 'Cancelled') " +
                "and c.datePaid <= '" + dtpEndDate.Value + "' " +
                "and c.datePaid >= '" + dtpStartDate.Value + "' " +
                "and inssaletransmissiondate is null " +
                "and (c.ProgramID < 47 or c.ProgramID > 60) " +
                "and not c.DatePaid is null " +
                "and c.InsCarrierID = 1 " +
                "and not contractno like 'VGL%'" +
                "and c.contractid in (select contractid from contractamt where ratetypeid = 1)" +
                "and not c.contractno in (select contractno from AmTrustContractAdd where AddOrRemove = 'R')" +
                "or c.contractno in (select contractNo from AmTrustContractAdd where AddOrRemove = 'A' and Status = 'Paid' ) " +
                " order by c.contractno ";
            System.Data.DataTable dt = contractMappingDBO.dboInportInformation(sql);
            int tracker = dt.Rows.Count;
            lblProgressBar.Text = "Processing AmTrust Sales";
            lblProgressBar.Refresh();
            lblContract.Text = "Paid Contracts:" + tracker;
            lblContract.Refresh();
            progressBar1.Value = 0;
            progressBar1.Maximum = tracker;
            progressBar1.Minimum = 0;
            progressBar1.Visible = true;
            progressBar1.Step = 1;
            foreach (DataRow dr in dt.Rows)
            {
                progressBar1.PerformStep();


                try
                {
                    clipID = Convert.ToInt16(dr["clipID"]);
                }
                catch
                {
                    clipID = 0;
                }//clipID
                try
                {
                    ReInsuranceCompanyName = Convert.ToString(dr["ReInsuranceCompanyName"]);
                }
                catch
                {
                    ReInsuranceCompanyName = "";
                }//ReInsuranceCompanyName
                try
                {
                    ReinsuranceType = Convert.ToString(dr["ReinsuranceType"]);
                }
                catch
                {
                    ReinsuranceType = "";
                }//ReinsuranceType
                try
                {
                    ReInsuranceCompanyCode = Convert.ToString(dr["ReInsuranceCompanyCode"]);
                }
                catch
                {
                    ReInsuranceCompanyCode = "";
                }//ReInsuranceCompanyCode
                try
                {
                    TPA = dr["TPA"].ToString();
                }
                catch
                {
                    TPA = "";
                }//TPA
                try
                {
                    InsuranceCompanyName = dr["InsuranceCompanyName"].ToString();
                }
                catch
                {
                    InsuranceCompanyName = "";
                }//InsuranceCompanyName
                try
                {
                    ProgramName = dr["ProgramName"].ToString();
                }
                catch
                {
                    ProgramName = "";
                }//ProgramName
                try
                {
                    DealerPolicyNumber = dr["DealerPolicyNumber"].ToString();
                }
                catch
                {
                    DealerPolicyNumber = "";
                }//DealerPolicyNumber
                try
                {
                    InsuranceType = dr["InsuranceType"].ToString();
                }
                catch
                {
                    InsuranceType = "FD";
                }//InsuranceType
                try
                {
                    Currency = dr["Currency"].ToString();
                }
                catch
                {
                    Currency = "";
                }//Currency
                try
                {
                    CustomerContractNumber = dr["CustomerContractNo"].ToString();
                }
                catch
                {
                    CustomerContractNumber = "";
                }//CustomerContractNumber
                try
                {
                    WarrantySKU = dr["WarrantySKU"].ToString();
                }
                catch
                {
                    WarrantySKU = "";
                }//WarrantySKU
                try
                {
                    WarrantyDescription = Convert.ToString(dr["WarrantyDescription"]);
                }
                catch
                {
                    WarrantyDescription = "";
                }//WarrantyDescription
                try
                {
                    CoverageType = dr["CoverageType"].ToString();
                }
                catch
                {
                    CoverageType = "";
                }//CoverageType
                try
                {
                    ProductID = dr["ProductID"].ToString();
                }
                catch
                {
                    ProductID = "";
                }//ProductID
                try
                {
                    ContractStatus_Transcode = dr["ContractStatus_Transcode"].ToString();
                }
                catch
                {
                    ContractStatus_Transcode = "";
                }//ContractStatus_Transcode
                try
                {
                    TermMileage = dr["TermMileage"].ToString();
                }
                catch
                {
                    TermMileage = "";
                }//TermMileage
                try
                {
                    TermMonths = dr["TermMonths"].ToString();
                }
                catch
                {
                    TermMonths = "";
                }//TermMonths
                try
                {
                    StartOfRiskType = dr["StartOfRiskType"].ToString();
                }
                catch
                {
                    StartOfRiskType = "";
                }//StartOfRiskType
                try
                {
                    TransactionDate = dr["TransactionDate"].ToString();
                }
                catch
                {
                    TransactionDate = "";
                }//TransactionDate
                try
                {
                    ContractPurchaseDate = dr["ContractPurchaseDate"].ToString();
                }
                catch
                {
                    ContractPurchaseDate = "";
                }//ContractPurchaseDate
                try
                {
                    ProductPurchaseDate = dr["ProductPurchaseDate"].ToString();
                }
                catch
                {
                    ProductPurchaseDate = "";
                }//ProductPurchaseDate
                try
                {
                    LaborCoverageEffectiveDate = dr["LaborCoverageEffectiveDate"].ToString();
                }
                catch
                {
                    LaborCoverageEffectiveDate = "";
                }//LaborCoverageEffectiveDate
                try
                {
                    LaborCoverageExpirationDate = dr["LaborCoverageExpirationDate"].ToString();
                }
                catch
                {
                    LaborCoverageExpirationDate = "";
                }//LaborCoverageExpirationDate
                try
                {
                    PartsCoverageEffectiveDate = dr["PartsCoverageEffectiveDate"].ToString();
                }
                catch
                {
                    PartsCoverageEffectiveDate = "";
                }//PartsCoverageEffectiveDate
                try
                {
                    PartsCoverageExpirationDate = dr["PartsCoverageExpirationDate"].ToString();
                }
                catch
                {
                    PartsCoverageExpirationDate = "";
                }//PartsCoverageExpirationDate
                try
                {
                    ProductQuantity = Convert.ToInt32(dr["ProductQuantity"]);
                }
                catch
                {
                    ProductQuantity = 0;
                }//ProductQuantity
                try
                {
                    ProductSKU = dr["ProductSKU"].ToString();
                }
                catch
                {
                    ProductSKU = "";
                }//ProductSKU
                try
                {
                    ProductType = dr["ProductType"].ToString();
                }
                catch
                {
                    ProductType = "";
                }//ProductType
                try
                {
                    Make = dr["Make"].ToString();
                }
                catch
                {
                    Make = "";
                }//Make
                try
                {
                    Model = dr["Model"].ToString();
                }
                catch
                {
                    Model = "";
                }//Model
                try
                {
                    ModelYear = dr["ModelYear"].ToString();
                }
                catch
                {
                    ModelYear = "";
                }//ModelYear
                try
                {
                    VIN = dr["VIN"].ToString();
                }
                catch
                {
                    VIN = "";
                }//VIN
                try
                {
                    CC = dr["cc"].ToString();
                }
                catch
                {
                    CC = "";
                }//CC
                try
                {
                    RateClass = dr["RateClass"].ToString();
                }
                catch
                {
                    RateClass = "";
                }//RateClass
                try
                {
                    New_Used = dr["New_Used"].ToString();
                }
                catch
                {
                    New_Used = "";
                }//New_Used
                try
                {
                    ProductPurchasePrice = dr["ProductPurchasePrice"].ToString();
                }
                catch
                {
                    ProductPurchasePrice = "";
                }//ProductPurchasePrice
                try
                {
                    MileageAtPurchase = dr["MileageAtPurchase"].ToString();
                }
                catch
                {
                    MileageAtPurchase = "";
                }//MileageAtPurchase
                try
                {
                    MileageAtCoverageStart = dr["MileageAtCoverageStart"].ToString();
                }
                catch
                {
                    MileageAtCoverageStart = "";
                }//MileageAtCoverageStart
                try
                {
                    ContractExpireMileage = dr["ContractExpireMileage"].ToString();
                }
                catch
                {
                    ContractExpireMileage = "";
                }//ContractExpireMileage
                try
                {
                    UseType = dr["UseType"].ToString();
                }
                catch
                {
                    UseType = "";
                }//UseType
                try
                {
                    CustomerFirstName = dr["CustomerFirstName"].ToString();
                }
                catch
                {
                    CustomerFirstName = "";
                }//CustomerFirstName
                try
                {
                    CustomerLastName = dr["CustomerLastName"].ToString();
                }
                catch
                {
                    CustomerLastName = "";
                }//CustomerLastName
                try
                {
                    CustomerAddress = dr["CustomerAddress"].ToString();
                }
                catch
                {
                    CustomerAddress = "";
                }//CustomerAddress
                try
                {
                    CustomerAddress2 = dr["CustomerAddress2"].ToString();
                }
                catch
                {
                    CustomerAddress2 = "";
                }//CustomerAddress2
                try
                {
                    CustomerCity = dr["CustomerCity"].ToString();
                }
                catch
                {
                    CustomerCity = "";
                }//CustomerCity
                try
                {
                    CustomerState = dr["CustomerState"].ToString();
                }
                catch
                {
                    CustomerState = "";
                }//CustomerState
                try
                {
                    CustomerZip = dr["CustomerZip"].ToString();
                }
                catch
                {
                    CustomerZip = "";
                }//CustomerZip
                try
                {
                    CustomerCountry = dr["CustomerCountry"].ToString();
                }
                catch
                {
                    CustomerCountry = "";
                }//CustomerCountry
                try
                {
                    ProducerCode = dr["ProducerCode"].ToString();
                }
                catch
                {
                    ProducerCode = "";
                }//ProducerCode
                try
                {
                    ProducerName = dr["ProducerName"].ToString();
                }
                catch
                {
                    ProducerName = "";
                }//ProducerName
                try
                {
                    ProducerAddress = dr["ProducerAddress"].ToString();
                }
                catch
                {
                    ProducerAddress = "";
                }//ProducerAddress
                try
                {
                    ProducerAddress2 = dr["ProducerAddress2"].ToString();
                }
                catch
                {
                    ProducerAddress2 = "";
                }//ProducerAddress2
                try
                {
                    ProducerCity = dr["ProducerCity"].ToString();
                }
                catch
                {
                    ProducerCity = "";
                }//ProducerCity
                try
                {
                    ProducerState = dr["ProducerState"].ToString();
                }
                catch
                {
                    ProducerState = "";
                }//ProducerState
                try
                {
                    ProducerZipCode = dr["ProducerZipCode"].ToString();
                }
                catch
                {
                    ProducerZipCode = "";
                }//ProducerZipCode
                try
                {
                    ProducerCountry = dr["ProducerCountry"].ToString();
                }
                catch
                {
                    ProducerCountry = "";
                }//ProducerCountry
                try
                {
                    SalesChannel = dr["SalesChannel"].ToString();
                }
                catch
                {
                    SalesChannel = "";
                }//SalesChannel
                try
                {
                    ClaimLoL = dr["ClaimLimitOfLiability"].ToString();
                }
                catch
                {
                    ClaimLoL = "";
                }//ClaimLoL
                try
                {
                    ContractLoL = dr["ContractLimitOfLiability"].ToString();
                }
                catch
                {
                    ContractLoL = "";
                }//ContractLoL
                try
                {
                    DeductibleType = dr["DeductibleType"].ToString();
                }
                catch
                {
                    DeductibleType = "";
                }//DeductibleType
                try
                {
                    Deductible = dr["Deductible"].ToString();
                }
                catch
                {
                    Deductible = "";
                }//Deductible
                try
                {
                    RetailAmount = dr["RetailAmount"].ToString();
                }
                catch
                {
                    RetailAmount = "";
                }//RetailAmount
                try
                {
                    dealerCost = Convert.ToDouble(dr["ContractPrice"]);
                }
                catch
                {
                    dealerCost = 0;
                }//dealerCost
                try
                {
                    if (InsuranceType.ToUpper() == "XOL")
                    {
                        NetInsurancePremiumControl = Convert.ToDouble(dr["XOLNetInsurancePremiumControl"]);
                    }
                    else
                    {
                        NetInsurancePremiumControl = Convert.ToDouble(dr["NetInsurancePremiumControl"]);
                    }
                }
                catch
                {
                    NetInsurancePremiumControl = 0;
                }//NetInsurancePremiumControl
                try
                {
                    if (InsuranceType.ToUpper() == "XOL")
                    {
                        InsurancePremiumChange = Convert.ToDouble(dr["XOLInsurancePremiumChange"]);
                    }
                    else
                    {
                        InsurancePremiumChange = Convert.ToDouble(dr["InsurancePremiumChange"]);
                    }

                }
                catch
                {
                    InsurancePremiumChange = 0;
                }//InsurancePremiumChange
                try
                {
                    NetReserveAmountControl = Convert.ToDouble(dr["NetReserveAmountControl"]);
                }
                catch
                {
                    NetReserveAmountControl = 0;
                }//NetReserveAmountControl
                try
                {
                    ReserveAmountChange = Convert.ToDouble(dr["ReserveAmountChange"]);
                }
                catch
                {
                    ReserveAmountChange = 0;
                }//ReserveAmountChange
                try
                {
                    PremiumTax = Convert.ToDouble(dr["PremiumTax"]);
                }
                catch
                {
                    PremiumTax = 0;
                }//PremiumTax
                try
                {
                    Ceding_Insurance_Fee = Convert.ToDouble(dr["Ceding_Insurance_Fee"]);
                }
                catch
                {
                    Ceding_Insurance_Fee = 0;
                }//Ceding_Insurance_Fee
                try
                {
                    ContingencyFee = Convert.ToDouble(dr["ContingencyFee"]);
                }
                catch
                {
                    ContingencyFee = 0;
                }//ContingencyFee
                try
                {
                    ObligorFee = Convert.ToDouble(dr["ObligorFee"]);
                }
                catch
                {
                    ObligorFee = 0;
                }//ObligorFee
                try
                {
                    BalanceInsuredAmount = Convert.ToDouble(dr["BalanceInsuredAmount"]);
                }
                catch
                {
                    BalanceInsuredAmount = 0;
                }//BalanceInsuredAmount
                try
                {
                    CommercialSurcharge = Convert.ToDouble(dr["CommercialSurcharge"]);
                }
                catch
                {
                    CommercialSurcharge = 0;
                }//CommercialSurcharge
                try
                {
                    Diesel_Hybrid_Surcharge = Convert.ToDouble(dr["Diesel_Hybrid_Surcharge"]);
                }
                catch
                {
                    Diesel_Hybrid_Surcharge = 0;
                }//Diesel_Hybrid_Surcharge
                try
                {
                    FourWD_AWD_Surcharge = Convert.ToDouble(dr["AWD_4WD_Surcharge"]);
                }
                catch
                {
                    FourWD_AWD_Surcharge = 0;
                }//FourWD_AWD_Surcharge
                try
                {
                    Turbo_SuperChargeSurcharge = Convert.ToDouble(dr["Turbo_SuperChargeSurcharge"]);
                }
                catch
                {
                    Turbo_SuperChargeSurcharge = 0;
                }//Turbo_SuperChargeSurcharge
                try
                {
                    OtherSurcharges = Convert.ToDouble(dr["OtherSurcharges"]);
                }
                catch
                {
                    OtherSurcharges = 0;
                }//OtherSurcharges
                try
                {
                    CancellationDate = Convert.ToString(dr["CancellationDate"]);
                }
                catch
                {
                    CancellationDate = "";
                }//CancellationDate
                try
                {
                    CancellationFormula = "ProRate";
                }
                catch
                {
                    CancellationFormula = "";
                }//CancellationFormula
                try
                {
                    CancellationMileage = Convert.ToDouble(dr["CancellationMileage"]);
                }
                catch
                {
                    CancellationMileage = 0;
                }//CancellationMileage
                try
                {
                    CancellationPercentage = Convert.ToString(dr["CancellationPercentage"]);
                }
                catch
                {
                    CancellationPercentage = "";
                }//CancellationPercentage
                try
                {
                    ObligorID = Convert.ToString(dr["ObligorID"]);
                }
                catch
                {
                    ObligorID = "";
                }//ObligorID
                try
                {
                    ObligorName = Convert.ToString(dr["ObligorName"]);
                }
                catch
                {
                    ObligorName = "";
                }//ObligorName
                try
                {
                    ObligorState = Convert.ToString(dr["ObligorState"]);
                }
                catch
                {
                    ObligorState = "";
                }//ObligorState
                try
                {
                    ObligorAddress1 = Convert.ToString(dr["ObligorAddress1"]);
                }
                catch
                {
                    ObligorAddress1 = "";
                }//ObligorAddress1
                try
                {
                    ObligorAddress2 = Convert.ToString(dr["ObligorAddress2"]);
                }
                catch
                {
                    ObligorAddress2 = "";
                }//ObligorAddress2
                try
                {
                    ObligorCountry = Convert.ToString(dr["ObligorCountry"]);
                }
                catch
                {
                    ObligorCountry = "";
                }//ObligorCountry
                try
                {
                    formNumber = Convert.ToString(dr["FormNumber"]);
                }
                catch
                {
                    formNumber = "";
                }//formNumber
                CancellationDate = "";
                CancellationFormula = "";
                CancellationMileage = 0;
                CancellationPercentage = "";
                if (ProductType != "Motorcycles")
                {
                    CC = "";
                }
                try
                {
                    if (ReInsuranceCompanyCode.Length == 0)
                    {
                        ReInsuranceCompanyCode = "N";
                    }
                }
                catch
                {
                    ReInsuranceCompanyCode = "N";
                }





                if (DealerPolicyNumber.ToUpper().Contains("RAC") || DealerPolicyNumber.ToUpper().Contains("REPW"))
                {
                    SalesChannel = "CallCenter";
                }
                else
                {
                    SalesChannel = "Dealer";
                }

                if (ContractStatus_Transcode.ToUpper() == "CANCELLED")
                {
                    ContractStatus_Transcode = "PAID";
                }

                if (ProducerCode == "1001")
                {
                    ProducerName = "RED Auto Protection INC";
                    ProducerAddress = "5350 College Blvd";
                    ProducerCity = "Overland Park";
                    ProducerState = "KS";
                    ProducerZipCode = "66211";
                }
                /*if obligor is casc = veritas
                 if obligor is cascFL = cascFL
                 if obligor is wesco or northcost and before 6/1/2019 = redshield
                 after 6/1/2019 = veritas unless 
                 RANW and REPW = redshield
                 obligor redshield= redshield*/


                if (ContractStatus_Transcode.ToUpper() == "PAID")
                {
                    ContractStatus_Transcode = "A";
                }
                else if (ContractStatus_Transcode.ToUpper() == "CANCELLED")
                {
                    if (Convert.ToInt32(CancellationPercentage) < 1)
                    {
                        ContractStatus_Transcode = "CP";
                    }
                    else
                    {
                        ContractStatus_Transcode = "CF";
                    }
                }

                if (InsuranceType == "First Dollar")
                {
                    InsuranceType = "FD";
                }
                if (New_Used == "U")
                {
                    New_Used = "USED";
                }
                else if (New_Used == "N" || New_Used == "n")
                {
                    New_Used = "NEW";
                }
                else
                {
                    New_Used = "USED";
                }

                salesControlMapping();
                string ContractWriteOut = "";
                ContractWriteOut = ContractWriteOut + TPA.Trim() + "|";
                ContractWriteOut = ContractWriteOut + InsuranceCompanyName.Trim() + "|";
                ContractWriteOut = ContractWriteOut + ProgramName.Trim() + "|";
                ContractWriteOut = ContractWriteOut + InsuranceType.Trim() + "|";
                ContractWriteOut = ContractWriteOut + Currency.Trim() + "|";
                ContractWriteOut = ContractWriteOut + DealerPolicyNumber.Trim() + "|";
                ContractWriteOut = ContractWriteOut + "|";
                ContractWriteOut = ContractWriteOut + CustomerContractNumber.Trim() + "|";
                ContractWriteOut = ContractWriteOut + WarrantySKU.Trim() + "|";
                ContractWriteOut = ContractWriteOut + WarrantyDescription.Trim() + "|";
                ContractWriteOut = ContractWriteOut + "|";
                ContractWriteOut = ContractWriteOut + CoverageType.Trim() + "|";
                ContractWriteOut = ContractWriteOut + coverageSubType.Trim() + "|";
                ContractWriteOut = ContractWriteOut + ProductID.Trim() + "|";
                ContractWriteOut = ContractWriteOut + ContractStatus_Transcode.Trim() + "|";
                ContractWriteOut = ContractWriteOut + TermMileage.Trim() + "|";
                ContractWriteOut = ContractWriteOut + TermMonths.Trim() + "|";
                ContractWriteOut = ContractWriteOut + "|";
                ContractWriteOut = ContractWriteOut + "|";
                ContractWriteOut = ContractWriteOut + StartOfRiskType.Trim() + "|";
                try
                {
                    converter = Convert.ToDateTime(TransactionDate);
                    TransactionDate = converter.ToString("MM/dd/yyyy");
                }
                catch
                {
                }
                ContractWriteOut = ContractWriteOut + TransactionDate + "|";
                try
                {
                    converter = Convert.ToDateTime(ContractPurchaseDate);
                    ContractPurchaseDate = converter.ToString("MM/dd/yyyy");
                }
                catch
                {
                }
                ContractWriteOut = ContractWriteOut + ContractPurchaseDate + "|";
                ProductPurchaseDate = ContractPurchaseDate;
                try
                {
                    converter = Convert.ToDateTime(ProductPurchaseDate);
                    ProductPurchaseDate = converter.ToString("MM/dd/yyyy");
                }
                catch
                {
                }
                ContractWriteOut = ContractWriteOut + ProductPurchaseDate + "|";
                ContractWriteOut = ContractWriteOut + "|";

                ContractWriteOut = ContractWriteOut + "|";
                ContractWriteOut = ContractWriteOut + "|";

                try
                {
                    converter = Convert.ToDateTime(LaborCoverageEffectiveDate);
                    LaborCoverageEffectiveDate = converter.ToString("MM/dd/yyyy");
                }
                catch
                {
                }
                ContractWriteOut = ContractWriteOut + LaborCoverageEffectiveDate + "|";
                try
                {
                    converter = Convert.ToDateTime(LaborCoverageExpirationDate);
                    LaborCoverageExpirationDate = converter.ToString("MM/dd/yyyy");
                }
                catch
                {
                }
                ContractWriteOut = ContractWriteOut + LaborCoverageExpirationDate + "|";
                try
                {
                    converter = Convert.ToDateTime(PartsCoverageEffectiveDate);
                    PartsCoverageEffectiveDate = converter.ToString("MM/dd/yyyy");
                }
                catch
                {
                }
                ContractWriteOut = ContractWriteOut + PartsCoverageEffectiveDate + "|";
                try
                {
                    converter = Convert.ToDateTime(PartsCoverageExpirationDate);
                    PartsCoverageExpirationDate = converter.ToString("MM/dd/yyyy");
                }
                catch
                {
                }
                ContractWriteOut = ContractWriteOut + PartsCoverageExpirationDate + "|";
                ContractWriteOut = ContractWriteOut + ProductQuantity + "|";
                ContractWriteOut = ContractWriteOut + ProductSKU.Trim() + "|";
                ContractWriteOut = ContractWriteOut + ProductType.Trim() + "|";
                ContractWriteOut = ContractWriteOut + "|";
                ContractWriteOut = ContractWriteOut + "|";
                ContractWriteOut = ContractWriteOut + "|";
                ContractWriteOut = ContractWriteOut + "|";
                ContractWriteOut = ContractWriteOut + Make.Trim() + "|";
                ContractWriteOut = ContractWriteOut + Model.Trim() + "|";
                ContractWriteOut = ContractWriteOut + ModelYear.Trim() + "|";
                ContractWriteOut = ContractWriteOut + "|";
                ContractWriteOut = ContractWriteOut + VIN.Trim() + "|";
                ContractWriteOut = ContractWriteOut + CC.Trim() + "|";
                ContractWriteOut = ContractWriteOut + RateClass.Trim() + "|";
                ContractWriteOut = ContractWriteOut + New_Used.Trim() + "|";
                ContractWriteOut = ContractWriteOut + "|";
                ContractWriteOut = ContractWriteOut + ProductPurchasePrice.Trim() + "|";
                ContractWriteOut = ContractWriteOut + "|";
                ContractWriteOut = ContractWriteOut + MileageAtPurchase.Trim() + "|";
                ContractWriteOut = ContractWriteOut + MileageAtCoverageStart.Trim() + "|";
                ContractWriteOut = ContractWriteOut + "|";
                ContractWriteOut = ContractWriteOut + "|";
                ContractWriteOut = ContractWriteOut + "|";
                ContractWriteOut = ContractWriteOut + "|";
                ContractWriteOut = ContractWriteOut + "|";
                ContractWriteOut = ContractWriteOut + ContractExpireMileage.Trim() + "|";
                ContractWriteOut = ContractWriteOut + "|";
                ContractWriteOut = ContractWriteOut + UseType.Trim() + "|";
                ContractWriteOut = ContractWriteOut + "|";
                ContractWriteOut = ContractWriteOut + CustomerFirstName.Trim() + "|";
                ContractWriteOut = ContractWriteOut + CustomerLastName.Trim() + "|";
                ContractWriteOut = ContractWriteOut + CustomerAddress.Trim() + "|";
                ContractWriteOut = ContractWriteOut + CustomerAddress2.Trim() + "|";
                ContractWriteOut = ContractWriteOut + CustomerCity.Trim() + "|";
                ContractWriteOut = ContractWriteOut + CustomerState.Trim() + "|";
                ContractWriteOut = ContractWriteOut + CustomerZip.Trim() + "|";
                ContractWriteOut = ContractWriteOut + CustomerCountry.Trim() + "|";
                ContractWriteOut = ContractWriteOut + "|";
                ContractWriteOut = ContractWriteOut + "|";
                ContractWriteOut = ContractWriteOut + "|";
                ContractWriteOut = ContractWriteOut + ProducerCode.Trim() + "|";
                ContractWriteOut = ContractWriteOut + ProducerName.Trim() + "|";
                ContractWriteOut = ContractWriteOut + ProducerAddress + "|";
                ContractWriteOut = ContractWriteOut + ProducerAddress2 + "|";
                ContractWriteOut = ContractWriteOut + ProducerCity.Trim() + "|";
                ContractWriteOut = ContractWriteOut + ProducerState.Trim() + "|";
                ContractWriteOut = ContractWriteOut + ProducerZipCode + "|";
                ContractWriteOut = ContractWriteOut + ProducerCountry.Trim() + "|";
                ContractWriteOut = ContractWriteOut + SalesChannel.Trim() + "|";
                ContractWriteOut = ContractWriteOut + "|";
                ContractWriteOut = ContractWriteOut + ClaimLoL.Trim() + "|";
                ContractWriteOut = ContractWriteOut + ContractLoL.Trim() + "|";
                ContractWriteOut = ContractWriteOut + "|";
                ContractWriteOut = ContractWriteOut + "|";
                ContractWriteOut = ContractWriteOut + "|";
                ContractWriteOut = ContractWriteOut + "|";
                ContractWriteOut = ContractWriteOut + DeductibleType.Trim() + "|";
                ContractWriteOut = ContractWriteOut + Deductible.Trim() + "|";
                ContractWriteOut = ContractWriteOut + "|";
                ContractWriteOut = ContractWriteOut + ReInsuranceCompanyCode + "|";
                ContractWriteOut = ContractWriteOut + ReInsuranceCompanyName + "|";
                ContractWriteOut = ContractWriteOut + ReinsuranceType + "|";
                ContractWriteOut = ContractWriteOut + "|";
                ContractWriteOut = ContractWriteOut + "|";
                ContractWriteOut = ContractWriteOut + "|";
                ContractWriteOut = ContractWriteOut + "|";
                ContractWriteOut = ContractWriteOut + RetailAmount.Trim() + "|";
                ContractWriteOut = ContractWriteOut + dealerCost + "|";
                ContractWriteOut = ContractWriteOut + NetInsurancePremiumControl + "|";
                ContractWriteOut = ContractWriteOut + InsurancePremiumChange + "|";
                ContractWriteOut = ContractWriteOut + NetReserveAmountControl + "|";
                ContractWriteOut = ContractWriteOut + ReserveAmountChange + "|";
                ContractWriteOut = ContractWriteOut + PremiumTax + "|";
                ContractWriteOut = ContractWriteOut + "|";
                ContractWriteOut = ContractWriteOut + "|";
                ContractWriteOut = ContractWriteOut + "|";
                ContractWriteOut = ContractWriteOut + "|";
                ContractWriteOut = ContractWriteOut + "|";
                ContractWriteOut = ContractWriteOut + "|";
                ContractWriteOut = ContractWriteOut + "|";
                ContractWriteOut = ContractWriteOut + Ceding_Insurance_Fee + "|";
                ContractWriteOut = ContractWriteOut + "|";
                ContractWriteOut = ContractWriteOut + ContingencyFee + "|";
                ContractWriteOut = ContractWriteOut + ObligorFee + "|";
                ContractWriteOut = ContractWriteOut + "|";
                ContractWriteOut = ContractWriteOut + CommercialSurcharge + "|";
                ContractWriteOut = ContractWriteOut + Diesel_Hybrid_Surcharge + "|";
                ContractWriteOut = ContractWriteOut + FourWD_AWD_Surcharge + "|";
                ContractWriteOut = ContractWriteOut + Turbo_SuperChargeSurcharge + "|";
                ContractWriteOut = ContractWriteOut + OtherSurcharges + "|";
                ContractWriteOut = ContractWriteOut + "|";
                ContractWriteOut = ContractWriteOut + CancellationDate.Trim() + "|";
                ContractWriteOut = ContractWriteOut + CancellationFormula.Trim() + "|";
                ContractWriteOut = ContractWriteOut + "|";
                ContractWriteOut = ContractWriteOut + CancellationMileage + "|";
                ContractWriteOut = ContractWriteOut + CancellationPercentage.Trim() + "|";
                ContractWriteOut = ContractWriteOut + "|";
                ContractWriteOut = ContractWriteOut + ObligorID.Trim() + "|";
                ContractWriteOut = ContractWriteOut + ObligorName.Trim() + "|";
                ContractWriteOut = ContractWriteOut + ObligorState.Trim() + "|";
                ContractWriteOut = ContractWriteOut + ObligorAddress1.Trim() + "|";
                ContractWriteOut = ContractWriteOut + ObligorAddress2.Trim() + "|";
                ContractWriteOut = ContractWriteOut + ObligorCountry.Trim() + "|";
                ContractWriteOut = ContractWriteOut + formNumber + "|";
                ContractWriteOut = ContractWriteOut + "|";
                ContractWriteOut = ContractWriteOut + "|";
                ContractWriteOut = ContractWriteOut.Replace(",", "");
                streamWriterContract.WriteLine(ContractWriteOut);

            }
            lblProgressBar.Text = "Executing Querry";
            lblProgressBar.Refresh();
            sql = "select distinct c.clipID as clipID, " +
                "ctpa.TPA as TPA,  " +
                "'Wesco Insurance Company' as InsuranceCompanyName," +
                "c.ProgramName, " +
                "c.contractno as DealerPolicyNumber, " +
                "'' as DealerPolicyNumberSuffix, " +
                "ccp.ClipType as InsuranceType, " +
                "'USD' as Currency, " +
                "c.ContractNo as CustomerContractNo, " +
                "cc.ContractCode as WarrantySKU, " +
                "cc.Program + ' ' + cc.PlanType as WarrantyDescription, " +
                "'' as CoverageGroup, " +
                "'VSC' as CoverageType," +
                "'' as CoverageSubType, " +
                "cc.ContractCode as ProductID, " +
                "'Canceled' as ContractStatus_Transcode, " +
                "c.TermMile as TermMileage, " +
                "c.TermMonth as TermMonths, " +
                "'' as TermHours, " +
                "'' as ManufacturerTermMonths, " +
                "'Date of Purchase' as StartOfRiskType, " +
                "ccc.canceldate as TransactionDate, " +
                "SaleDate as ContractPurchaseDate, " +
                "SaleDate as ProductPurchaseDate, " +
                "'' as VehicleInServiceDate, " +
                "'' as ManufacturerEffectiveDate, " +
                "'' as ManufacturerExpirationDate, " +
                "c.EffDate as LaborCoverageEffectiveDate, " +
                "c.ExpDate as LaborCoverageExpirationDate, " +
                "c.EffDate as PartsCoverageEffectiveDate, " +
                "c.ExpDate as PartsCoverageExpirationDate, " +
                "'1' as ProductQuantity, " +
                "cc.ContractCode as ProductSKU, " +
                "case " +
                "when c.programid = 10 or c.programid = 63 then  " +
                "case when c.PlanTypeID = 79 or c.PlanTypeID = 80 then " +
                "'Trailers' " +
                "when c.plantypeid > 98 or c.plantypeid < 109 then " +
                "'Trailers' " +
                "else " +
                "'RV' " +
                "end " +
                "when c.programid = 9 or c.ProgramID = 62 then " +
                "'Motorcycles' " +
                "else  " +
                "'Auto' " +
                "end as ProductType, '' as ProductSubType, '' as HoursAtPurchase, '' as LoanOrLease, '' as LoanToValue, c.Make, c.Model, c.year as ModelYear, '' as Category, c.VIN, c.EngineCC as cc, " +
                "c.Class as RateClass, c.NewUsed as New_Used, '' as NADA_MSRPValue, '0.00' as ProductPurchasePrice, '' as AmountFinanced, SaleMile as MileageAtPurchase, c.EffMile as MileageAtCoverageStart, " +
                "'' as LoanAPR, '' as AgreementType, '' as SkipBenefitQuantity, '' as DeliquentPaymentBenefitQuantity, '' as ContractExpireHours, c.ExpMile as ContractExpireMileage, '' as UpperPriceBand, " +
                "case " +
                "when commercial<> 0 then " +
                "'Commercial' " +
                "else " +
                "'Personal' " +
                "end as UseType, '' as CompanyNameOfPurchaser, c.FName as CustomerFirstName, c.LName as CustomerLastName, c.Addr1 as CustomerAddress, c.Addr2 as CustomerAddress2, " +
                "c.city as CustomerCity, c.State as CustomerState, c.Zip as CustomerZip, 'USA' as CustomerCountry, '' as CustomerEmail, '' as CustomerPhone, '' as ProducerType, " +
                "d.DealerNo as ProducerCode,(d.DealerName + ' - ' + d.dealerNo) as ProducerName, d.Addr1 as ProducerAddress, d.Addr2 as ProducerAddress2, d.city as ProducerCity, d.State as ProducerState, d.zip as ProducerZipCode, " +
                "'USA' as ProducerCountry,  " +
                "case " +
                "when d.dealerno = '1001' then " +
                "'Call Center' " +
                "when left(c.contractno, 3) = 'REP' then " +
                "'Call Center' " +
                "else " +
                "'Dealer' " +
                "end as SalesChannel, '' as BookID, cll.ClaimLOL as ClaimLimitOfLiability, cll.ContractLOL as ContractLimitOfLiability, '' as ClaimCountLimit, '' as ClaimType, " +
                "'' as ContractPaymentStatus, '' as PaidFlag, 'Standard' as DeductibleType, dd.DeductAmt as Deductible, '' as DeductibleAtProducer, c.ReInsuranceCompanyCode as ReInsuranceCompanyCode, " +
                "c.ReInsuranceCompanyName as ReInsuranceCompanyName, c.ReinsuranceType as ReinsuranceType, '' as ManagementFee, '' as RiskPoolFee, " +
                "" +
                "" +
                "( (case when r.Amt is null then 0 else r.amt end) - (case when r.cancelAmt is null then 0 else r.cancelamt end) " +
                " + (case when s.Amt is null then 0 else s.amt end) - (case when s.cancelAmt is null then 0 else s.cancelamt end) " +
                " + (case when pt.Amt is null then 0 else pt.Amt end) - (case when pt.cancelAmt is null then 0 else pt.cancelAmt end) " +
                " + (case when o.Amt is null then 0 else o.amt end) - (case when o.cancelAmt is null then 0 else o.cancelamt end) " +
                " + (case when cf.Amt is null then 0 else cf.amt end) - (case when cf.cancelAmt is null then 0 else cf.cancelamt end)  " +
                " + (case when cy.Amt is null then 0 else cy.amt end) - (case when cy.cancelAmt is null then 0 else cy.cancelamt end))  " +
                " as DealerCost, " +
                "" +
                "" +
                "c.CustomerCost as RetailAmount, c.MoxyDealerCost as ContractPrice, " +
                "" +
                "((case when pt.cancelAmt is null then 0 else pt.cancelAmt end) +" +
                "(case when cf.cancelAmt is null then 0 else cf.cancelamt end) + " +
                " (case when s.cancelAmt is null then 0 else s.cancelamt end) +" +
                "(case when r.cancelAmt is null then 0 else r.cancelamt end)) " +
                "as InsurancePremiumChange, " +
                "" +
                "(case when pt.cancelAmt is null then 0 else pt.cancelAmt end) +" +
                "(case when cf.cancelAmt is null then 0 else cf.cancelamt end) " +
                "as XOLInsurancePremiumChange, " +

                "( (case when r.Amt is null then 0 else r.amt end) - (case when r.cancelAmt is null then 0 else r.cancelamt end) " +
                " + (case when s.Amt is null then 0 else s.amt end) - (case when s.cancelAmt is null then 0 else s.cancelamt end) " +
                " + (case when pt.Amt is null then 0 else pt.Amt end) - (case when pt.cancelAmt is null then 0 else pt.cancelAmt end) " +
                " + (case when cf.Amt is null then 0 else cf.amt end) - (case when cf.cancelAmt is null then 0 else cf.cancelamt end)  " +
                ")" +
                "as NetInsurancePremiumControl, " +
                " " +
                "(  " +
                " + (case when pt.Amt is null then 0 else pt.Amt end) - (case when pt.cancelAmt is null then 0 else pt.cancelAmt end) " +
                " + (case when cf.Amt is null then 0 else cf.amt end) - (case when cf.cancelAmt is null then 0 else cf.cancelamt end)  " +
                ") " +
                "as XOLNetInsurancePremiumControl, " +
                " " +
                " ((case when r.cancelAmt is null then 0 else r.cancelamt end) + (case when s.cancelAmt is null then 0 else s.cancelamt end)) as ReserveAmountChange, " +
                " (case when r.Amt is null then 0 else r.amt end) -(case when r.cancelAmt is null then 0 else r.cancelamt end) + (case when s.Amt is null then 0 else s.amt end) - (case when s.cancelAmt is null then 0 else s.cancelamt end) as NetReserveAmountControl, " +

                " (case when pt.cancelAmt is null then 0 else pt.cancelAmt end)  as PremiumTax, " +
                " " +
                "'' as RiskFees, '' as IPTRate, '' as ServiceFees, '' as Adminamount, '' as BaseReserveAmount,  '' as SurchargeReserveAmount, '' as BrokerFee, " +

                "  (case when cf.cancelamt is null then 0 else cf.cancelamt end) as Ceding_Insurance_Fee, " +

                "'' as Commission, " +
                " (case when cy.cancelAmt is null then 0 else cy.cancelamt end) as ContingencyFee, " +
                "  (case when o.cancelAmt is null then 0 else o.cancelamt end) as ObligorFee, '0' as BalanceInsuredAmount, " +
                " (case when comm.cancelAmt is null then 0 else comm.cancelamt end) as CommercialSurcharge, " +
                " (case when DH.cancelAmt is null then 0 else DH.cancelamt end) as Diesel_Hybrid_Surcharge, " +
                " (case when awd.cancelAmt is null then 0 else awd.cancelamt end) as AWD_4WD_Surcharge, " +
                " (case when t.cancelAmt is null then 0 else t.cancelamt end) as Turbo_SuperChargeSurcharge, " +
                " ((case when s.cancelamt is null then 0 else s.cancelAmt end) - " +
                " (case when comm.cancelAmt is null then 0 else comm.cancelamt end) - " +
                " (case when DH.cancelAmt is null then 0 else DH.cancelamt end) - " +
                " (case when awd.cancelAmt is null then 0 else awd.cancelamt end) - " +
                " (case when t.cancelAmt is null then 0 else t.cancelamt end)) as OtherSurcharges, '' as CancellationClaimAdjusment, " +
                "" +
                "" +
                "ccc.canceldate as CancellationDate, 'ProRated' as CancellationFormula, '' as CancellationHours, ccc.CancelMile as CancellationMileage, " +
                "ccc.CancelFactor as CancellationPercentage, '' as CancellationReason, c.ObligorID as ObligorID, ob.ObligorName as ObligorName, " +
                " ob.State as ObligorState,  " +
                "'' as ObligorAddress1, '' as ObligorAddress2, 'USA' as ObligorCountry, c.FormNo as FormNumber, '' as TPAAccountManager, '' as TPASalesAgent, '' as TPASalesRep " +
                "from contract c " +
                "left join program p on c.ProgramID = p.ProgramID " +
                "left join inscarrier ic on ic.InsCarrierID = c.InsCarrierID " +
                "left join contractcode cc on cc.ProgramID = c.ProgramID and cc.PlanTypeID = c.PlanTypeID " +
                "left join dealer d on d.dealerid = c.dealerid " +
                "left join deductible dd on dd.DeductID = c.DeductID " +
                " " +
                "left join(select contractid, sum(amt) as Amt, sum(cancelamt) as cancelamt from contractamt ca " +
                "inner join ratetype rt on rt.RateTypeID = ca.RateTypeID where rt.RateCategoryID = 4 group by contractid) as R on c.contractid = r.ContractID " +
                " " +
                "left join(select contractid, sum(amt) as Amt, sum(cancelamt) as cancelamt from contractamt ca " +
                "inner join ratetype rt on rt.RateTypeID = ca.RateTypeID where rt.RateCategoryID = 3 group by contractid) as s on c.contractid = s.ContractID " +
                "left join(select contractid, sum(amt) as Amt, sum(cancelamt) as cancelamt from contractamt ca " +
                "where ca.RatetypeID = 8" +
                "group by contractid) as CF on c.contractid = cf.ContractID " +
                "left join(select contractid, sum(amt) as Amt, sum(cancelamt) as cancelamt from contractamt ca " +
                "where ca.RatetypeID = 6 " +
                "group by contractid) as pt on c.contractid = pt.ContractID " +
                "left join(select contractid, sum(amt) as Amt, sum(cancelamt) as cancelamt from contractamt ca " +
                "where ca.RatetypeID = 9 " +
                "group by contractid) as o on c.contractid = o.ContractID " +
                "left join(select contractid, sum(amt) as Amt, sum(cancelamt) as CancelAmt from contractamt ca " +
                "where ca.RatetypeID = 7" +
                "group by contractid) as cy on c.contractid = cy.ContractID " +
                "left join(select contractid, sum(amt) as Amt, sum(cancelamt) as cancelamt from contractamt ca " +
                "where ca.RatetypeID = 21" +
                "group by contractid) as comm on c.contractid = comm.ContractID " +
                "left join(select contractid, sum(amt) as Amt, sum(cancelamt) as cancelamt from contractamt ca " +
                "where (ca.RatetypeID = 19 or ca.RateTypeID = 39) group by contractid) as DH on c.contractid = DH.ContractID " +
                "left join(select contractid, sum(amt) as Amt, sum(cancelamt) as cancelamt from contractamt ca " +
                "where ca.RatetypeID = 18" +
                "group by contractid) as AWD on c.contractid = AWD.ContractID " +
                "left join(select contractid, sum(amt) as Amt, sum(cancelamt) as cancelamt from contractamt ca " +
                "where ca.RatetypeID = 20 " +
                "group by contractid) as t on c.contractid = t.ContractID " +
                "left join obligor ob on c.ObligorID = ob.ObligorID " +
                "left join contractCancel ccc on ccc.contractID = c.contractID " +
                "left join PlanType plt on plt.PlanTypeID = c.PlanTypeID " +
                "left join contractClip ccp on ccp.ClipId = c.clipID " +
                "left join ContractLimitofLiabilty  cll on cll.PlanTypeID = c.PlanTypeID and cll.ProgramID = c.ProgramID " +
                "left join ContractResinsurance cri on cri.ReinsuranceID = c.ReinsuranceID " +
                "left join contractTPA ctpa on ctpa.TPAID = c.TPAID " +
                "where(c.status = 'Cancelled') " +
                "and ccc.canceldate >= '" + dtpStartDate.Value + "' " +
                "and ccc.canceldate <= '" + dtpEndDate.Value + "' " +
                "and c.INSCancelTransmissionDate is null " +
                "and (c.ProgramID < 47 or c.ProgramID > 60) " +
                "and c.InsCarrierID = 1 " +
                "and c.contractid in (select contractid from contractamt where ratetypeid = 1)" +
                "and not c.DatePaid is null " +
                "and not c.contractno in (select contractno from AmTrustContractAdd where AddOrRemove = 'R')" +
                "or c.contractno in (select contractno from AmTrustContractAdd where AddOrRemove = 'A' and Status = 'Cancelled')" +
                "order by c.contractno ";
            dt = contractMappingDBO.dboInportInformation(sql);
            lblProgressBar.Text = "Processing AmTrust Cancels";
            lblProgressBar.Refresh();
            tracker = dt.Rows.Count;
            lbCancel.Text = "Cancel Contracts:" + tracker;
            lbCancel.Refresh();
            progressBar1.Value = 0;
            progressBar1.Maximum = tracker;
            progressBar1.Minimum = 0;
            progressBar1.Visible = true;
            progressBar1.Step = 1;
            foreach (DataRow dr in dt.Rows)
            {
                progressBar1.PerformStep();
                try
                {
                    clipID = Convert.ToInt16(dr["clipID"]);
                }
                catch
                {
                    clipID = 0;
                }//clipID
                try
                {
                    ReInsuranceCompanyName = Convert.ToString(dr["ReInsuranceCompanyName"]);
                }
                catch
                {
                    ReInsuranceCompanyName = "";
                }//ReInsuranceCompanyName
                try
                {
                    ReinsuranceType = Convert.ToString(dr["ReinsuranceType"]);
                }
                catch
                {
                    ReinsuranceType = "";
                }//ReinsuranceType
                try
                {
                    ReInsuranceCompanyCode = Convert.ToString(dr["ReInsuranceCompanyCode"]);
                }
                catch
                {
                    ReInsuranceCompanyCode = "";
                }//ReInsuranceCompanyCode
                try
                {
                    TPA = dr["TPA"].ToString();
                }
                catch
                {
                    TPA = "";
                }//TPA
                try
                {
                    InsuranceCompanyName = dr["InsuranceCompanyName"].ToString();
                }
                catch
                {
                    InsuranceCompanyName = "";
                }//InsuranceCompanyName
                try
                {
                    ProgramName = dr["ProgramName"].ToString();
                }
                catch
                {
                    ProgramName = "";
                }//ProgramName
                try
                {
                    DealerPolicyNumber = dr["DealerPolicyNumber"].ToString();
                }
                catch
                {
                    DealerPolicyNumber = "";
                }//DealerPolicyNumber
                try
                {
                    InsuranceType = dr["InsuranceType"].ToString();
                }
                catch
                {
                    InsuranceType = "FD";
                }//InsuranceType
                try
                {
                    Currency = dr["Currency"].ToString();
                }
                catch
                {
                    Currency = "";
                }//Currency
                try
                {
                    CustomerContractNumber = dr["CustomerContractNo"].ToString();
                }
                catch
                {
                    CustomerContractNumber = "";
                }//CustomerContractNumber
                try
                {
                    WarrantySKU = dr["WarrantySKU"].ToString();
                }
                catch
                {
                    WarrantySKU = "";
                }//WarrantySKU
                try
                {
                    WarrantyDescription = Convert.ToString(dr["WarrantyDescription"]);
                }
                catch
                {
                    WarrantyDescription = "";
                }//WarrantyDescription
                try
                {
                    CoverageType = dr["CoverageType"].ToString();
                }
                catch
                {
                    CoverageType = "";
                }//CoverageType
                try
                {
                    ProductID = dr["ProductID"].ToString();
                }
                catch
                {
                    ProductID = "";
                }//ProductID
                try
                {
                    ContractStatus_Transcode = dr["ContractStatus_Transcode"].ToString();
                }
                catch
                {
                    ContractStatus_Transcode = "";
                }//ContractStatus_Transcode
                try
                {
                    TermMileage = dr["TermMileage"].ToString();
                }
                catch
                {
                    TermMileage = "";
                }//TermMileage
                try
                {
                    TermMonths = dr["TermMonths"].ToString();
                }
                catch
                {
                    TermMonths = "";
                }//TermMonths
                try
                {
                    StartOfRiskType = dr["StartOfRiskType"].ToString();
                }
                catch
                {
                    StartOfRiskType = "";
                }//StartOfRiskType
                try
                {
                    TransactionDate = dr["TransactionDate"].ToString();
                }
                catch
                {
                    TransactionDate = "";
                }//TransactionDate
                try
                {
                    ContractPurchaseDate = dr["ContractPurchaseDate"].ToString();
                }
                catch
                {
                    ContractPurchaseDate = "";
                }//ContractPurchaseDate
                try
                {
                    ProductPurchaseDate = dr["ProductPurchaseDate"].ToString();
                }
                catch
                {
                    ProductPurchaseDate = "";
                }//ProductPurchaseDate
                try
                {
                    LaborCoverageEffectiveDate = dr["LaborCoverageEffectiveDate"].ToString();
                }
                catch
                {
                    LaborCoverageEffectiveDate = "";
                }//LaborCoverageEffectiveDate
                try
                {
                    LaborCoverageExpirationDate = dr["LaborCoverageExpirationDate"].ToString();
                }
                catch
                {
                    LaborCoverageExpirationDate = "";
                }//LaborCoverageExpirationDate
                try
                {
                    PartsCoverageEffectiveDate = dr["PartsCoverageEffectiveDate"].ToString();
                }
                catch
                {
                    PartsCoverageEffectiveDate = "";
                }//PartsCoverageEffectiveDate
                try
                {
                    PartsCoverageExpirationDate = dr["PartsCoverageExpirationDate"].ToString();
                }
                catch
                {
                    PartsCoverageExpirationDate = "";
                }//PartsCoverageExpirationDate
                try
                {
                    ProductQuantity = Convert.ToInt32(dr["ProductQuantity"]);
                }
                catch
                {
                    ProductQuantity = 0;
                }//ProductQuantity
                try
                {
                    ProductSKU = dr["ProductSKU"].ToString();
                }
                catch
                {
                    ProductSKU = "";
                }//ProductSKU
                try
                {
                    ProductType = dr["ProductType"].ToString();
                }
                catch
                {
                    ProductType = "";
                }//ProductType
                try
                {
                    Make = dr["Make"].ToString();
                }
                catch
                {
                    Make = "";
                }//Make
                try
                {
                    Model = dr["Model"].ToString();
                }
                catch
                {
                    Model = "";
                }//Model
                try
                {
                    ModelYear = dr["ModelYear"].ToString();
                }
                catch
                {
                    ModelYear = "";
                }//ModelYear
                try
                {
                    VIN = dr["VIN"].ToString();
                }
                catch
                {
                    VIN = "";
                }//VIN
                try
                {
                    CC = dr["cc"].ToString();
                }
                catch
                {
                    CC = "";
                }//CC
                try
                {
                    RateClass = dr["RateClass"].ToString();
                }
                catch
                {
                    RateClass = "";
                }//RateClass
                try
                {
                    New_Used = dr["New_Used"].ToString();
                }
                catch
                {
                    New_Used = "";
                }//New_Used
                try
                {
                    ProductPurchasePrice = dr["ProductPurchasePrice"].ToString();
                }
                catch
                {
                    ProductPurchasePrice = "";
                }//ProductPurchasePrice
                try
                {
                    MileageAtPurchase = dr["MileageAtPurchase"].ToString();
                }
                catch
                {
                    MileageAtPurchase = "";
                }//MileageAtPurchase
                try
                {
                    MileageAtCoverageStart = dr["MileageAtCoverageStart"].ToString();
                }
                catch
                {
                    MileageAtCoverageStart = "";
                }//MileageAtCoverageStart
                try
                {
                    ContractExpireMileage = dr["ContractExpireMileage"].ToString();
                }
                catch
                {
                    ContractExpireMileage = "";
                }//ContractExpireMileage
                try
                {
                    UseType = dr["UseType"].ToString();
                }
                catch
                {
                    UseType = "";
                }//UseType
                try
                {
                    CustomerFirstName = dr["CustomerFirstName"].ToString();
                }
                catch
                {
                    CustomerFirstName = "";
                }//CustomerFirstName
                try
                {
                    CustomerLastName = dr["CustomerLastName"].ToString();
                }
                catch
                {
                    CustomerLastName = "";
                }//CustomerLastName
                try
                {
                    CustomerAddress = dr["CustomerAddress"].ToString();
                }
                catch
                {
                    CustomerAddress = "";
                }//CustomerAddress
                try
                {
                    CustomerAddress2 = dr["CustomerAddress2"].ToString();
                }
                catch
                {
                    CustomerAddress2 = "";
                }//CustomerAddress2
                try
                {
                    CustomerCity = dr["CustomerCity"].ToString();
                }
                catch
                {
                    CustomerCity = "";
                }//CustomerCity
                try
                {
                    CustomerState = dr["CustomerState"].ToString();
                }
                catch
                {
                    CustomerState = "";
                }//CustomerState
                try
                {
                    CustomerZip = dr["CustomerZip"].ToString();
                }
                catch
                {
                    CustomerZip = "";
                }//CustomerZip
                try
                {
                    CustomerCountry = dr["CustomerCountry"].ToString();
                }
                catch
                {
                    CustomerCountry = "";
                }//CustomerCountry
                try
                {
                    ProducerCode = dr["ProducerCode"].ToString();
                }
                catch
                {
                    ProducerCode = "";
                }//ProducerCode
                try
                {
                    ProducerName = dr["ProducerName"].ToString();
                }
                catch
                {
                    ProducerName = "";
                }//ProducerName
                try
                {
                    ProducerAddress = dr["ProducerAddress"].ToString();
                }
                catch
                {
                    ProducerAddress = "";
                }//ProducerAddress
                try
                {
                    ProducerAddress2 = dr["ProducerAddress2"].ToString();
                }
                catch
                {
                    ProducerAddress2 = "";
                }//ProducerAddress2
                try
                {
                    ProducerCity = dr["ProducerCity"].ToString();
                }
                catch
                {
                    ProducerCity = "";
                }//ProducerCity
                try
                {
                    ProducerState = dr["ProducerState"].ToString();
                }
                catch
                {
                    ProducerState = "";
                }//ProducerState
                try
                {
                    ProducerZipCode = dr["ProducerZipCode"].ToString();
                }
                catch
                {
                    ProducerZipCode = "";
                }//ProducerZipCode
                try
                {
                    ProducerCountry = dr["ProducerCountry"].ToString();
                }
                catch
                {
                    ProducerCountry = "";
                }//ProducerCountry
                try
                {
                    SalesChannel = dr["SalesChannel"].ToString();
                }
                catch
                {
                    SalesChannel = "";
                }//SalesChannel
                try
                {
                    ClaimLoL = dr["ClaimLimitOfLiability"].ToString();
                }
                catch
                {
                    ClaimLoL = "";
                }//ClaimLoL
                try
                {
                    ContractLoL = dr["ContractLimitOfLiability"].ToString();
                }
                catch
                {
                    ContractLoL = "";
                }//ContractLoL
                try
                {
                    DeductibleType = dr["DeductibleType"].ToString();
                }
                catch
                {
                    DeductibleType = "";
                }//DeductibleType
                try
                {
                    Deductible = dr["Deductible"].ToString();
                }
                catch
                {
                    Deductible = "";
                }//Deductible
                try
                {
                    RetailAmount = dr["RetailAmount"].ToString();
                }
                catch
                {
                    RetailAmount = "";
                }//RetailAmount
                try
                {
                    dealerCost = (-1 * Convert.ToDouble(dr["ContractPrice"]));
                }
                catch
                {
                    dealerCost = 0;
                }//dealerCost
                try
                {
                    if (InsuranceType.ToUpper() == "XOL")
                    {
                        NetInsurancePremiumControl = Convert.ToDouble(dr["XOLNetInsurancePremiumControl"]);
                    }
                    else
                    {
                        NetInsurancePremiumControl = Convert.ToDouble(dr["NetInsurancePremiumControl"]);
                    }
                }
                catch
                {
                    NetInsurancePremiumControl = 0;
                }//NetInsurancePremiumControl
                try
                {
                    if (InsuranceType.ToUpper() == "XOL")
                    {
                        InsurancePremiumChange = Convert.ToDouble(dr["XOLInsurancePremiumChange"]);
                    }
                    else
                    {
                        InsurancePremiumChange = Convert.ToDouble(dr["InsurancePremiumChange"]);
                    }

                }
                catch
                {
                    InsurancePremiumChange = 0;
                }//InsurancePremiumChange
                try
                {
                    NetReserveAmountControl = Convert.ToDouble(dr["NetReserveAmountControl"]);
                }
                catch
                {
                    NetReserveAmountControl = 0;
                }//NetReserveAmountControl
                try
                {
                    ReserveAmountChange = Convert.ToDouble(dr["ReserveAmountChange"]);
                }
                catch
                {
                    ReserveAmountChange = 0;
                }//ReserveAmountChange
                try
                {
                    PremiumTax = Convert.ToDouble(dr["PremiumTax"]);
                }
                catch
                {
                    PremiumTax = 0;
                }//PremiumTax
                try
                {
                    Ceding_Insurance_Fee = Convert.ToDouble(dr["Ceding_Insurance_Fee"]);
                }
                catch
                {
                    Ceding_Insurance_Fee = 0;
                }//Ceding_Insurance_Fee
                try
                {
                    ContingencyFee = Convert.ToDouble(dr["ContingencyFee"]);
                }
                catch
                {
                    ContingencyFee = 0;
                }//ContingencyFee
                try
                {
                    ObligorFee = Convert.ToDouble(dr["ObligorFee"]);
                }
                catch
                {
                    ObligorFee = 0;
                }//ObligorFee
                try
                {
                    BalanceInsuredAmount = Convert.ToDouble(dr["BalanceInsuredAmount"]);
                }
                catch
                {
                    BalanceInsuredAmount = 0;
                }//BalanceInsuredAmount
                try
                {
                    CommercialSurcharge = Convert.ToDouble(dr["CommercialSurcharge"]);
                }
                catch
                {
                    CommercialSurcharge = 0;
                }//CommercialSurcharge
                try
                {
                    Diesel_Hybrid_Surcharge = Convert.ToDouble(dr["Diesel_Hybrid_Surcharge"]);
                }
                catch
                {
                    Diesel_Hybrid_Surcharge = 0;
                }//Diesel_Hybrid_Surcharge
                try
                {
                    FourWD_AWD_Surcharge = Convert.ToDouble(dr["AWD_4WD_Surcharge"]);
                }
                catch
                {
                    FourWD_AWD_Surcharge = 0;
                }//FourWD_AWD_Surcharge
                try
                {
                    Turbo_SuperChargeSurcharge = Convert.ToDouble(dr["Turbo_SuperChargeSurcharge"]);
                }
                catch
                {
                    Turbo_SuperChargeSurcharge = 0;
                }//Turbo_SuperChargeSurcharge
                try
                {
                    OtherSurcharges = Convert.ToDouble(dr["OtherSurcharges"]);
                }
                catch
                {
                    OtherSurcharges = 0;
                }//OtherSurcharges
                try
                {
                    CancellationDate = Convert.ToString(dr["CancellationDate"]);
                }
                catch
                {
                    CancellationDate = "";
                }//CancellationDate
                try
                {
                    CancellationFormula = Convert.ToString(dr["CancellationFormula"]);
                }
                catch
                {
                    CancellationFormula = "";
                }//CancellationFormula
                try
                {
                    CancellationMileage = Convert.ToDouble(dr["CancellationMileage"]);
                }
                catch
                {
                    CancellationMileage = 0;
                }//CancellationMileage
                try
                {
                    CancellationPercentage = Convert.ToString(dr["CancellationPercentage"]);
                }
                catch
                {
                    CancellationPercentage = "";
                }//CancellationPercentage
                try
                {
                    ObligorID = Convert.ToString(dr["ObligorID"]);
                }
                catch
                {
                    ObligorID = "";
                }//ObligorID
                try
                {
                    ObligorName = Convert.ToString(dr["ObligorName"]);
                }
                catch
                {
                    ObligorName = "";
                }//ObligorName
                try
                {
                    ObligorState = Convert.ToString(dr["ObligorState"]);
                }
                catch
                {
                    ObligorState = "";
                }//ObligorState
                try
                {
                    ObligorAddress1 = Convert.ToString(dr["ObligorAddress1"]);
                }
                catch
                {
                    ObligorAddress1 = "";
                }//ObligorAddress1
                try
                {
                    ObligorAddress2 = Convert.ToString(dr["ObligorAddress2"]);
                }
                catch
                {
                    ObligorAddress2 = "";
                }//ObligorAddress2
                try
                {
                    ObligorCountry = Convert.ToString(dr["ObligorCountry"]);
                }
                catch
                {
                    ObligorCountry = "";
                }//ObligorCountry
                try
                {
                    formNumber = Convert.ToString(dr["FormNumber"]);
                }
                catch
                {
                    formNumber = "";
                }//formNumber

                if (ProductType != "Motorcycles")
                {
                    CC = "";
                }
                if (DealerPolicyNumber.ToUpper().Contains("RAC") || DealerPolicyNumber.ToUpper().Contains("REPW"))
                {
                    SalesChannel = "CallCenter";
                }
                else
                {
                    SalesChannel = "Dealer";
                }
                if (ProducerCode == "1001")
                {
                    ProducerName = "RED Auto Protection INC";
                    ProducerAddress = "5350 College Blvd";
                    ProducerCity = "Overland Park";
                    ProducerState = "KS";
                    ProducerZipCode = "66211";
                }
                if (InsuranceType == "First Dollar")
                {
                    InsuranceType = "FD";
                }


                try
                {
                    if (Convert.ToDouble(CancellationPercentage) < 1)
                    {
                        ContractStatus_Transcode = "CP";
                    }
                    else
                    {
                        ContractStatus_Transcode = "CF";
                    }
                }
                catch
                {
                    ContractStatus_Transcode = "CF";
                }


                try
                {
                    if (ReInsuranceCompanyCode.Length < 0)
                    {
                        ReInsuranceCompanyCode = "N";
                    }
                }
                catch
                {
                    ReInsuranceCompanyCode = "N";
                }


                if (New_Used == "U")
                {
                    New_Used = "USED";
                }
                else if (New_Used == "N" || New_Used == "n")
                {
                    New_Used = "NEW";
                }
                else
                {
                    New_Used = "USED";
                }
                if (InsuranceType.ToUpper() == "XOL")
                {
                    insurancePremiumcontrol = Ceding_Insurance_Fee + PremiumTax;
                }


                InsurancePremiumChange = InsurancePremiumChange * -1;
                ReserveAmountChange = ReserveAmountChange * -1;
                PremiumTax = PremiumTax * -1;
                Ceding_Insurance_Fee = Ceding_Insurance_Fee * -1;
                ContingencyFee = ContingencyFee * -1;
                ObligorFee = ObligorFee * -1;
                CommercialSurcharge = CommercialSurcharge * -1;
                Diesel_Hybrid_Surcharge = Diesel_Hybrid_Surcharge * -1;
                FourWD_AWD_Surcharge = FourWD_AWD_Surcharge * -1;
                Turbo_SuperChargeSurcharge = Turbo_SuperChargeSurcharge * -1;
                OtherSurcharges = OtherSurcharges * -1;






                if (ContractStatus_Transcode == "CF" || ContractStatus_Transcode == "CP")
                {
                    salesControlMapping();
                    string ContractWriteOut = "";
                    ContractWriteOut = ContractWriteOut + TPA.Trim() + "|";
                    ContractWriteOut = ContractWriteOut + InsuranceCompanyName.Trim() + "|";
                    ContractWriteOut = ContractWriteOut + ProgramName.Trim() + "|";
                    ContractWriteOut = ContractWriteOut + InsuranceType.Trim() + "|";
                    ContractWriteOut = ContractWriteOut + Currency.Trim() + "|";
                    ContractWriteOut = ContractWriteOut + DealerPolicyNumber.Trim() + "|";
                    ContractWriteOut = ContractWriteOut + "|";
                    ContractWriteOut = ContractWriteOut + CustomerContractNumber.Trim() + "|";
                    ContractWriteOut = ContractWriteOut + WarrantySKU.Trim() + "|";
                    ContractWriteOut = ContractWriteOut + WarrantyDescription.Trim() + "|";
                    ContractWriteOut = ContractWriteOut + "|";
                    ContractWriteOut = ContractWriteOut + CoverageType.Trim() + "|";
                    ContractWriteOut = ContractWriteOut + coverageSubType.Trim() + "|";
                    ContractWriteOut = ContractWriteOut + ProductID.Trim() + "|";
                    ContractWriteOut = ContractWriteOut + ContractStatus_Transcode.Trim() + "|";
                    ContractWriteOut = ContractWriteOut + TermMileage.Trim() + "|";
                    ContractWriteOut = ContractWriteOut + TermMonths.Trim() + "|";
                    ContractWriteOut = ContractWriteOut + "|";
                    ContractWriteOut = ContractWriteOut + "|";
                    ContractWriteOut = ContractWriteOut + StartOfRiskType.Trim() + "|";
                    try
                    {
                        converter = Convert.ToDateTime(TransactionDate);
                        TransactionDate = converter.ToString("MM/dd/yyyy");
                    }
                    catch
                    {
                    }
                    ContractWriteOut = ContractWriteOut + TransactionDate + "|";
                    try
                    {
                        converter = Convert.ToDateTime(ContractPurchaseDate);
                        ContractPurchaseDate = converter.ToString("MM/dd/yyyy");
                    }
                    catch
                    {
                    }
                    ContractWriteOut = ContractWriteOut + ContractPurchaseDate + "|";
                    ProductPurchaseDate = ContractPurchaseDate;
                    try
                    {
                        converter = Convert.ToDateTime(ProductPurchaseDate);
                        ProductPurchaseDate = converter.ToString("MM/dd/yyyy");
                    }
                    catch
                    {
                    }
                    ContractWriteOut = ContractWriteOut + ProductPurchaseDate + "|";
                    ContractWriteOut = ContractWriteOut + "|";

                    ContractWriteOut = ContractWriteOut + "|";
                    ContractWriteOut = ContractWriteOut + "|";

                    try
                    {
                        converter = Convert.ToDateTime(LaborCoverageEffectiveDate);
                        LaborCoverageEffectiveDate = converter.ToString("MM/dd/yyyy");
                    }
                    catch
                    {
                    }
                    ContractWriteOut = ContractWriteOut + LaborCoverageEffectiveDate + "|";
                    try
                    {
                        converter = Convert.ToDateTime(LaborCoverageExpirationDate);
                        LaborCoverageExpirationDate = converter.ToString("MM/dd/yyyy");
                    }
                    catch
                    {
                    }
                    ContractWriteOut = ContractWriteOut + LaborCoverageExpirationDate + "|";
                    try
                    {
                        converter = Convert.ToDateTime(PartsCoverageEffectiveDate);
                        PartsCoverageEffectiveDate = converter.ToString("MM/dd/yyyy");
                    }
                    catch
                    {
                    }
                    ContractWriteOut = ContractWriteOut + PartsCoverageEffectiveDate + "|";
                    try
                    {
                        converter = Convert.ToDateTime(PartsCoverageExpirationDate);
                        PartsCoverageExpirationDate = converter.ToString("MM/dd/yyyy");
                    }
                    catch
                    {
                    }
                    ContractWriteOut = ContractWriteOut + PartsCoverageExpirationDate + "|";
                    ContractWriteOut = ContractWriteOut + ProductQuantity + "|";
                    ContractWriteOut = ContractWriteOut + ProductSKU.Trim() + "|";
                    ContractWriteOut = ContractWriteOut + ProductType.Trim() + "|";
                    ContractWriteOut = ContractWriteOut + "|";
                    ContractWriteOut = ContractWriteOut + "|";
                    ContractWriteOut = ContractWriteOut + "|";
                    ContractWriteOut = ContractWriteOut + "|";
                    ContractWriteOut = ContractWriteOut + Make.Trim() + "|";
                    ContractWriteOut = ContractWriteOut + Model.Trim() + "|";
                    ContractWriteOut = ContractWriteOut + ModelYear.Trim() + "|";
                    ContractWriteOut = ContractWriteOut + "|";
                    ContractWriteOut = ContractWriteOut + VIN.Trim() + "|";
                    ContractWriteOut = ContractWriteOut + CC.Trim() + "|";
                    ContractWriteOut = ContractWriteOut + RateClass.Trim() + "|";
                    ContractWriteOut = ContractWriteOut + New_Used.Trim() + "|";
                    ContractWriteOut = ContractWriteOut + "|";
                    ContractWriteOut = ContractWriteOut + ProductPurchasePrice.Trim() + "|";
                    ContractWriteOut = ContractWriteOut + "|";
                    ContractWriteOut = ContractWriteOut + MileageAtPurchase.Trim() + "|";
                    ContractWriteOut = ContractWriteOut + MileageAtCoverageStart.Trim() + "|";
                    ContractWriteOut = ContractWriteOut + "|";
                    ContractWriteOut = ContractWriteOut + "|";
                    ContractWriteOut = ContractWriteOut + "|";
                    ContractWriteOut = ContractWriteOut + "|";
                    ContractWriteOut = ContractWriteOut + "|";
                    ContractWriteOut = ContractWriteOut + ContractExpireMileage.Trim() + "|";
                    ContractWriteOut = ContractWriteOut + "|";
                    ContractWriteOut = ContractWriteOut + UseType.Trim() + "|";
                    ContractWriteOut = ContractWriteOut + "|";
                    ContractWriteOut = ContractWriteOut + CustomerFirstName.Trim() + "|";
                    ContractWriteOut = ContractWriteOut + CustomerLastName.Trim() + "|";
                    ContractWriteOut = ContractWriteOut + CustomerAddress.Trim() + "|";
                    ContractWriteOut = ContractWriteOut + CustomerAddress2.Trim() + "|";
                    ContractWriteOut = ContractWriteOut + CustomerCity.Trim() + "|";
                    ContractWriteOut = ContractWriteOut + CustomerState.Trim() + "|";
                    ContractWriteOut = ContractWriteOut + CustomerZip.Trim() + "|";
                    ContractWriteOut = ContractWriteOut + CustomerCountry.Trim() + "|";
                    ContractWriteOut = ContractWriteOut + "|";
                    ContractWriteOut = ContractWriteOut + "|";
                    ContractWriteOut = ContractWriteOut + "|";
                    ContractWriteOut = ContractWriteOut + ProducerCode.Trim() + "|";
                    ContractWriteOut = ContractWriteOut + ProducerName.Trim() + "|";
                    ContractWriteOut = ContractWriteOut + ProducerAddress + "|";
                    ContractWriteOut = ContractWriteOut + ProducerAddress2 + "|";
                    ContractWriteOut = ContractWriteOut + ProducerCity.Trim() + "|";
                    ContractWriteOut = ContractWriteOut + ProducerState.Trim() + "|";
                    ContractWriteOut = ContractWriteOut + ProducerZipCode + "|";
                    ContractWriteOut = ContractWriteOut + ProducerCountry.Trim() + "|";
                    ContractWriteOut = ContractWriteOut + SalesChannel.Trim() + "|";
                    ContractWriteOut = ContractWriteOut + "|";
                    ContractWriteOut = ContractWriteOut + ClaimLoL.Trim() + "|";
                    ContractWriteOut = ContractWriteOut + ContractLoL.Trim() + "|";
                    ContractWriteOut = ContractWriteOut + "|";
                    ContractWriteOut = ContractWriteOut + "|";
                    ContractWriteOut = ContractWriteOut + "|";
                    ContractWriteOut = ContractWriteOut + "|";
                    ContractWriteOut = ContractWriteOut + DeductibleType.Trim() + "|";
                    ContractWriteOut = ContractWriteOut + Deductible.Trim() + "|";
                    ContractWriteOut = ContractWriteOut + "|";
                    ContractWriteOut = ContractWriteOut + ReInsuranceCompanyCode + "|";
                    ContractWriteOut = ContractWriteOut + ReInsuranceCompanyName + "|";
                    ContractWriteOut = ContractWriteOut + ReinsuranceType + "|";
                    ContractWriteOut = ContractWriteOut + "|";
                    ContractWriteOut = ContractWriteOut + "|";
                    ContractWriteOut = ContractWriteOut + "|";
                    ContractWriteOut = ContractWriteOut + "|";
                    ContractWriteOut = ContractWriteOut + RetailAmount.Trim() + "|";
                    ContractWriteOut = ContractWriteOut + dealerCost + "|";
                    ContractWriteOut = ContractWriteOut + NetInsurancePremiumControl + "|";
                    ContractWriteOut = ContractWriteOut + InsurancePremiumChange + "|";
                    ContractWriteOut = ContractWriteOut + NetReserveAmountControl + "|";
                    ContractWriteOut = ContractWriteOut + ReserveAmountChange + "|";
                    ContractWriteOut = ContractWriteOut + PremiumTax + "|";
                    ContractWriteOut = ContractWriteOut + "|";
                    ContractWriteOut = ContractWriteOut + "|";
                    ContractWriteOut = ContractWriteOut + "|";
                    ContractWriteOut = ContractWriteOut + "|";
                    ContractWriteOut = ContractWriteOut + "|";
                    ContractWriteOut = ContractWriteOut + "|";
                    ContractWriteOut = ContractWriteOut + "|";
                    ContractWriteOut = ContractWriteOut + Ceding_Insurance_Fee + "|";
                    ContractWriteOut = ContractWriteOut + "|";
                    ContractWriteOut = ContractWriteOut + ContingencyFee + "|";
                    ContractWriteOut = ContractWriteOut + ObligorFee + "|";
                    ContractWriteOut = ContractWriteOut + "|";
                    ContractWriteOut = ContractWriteOut + CommercialSurcharge + "|";
                    ContractWriteOut = ContractWriteOut + Diesel_Hybrid_Surcharge + "|";
                    ContractWriteOut = ContractWriteOut + FourWD_AWD_Surcharge + "|";
                    ContractWriteOut = ContractWriteOut + Turbo_SuperChargeSurcharge + "|";
                    ContractWriteOut = ContractWriteOut + OtherSurcharges + "|";
                    ContractWriteOut = ContractWriteOut + "|";
                    ContractWriteOut = ContractWriteOut + CancellationDate.Trim() + "|";
                    ContractWriteOut = ContractWriteOut + CancellationFormula.Trim() + "|";
                    ContractWriteOut = ContractWriteOut + "|";
                    ContractWriteOut = ContractWriteOut + CancellationMileage + "|";
                    ContractWriteOut = ContractWriteOut + CancellationPercentage.Trim() + "|";
                    ContractWriteOut = ContractWriteOut + "|";
                    ContractWriteOut = ContractWriteOut + ObligorID.Trim() + "|";
                    ContractWriteOut = ContractWriteOut + ObligorName.Trim() + "|";
                    ContractWriteOut = ContractWriteOut + ObligorState.Trim() + "|";
                    ContractWriteOut = ContractWriteOut + ObligorAddress1.Trim() + "|";
                    ContractWriteOut = ContractWriteOut + ObligorAddress2.Trim() + "|";
                    ContractWriteOut = ContractWriteOut + ObligorCountry.Trim() + "|";
                    ContractWriteOut = ContractWriteOut + formNumber + "|";
                    ContractWriteOut = ContractWriteOut + "|";
                    ContractWriteOut = ContractWriteOut + "|";
                    ContractWriteOut = ContractWriteOut.Replace(",", "");
                    streamWriterContract.WriteLine(ContractWriteOut);
                }



            }
            streamWriterContract.Close();
            StreamWriter streamWriterSalesControl = new StreamWriter(setPath() + "/MonthEnd/" + setMonth() + "/AmTrust/ " + setYear() + "-" + setMonthNo() + "_Veritas_SalesControl.txt");
            //streamWriterSalesControl.WriteLine("Total Record Count|Sales Count|Total Cancels Count|Retail Amount|Insurance Premium|Reserve Amount|Commission|Premium Tax|Contingency Fees|Service Fees|Admin Amount|");
            //streamWriterSalesControl.WriteLine(totalRecordCountcontrol + "|" + saleCountcontrol + "|" + totalCancelsCountcontrol + "|" + retailAmountcontrol + "|" + insurancePremiumcontrol + "|" + totalReserveAmount + "|" + commissionscontrol + "|" + premiumTaxcontrol + "|" + contigencyFeescontrol + "|" + serviceFeescontrol + "|" + adminAmountcontrol + "|");
            streamWriterSalesControl.WriteLine("Description|Totals|Control Total Mapping");
            streamWriterSalesControl.WriteLine("Total Record Count|");
            streamWriterSalesControl.Write(totalRecordCountcontrol + "|Count of all records|");
            streamWriterSalesControl.Write("Sales Count|");
            streamWriterSalesControl.WriteLine(saleCountcontrol + "|Count of all 'Sale' transcode records|");
            streamWriterSalesControl.Write("Total Cancels Count|");
            streamWriterSalesControl.WriteLine(totalCancelsCountcontrol + "|count of all 'Cancel' transcode records|");
            streamWriterSalesControl.Write("Retail Amount|");
            streamWriterSalesControl.WriteLine(retailAmountcontrol + "|SUM of retail amount from the sales file|");
            streamWriterSalesControl.Write("Insurance Premium|");
            streamWriterSalesControl.WriteLine(insurancePremiumcontrol + "|SUM of insurance premium from the sales file|");
            streamWriterSalesControl.Write("Reserve Amount|");
            streamWriterSalesControl.WriteLine(totalReserveAmount + "|SUM of reserve amount from the sales file|");
            streamWriterSalesControl.Write("Commission|");
            streamWriterSalesControl.WriteLine(commissionscontrol + "|SUM of comission from the sales file|");
            streamWriterSalesControl.Write("Premium Tax|");
            streamWriterSalesControl.WriteLine(premiumTaxcontrol + "|SUM of premium tax amount from the sales file|");
            streamWriterSalesControl.Write("Contingency Fees|");
            streamWriterSalesControl.WriteLine(contigencyFeescontrol + "|SUM of contingency fees amount from the sales file|");
            streamWriterSalesControl.Write("Service Fees|");
            streamWriterSalesControl.WriteLine(serviceFeescontrol + "|SUM of service fees from the sales file|");
            streamWriterSalesControl.Write("Admin Amount|");
            streamWriterSalesControl.WriteLine(adminAmountcontrol + "|SUM of admin amount from the sales file|");
            streamWriterSalesControl.Close();
            void salesControlMapping()
            {
                totalRecordCountcontrol = totalRecordCountcontrol + 1;
                if (ContractStatus_Transcode.ToLower() == "cf")
                {
                    totalCancelsCountcontrol = totalCancelsCountcontrol + 1;
                }
                else
                {
                    saleCountcontrol = saleCountcontrol + 1;
                }
                try
                {
                    retailAmountcontrol = Convert.ToDouble(RetailAmount) + retailAmountcontrol;
                }
                catch
                {
                }
                try
                {
                    insurancePremiumcontrol = insurancePremiumcontrol + Convert.ToDouble(NetInsurancePremiumControl);
                }
                catch
                {
                }
                try
                {
                    totalReserveAmount = totalReserveAmount + ReserveAmountChange;
                }
                catch
                {
                }
                try
                {
                    commissionscontrol = commissionscontrol + commissions;
                }
                catch
                {

                }
                try
                {
                    premiumTaxcontrol = premiumTaxcontrol + PremiumTax;
                }
                catch
                {
                }
                try
                {
                    contigencyFeescontrol = contigencyFeescontrol + contigency;

                }
                catch
                {

                }
                try
                {
                    serviceFeescontrol = serviceFeescontrol + serviceFee;

                }
                catch
                {

                }
                try
                {
                    adminAmountcontrol = adminAmountcontrol + admin;
                }
                catch
                {
                }
            }
        }
        public void AmTrustDealerDump()
        {
            try
            {
                File.Delete(setPath() + "/MonthEnd/" + setMonth() + "/AmTrust/ " + setYear() + "-" + setMonthNo() + "_Veritas_dealer.txt");
            }
            catch
            {

            }
            CSharpDBO.CSharpDBO dealerDBO = new CSharpDBO.CSharpDBO();
            StreamWriter streamWriterDealer = new StreamWriter(setPath() + "/MonthEnd/" + setMonth() + "/AmTrust/ " + setYear() + "-" + setMonthNo() + "_Veritas_dealer.txt");



            string accountName = "";
            string producerCode = "";
            string producerName = "";
            string producerCity = "";
            string producerState = "";
            string producerZipCode = "";
            string producerCountry = "";
            string producerStatus = "";
            string signUpDate = "";
            string cancelDate = "";

            string writer = "";

            writer = "";
            writer = "accountName | producerCode | producerName | producerCity | producerState | producerZipCode | producerCountry | producerStatus | signUpDate | cancelDate";
            streamWriterDealer.WriteLine(writer);

            string sql = "select * from dealer where dealerID in (select distinct dealerID from contract where status = 'Paid' or Status = 'Expired' or status = 'Cancelled')";

            dealerDBO.dboOpen(path);

            System.Data.DataTable dt = dealerDBO.dboInportInformation(sql);
            int tracker = dt.Rows.Count;
            lblProgressBar.Text = "Processing AmTrust Dealer";
            lblProgressBar.Refresh();
            lblDealer.Text = "Dealer:" + tracker;
            lblContract.Refresh();
            progressBar1.Value = 0;
            progressBar1.Maximum = tracker;
            progressBar1.Minimum = 0;
            progressBar1.Visible = true;
            progressBar1.Step = 1;

            foreach (DataRow dr in dt.Rows)
            {
                progressBar1.PerformStep();
                accountName = "";
                producerCode = "";
                producerName = "";
                producerCity = "";
                producerState = "";
                producerZipCode = "";
                producerCountry = "";
                producerStatus = "";
                signUpDate = "";
                cancelDate = "";

                try
                {
                    accountName = "Veritas";
                }
                catch
                {
                    accountName = "";
                }//accountName
                try
                {
                    producerCode = Convert.ToString(dr["DealerNo"]);
                }
                catch
                {
                    producerCode = "";
                }//producerCode
                try
                {
                    producerName = Convert.ToString(dr["DealerName"]) + " - " + Convert.ToString(dr["dealerNo"]); ;
                }
                catch
                {
                    producerName = "";
                }//producerName
                try
                {
                    producerCity = Convert.ToString(dr["city"]);
                }
                catch
                {
                    producerCity = "";
                }//producerCity
                try
                {
                    producerState = Convert.ToString(dr["state"]);
                }
                catch
                {
                    producerState = "";
                }//producerState
                try
                {
                    producerZipCode = Convert.ToString(dr["zip"]);
                }
                catch
                {
                    producerZipCode = "";
                }//producerZipCode
                producerCountry = "USA";
                try
                {
                    producerStatus = Convert.ToString(dr["DealerStatusID"]);
                }
                catch
                {
                    producerStatus = "";
                }//producerStatus
                try
                {
                    signUpDate = Convert.ToString(dr["DateSigned"]);
                }
                catch
                {
                    signUpDate = "";
                }//signUpDate
                if (signUpDate.Length == 0)
                {
                    try
                    {
                        signUpDate = Convert.ToString(dr["Moddate"]);
                    }
                    catch
                    {
                        signUpDate = "";
                    }//signUpDate
                    if (signUpDate.Length == 0)
                    {
                        signUpDate = "1/19/2015";
                    }
                }

                if (producerStatus.Trim() != "2")
                {
                    try
                    {
                        cancelDate = Convert.ToString(Convert.ToDateTime(dr["Moddate"]).AddDays(1));
                    }
                    catch
                    {
                        cancelDate = "";
                    }//cancelDate
                    producerStatus = "C";

                }
                else
                {
                    producerStatus = "A";
                }

                writer = "";
                writer = accountName + "|" + producerCode + "|" + producerName + "|" + producerCity + "|" + producerState + "|" + producerZipCode + "|" + producerCountry + "|" + producerStatus + "|" + signUpDate + "|" + cancelDate;
                streamWriterDealer.WriteLine(writer);
            }
            streamWriterDealer.Close();
        }

            //Arch - Arch utilizes ARCHEXPORT1 to generate file. 
        public void ArchContract()
        {

            string filePathContract = setPath() + @"\MonthEnd\" + setMonth() + @"\Arch\Contract_" + setMonth() + ".txt";
            string filePathClaimsPaid = setPath() + @"\MonthEnd\" + setMonth() + @"\Arch\Claim_Payment_" + setMonth() + ".txt";
            string filePathAllClaims = setPath() + @"\MonthEnd\" + setMonth() + @"\Arch\Claims_" + setMonth() + ".txt";
            DateTime end = dtpEndDate.Value;

            string sStartDate = Convert.ToString(dtpStartDate.Value);
            string sEndDate = Convert.ToString(end.AddDays(1));
            ARCHExport.Module1 module = new Module1();
            module.Main(setPath() + "/MonthEnd/" + setMonth() + @"\Arch\", sStartDate, sEndDate);



        }
        public void ArchClaimsPaid()
        {
            int coutnerIf = 0;
            int arrayCounter = 0;
            string writeOut = "";
            string sql = "";
            string MasterPolicy = "";
            string ContractNumber = "";
            string claimNumberCompare = "";
            string ClaimNumber = "";
            string claimSequence = "";
            int[] claimSequenceArray = { 1, 2, 3 };
            string paymentType = "";
            double paymentAmt = 0;
            double[] paymentAmtArray = new double[3];
            string payeeName = "";
            string payeeAddressLine1 = "";
            string payeeAddressLine2 = "";
            string payeeCity = "";
            string payeeState = "";
            string reference = "";
            string checkNumber = "";
            int creditCardNumber = 0;
            string paymentDate = "";
            DateTime today2 = DateTime.Today;
            CSharpDBO.CSharpDBO CLaimPaymentDBO = new CSharpDBO.CSharpDBO();
            string excludeArch = "";
            string includeArch = "";


            string excludeArchClaims = "";
            string includeArchClaims = "";

            if (rtbRemoveClaimsArch.Text.Length > 0)
            {
                excludeArchClaims = " and not c.claimNo in (" + setExcludeClaimsArch() + ")";
            }
            if (rtbAddClaimsArch.Text.Length > 0)
            {
                includeArchClaims = " or c.claimNo in ( " + setAddClaimsArch() + ")";
            }

            try
            {
                File.Delete(setPath() + "/MonthEnd/" + setMonth() + "/Arch/Claim_Payment_" + setMonth() + "2.txt");
            }
            catch
            {

            }

            string header = "MasterPolicy\tcontractNumber\tClaimNumber\tclaimSequence\tpaymentType\tpaymentAmt\tpayeeName\tpayeeAddressLine1\tpayeeAddressLine2\tpayeeCity\tpayeeState\treference\tcheckNumber\tcreditCardNumber\tpaymentDate\n";
            File.AppendAllText(setPath() + "/MonthEnd/" + setMonth() + "/Arch/Claim_Payment_" + setMonth() + ".txt", header);

            sql = "select distinct cd.ClaimDetailID as ClaimDetailID, " +
                "ccp.ClipNo as MasterPolicy, " +
                "c.ContractNo as contractNumber, " +
                "cl.ClaimNo as ClaimNumber," +
                "cd.JobNo as claimSequence, " +
                "'LOSS' as paymentType, " +
                "cd.PaidAmt as paymentAmt, " +
                "cp.PayeeName as payeeName, " +
                "cp.Addr1 as payeeAddressLine1, " +
                "cp.Addr2 as payeeAddressLine2, " +
                "cp.City as payeeCity, " +
                "cp.State as payeeState, " +
                "'' as reference, " +
                "'Calculate in code' as checkNumber,  " +
                "scsp.scredit_card_last_digits as creditCardNumber, " +
                "clp.DatePaid as paymentDate " +
                "from  Claim cl  " +
                "left join ClaimDetail cd on cd.ClaimID = cl.ClaimID  " +
                "left join  ClaimPayee cp on cp.ClaimPayeeID = cd.ClaimPayeeID  " +
                "left join  Contract c on c.ContractID = cl.ContractID  " +
                "left join  ClaimPayment clp on clp.ClaimDetailID = cd.ClaimDetailID  " +
                "left join scs_auto_raa.dbo.scs_claim_details scsC on scsC.idetail_id = cd.ClaimDetailID  " +
                "left join scs_auto_raa.dbo.scs_claim_credit_payments scsP on scsC.ipayment_id = scsP.ipayment_id  " +
                "left join contractClip ccp on ccp.clipID = c.clipID " +
                "where cl.Status = 'Paid'  " +
                "and (c.status = 'Paid' or c.status = 'Cancelled' or c.status = 'EXPIRED') " +
                "and cl.DatePaid >= '" + dtpStartDate.Value + "' " +
                "and cl.DatePaid <= '" + dtpEndDate.Value + "' " +
                "and c.InsCarrierID = 2 " +
                "and cd.RateTypeID = 1 " +
                "" + excludeArch + " " +
                "" + includeArch + " " +
                "and not cl.claimno = 'C000030105' " +
                "order by ClaimNumber desc";
            lblProgressBar.Text = "Executing Querry";
            lblProgressBar.Refresh();
            CLaimPaymentDBO.dboOpen(path);
            System.Data.DataTable ct = CLaimPaymentDBO.dboInportInformation(sql);
            int counter = ct.Rows.Count;
            lblProgressBar.Text = "Processing Arch Claims Paid";
            lblProgressBar.Refresh();

            //lblArchClaim.Text = "Arch Claims Paid:" + counter;
            //lblArchClaim.Refresh();
            progressBar1.Value = 0;
            progressBar1.Maximum = counter;
            progressBar1.Minimum = 0;
            progressBar1.Visible = true;
            progressBar1.Step = 1;


            foreach (DataRow dr in ct.Rows)
            {
                progressBar1.PerformStep();
                MasterPolicy = "";
                ContractNumber = "";
                ClaimNumber = "";
                claimSequence = "";
                paymentType = "";
                paymentAmt = 0;
                payeeName = "";
                payeeAddressLine1 = "";
                payeeAddressLine2 = "";
                payeeCity = "";
                payeeState = "";
                reference = "";
                checkNumber = "";
                creditCardNumber = 0;
                paymentDate = "";
                try
                {
                    MasterPolicy = Convert.ToString(dr["MasterPolicy"]);
                }
                catch
                {
                }
                try
                {
                    ContractNumber = dr["contractNumber"].ToString();
                }
                catch
                {
                }
                try
                {
                    ClaimNumber = dr["ClaimNumber"].ToString();
                }
                catch
                {
                }
                try
                {
                    claimSequence = dr["claimSequence"].ToString();
                }
                catch
                {
                }
                try
                {
                    paymentType = dr["paymentType"].ToString();
                }
                catch
                {
                }
                try
                {
                    paymentAmt = Convert.ToDouble(dr["paymentAmt"]);
                }
                catch
                {
                }
                try
                {
                    payeeName = dr["payeeName"].ToString();
                }
                catch
                {
                }
                try
                {
                    payeeAddressLine1 = dr["payeeAddressLine1"].ToString();
                }
                catch
                {
                }
                try
                {
                    payeeAddressLine2 = dr["payeeAddressLine2"].ToString();
                }
                catch
                {
                }
                try
                {
                    payeeCity = dr["payeeCity"].ToString();
                }
                catch
                {
                }
                try
                {
                    payeeState = dr["payeeCity"].ToString();
                }
                catch
                {
                }
                try
                {
                    payeeState = dr["payeeState"].ToString();
                }
                catch
                {
                }
                try
                {
                    reference = dr["reference"].ToString();
                }
                catch
                {
                }
                try
                {
                    checkNumber = dr["checkNumber"].ToString();
                }
                catch
                {
                }
                try
                {
                    creditCardNumber = Convert.ToInt32(dr["creditCardNumber"]);
                }
                catch
                {
                }
                try
                {
                    paymentDate = dr["paymentDate"].ToString();
                }
                catch
                {
                }

                if (creditCardNumber <= 0)
                {
                    Random rando = new Random();
                    int randomNumber = 0;
                    sql = "select * from claimachrandomnumber where claimno = '" + ClaimNumber + "'";
                    System.Data.DataTable random = CLaimPaymentDBO.dboInportInformation(sql);
                    foreach (DataRow dataRow in random.Rows)
                    {
                        try
                        {
                            randomNumber = Convert.ToInt32(dataRow["randomnumber"]);
                        }
                        catch
                        {
                            randomNumber = -1;
                        }
                    }

                    if (randomNumber <= 0)
                    {
                        bool exists = false;
                        randomNumber = rando.Next(100000000, 999999999);
                        while (!exists)
                        {
                            string check = "select * from claimachrandomnumber where RandomNumber = '" + randomNumber + "'";
                            CLaimPaymentDBO.dboInportInformation(check);
                            exists = CLaimPaymentDBO.hasRows();
                            if (exists)
                            {
                                randomNumber = rando.Next(100000000, 999999999);
                            }
                            else
                            {
                                exists = true;
                            }

                        }
                        checkNumber = randomNumber.ToString();
                        string insertRando = "insert into claimachrandomnumber(ClaimNo,RandomNumber) values('" + ClaimNumber + "'," + checkNumber + ")";
                        CLaimPaymentDBO.dboAlterDBOUnsafe(insertRando);
                    }
                    else
                    {
                        checkNumber = "ACH";
                    }

                }
                if (checkNumber == "Calculate in code")
                {
                    checkNumber = "ACH";
                }

                if (claimSequence.ToUpper() == "A01")
                {
                    paymentAmtArray[0] = paymentAmtArray[0] + paymentAmt;
                }
                else if (claimSequence.ToUpper().Contains("J"))
                {
                    paymentAmtArray[1] = paymentAmtArray[1] + paymentAmt;
                }
                else if (claimSequence.ToUpper().Contains("A") && claimSequence.ToUpper() != "A01")
                {
                    paymentAmtArray[2] = paymentAmtArray[2] + paymentAmt;
                }
                if (coutnerIf == 0)
                {
                    claimNumberCompare = ClaimNumber;
                    coutnerIf = 1;
                }


                if (claimNumberCompare != ClaimNumber)
                {
                    arrayCounter = 0;

                    while (arrayCounter < 3)
                    {
                        if (paymentAmtArray[arrayCounter] != 0)
                        {
                            writeOut = "";
                            writeOut = writeOut + MasterPolicy + "\t";
                            writeOut = writeOut + ContractNumber + "\t";
                            writeOut = writeOut + ClaimNumber + "\t";
                            writeOut = writeOut + claimSequenceArray[arrayCounter] + "\t";
                            writeOut = writeOut + paymentType + "\t";
                            writeOut = writeOut + paymentAmtArray[arrayCounter] + "\t";
                            writeOut = writeOut + payeeName + "\t";
                            writeOut = writeOut + payeeAddressLine1 + "\t";
                            writeOut = writeOut + payeeAddressLine2 + "\t";
                            writeOut = writeOut + payeeCity + "\t";
                            writeOut = writeOut + payeeState + "\t";
                            writeOut = writeOut + reference + "\t";
                            writeOut = writeOut + checkNumber + "\t";
                            writeOut = writeOut + creditCardNumber + "\t";
                            writeOut = writeOut + paymentDate + "\t";
                            writeOut = writeOut + "\n";
                            File.AppendAllText(setPath() + "/MonthEnd/" + setMonth() + "/Arch/Claim_Payment_" + setMonth() + "2.txt", writeOut);
                        }

                        arrayCounter++;
                    }
                    claimNumberCompare = ClaimNumber;
                    paymentAmtArray[0] = 0;
                    paymentAmtArray[1] = 0;
                    paymentAmtArray[2] = 0;
                }




            }
            progressBar1.Value = 0;
            lblProgressBar.Text = "";
            CLaimPaymentDBO.dboClose();
        }
        public void ArchClaimsAll()
        {
            CSharpDBO.CSharpDBO ClaimsDBO = new CSharpDBO.CSharpDBO();
            string writeOut = "";
            string sql = "";
            string MasterPolicy = "";
            string ContractNumber = "";
            int vehicleYear = 0;
            string vehicleMake = "";
            string vehicleModel = "";
            string VIN = "";

            string ClaimNumber = "";
            string checkNumber = "";
            int creditCardNumber = 0;

            string claimSequenceNumber = "";
            string dateOfLoss = "";
            string claimDate = "";
            string ContractName = "";
            string effectiveDate = "";
            string contractExperationDate = "";
            string vehicleType = "";
            string closeWithoutPay = "";
            string sellingDealerName = "";
            string sellingDealerCode = "";
            string dateClaimSentToInsuranceCarrier = "";
            string dateClaimMailed = "";
            string insurerApprovalNumber = "";
            string claimAdjuster = "";
            string serviceAdvisor = "";
            string techninian = "";
            string complaint = "";
            string failCode = "";
            string failComponent = "";
            string dateDenied = "";
            string denialReason = "";
            double lossMileage = 0;
            string claimStatus = "";
            double saleMilage = 0;
            double expirationMilage = 0;
            string contractFormNumber = "";
            string coverageCode = "";
            string componmentCoverageGroup = "";
            string componentRepairCorrection = "";
            string dianosticTime = "";
            double laborTime = 0;
            double laborTotal = 0;
            double PartCost = 0;
            double rental = 0;
            double towing = 0;
            double tax = 0;
            double Deductible = 0;
            double goodWill = 0;
            double claimTotal = 0;
            string checkDate = "";
            double pendingTotalAmount = 0;
            string repairFacilityName = "";
            string repairFacilityCode = "";
            string transientClaim = "";
            string repairFacilityAddress = "";
            string repairFacilityCity = "";
            string repairFacilityState = "";
            string repairFacilityZip = "";
            string repairFacilityPhoneNumber = "";
            string repairFacilityEmailAddress = "";
            string repairFacilityFaxNumber = "";
            DateTime today2 = DateTime.Today;

            string excludeArchClaims = "";
            string includeArchClaims = "";

            if (rtbRemoveClaimsArch.Text.Length > 0)
            {
                excludeArchClaims = " and not c.claimNo in (" + setExcludeClaimsArch() + ")";
            }
            if (rtbAddClaimsArch.Text.Length > 0)
            {
                includeArchClaims = " or c.claimNo in ( " + setAddClaimsArch() + ")";
            }


            try
            {
                File.Delete(setPath() + "/MonthEnd/" + setMonth() + "/Arch/Claims_" + setMonth() + ".txt");
            }
            catch
            {

            }

            string header = "MasterPolicy\tcontractNumber\tclaimNumber\tclaimSequenceNumber\tdateOfLoss\tclaimDate\tContractName\teffectiveDate\tcontractExperationDate\tvehicleType\tvehicleYear\tvehicleMake\tvehicleModel\tvin\tcloseWithoutPay\tsellingDealerName\tsellingDealerCode\tdateClaimSentToInsuranceCarrier\tdateClaimMailed\tinsurerApprovalNumber\tclaimAdjuster\tserviceAdvisor\ttechninian\tcomplaint\tfailCode\tfailComponent\tdateDenied\tdenialReason\tlossMileage\tclaimStatus\tsaleMilage\texpirationMilage\tcontractFormNumber\tcoverageCode\tcomponmentCoverageGroup\tcomponentRepairCorrection\tdianosticTime\tlaborTime\tlaborTotal\tPartCost\trental\ttowing\ttax\tDeductible\tgoodWill\tclaimTotal\tcheckNumber\tcheckDate\tcreditCardNumber\tpendingTotalAmount\trepairFacilityName\trepairFacilityCode\ttransientClaim\trepairFacilityAddress\trepairFacilityCity\trepairFacilityState\trepairFacilityZip\trepairFacilityPhoneNumber\trepairFacilityEmailAddress\trepairFacilityFaxNumber\n";
            File.AppendAllText(setPath() + "/MonthEnd/" + setMonth() + "/Arch/Claims_" + setMonth() + "2.txt", header);


            sql = "select distinct cd.ClaimDetailID as ClaimDetailID, " +
                "ccp.ClipNo as MasterPolicy," +
                "ct.ContractNo as contractNumber, " +
                "c.ClaimNo as claimNumber, " +
                "cd.JobNo as claimSequenceNumber, " +
                "c.LossDate as dateOfLoss," +
                "c.LossDate as claimDate, " +
                "ct.FName + ' ' + ct.LName as ContractName, " +
                "ct.EffDate as effectiveDate, " +
                "ct.ExpDate as contractExperationDate, " +
                "ct.ProgramID as vehicleType," +
                "ct.Year as vehicleYear, " +
                "ct.Make as vehicleMake, " +
                "ct.Model as vehicleModel," +
                "ct.VIN as vin,  " +
                "(case when cd.ClaimDetailStatus = 'Denied' then 'Y' else 'N' end) as closeWithoutPay,  " +
                "d.DealerName as sellingDealerName, " +
                "d.DealerNo as sellingDealerCode, " +
                "getdate() as dateClaimSentToInsuranceCarrier," +
                "'' as dateClaimMailed," +
                "'' as insurerApprovalNumber," +
                "(case when ui.FName = ui.LName then ui.FName else ui.FName+' '+ui.LName end) as claimAdjuster,'' as serviceAdvisor, " +
                "'' as techninian, sCD.sdetail_desc as complaint, " +
                "cd.LossCode as failCode, " +
                "sCD.sdetail_desc as failComponent,  " +
                "(case when cd.ClaimDetailStatus = 'Denied' then sCD.dtupdate_last end) as dateDenied, " +
                "(case when cd.ClaimDetailStatus = 'Denied' then scsCRS.sreason_code + ' ' + scsCRS.sreason_desc end) as denialReason,  " +
                "c.LossMile as lossMileage, " +
                "c.Status as claimStatus, " +
                "ct.SaleMile as saleMilage, " +
                "ct.ExpMile as expirationMilage, " +
                "ct.FormNo as contractFormNumber, " +
                "cc.ContractCode as coverageCode, " +
                "'Calculate in code' as componmentCoverageGroup, " +
                "cast(cn.Note as nvarchar(max)) as componentRepairCorrection, " +
                "'' as dianosticTime, sCD.clabor_rate as laborTime, " +
                "(case when cd.ClaimDetailType like '%Labor%' then cd.AuthAmt end ) as laborTotal, " +
                "(case when cd.ClaimDetailType like '%Part%' then cd.AuthAmt end ) as PartCost,  " +
                "(select PaidAmt as amt from ClaimDetail cdt where cdt.LossCode ='Zrental' and cdt.ClaimDetailID = cd.ClaimDetailID) as rental, " +
                "(select PaidAmt as amt from ClaimDetail cdt where (cdt.LossCode ='zbtw100c' or cdt.LossCode =  'zbtw100n') and cdt.ClaimDetailID = cd.ClaimDetailID) as towing, " +
                "cd.TaxAmt as tax, " +
                "(case when cd.ClaimDetailType like '%Deductible%' then cd.AuthAmt end ) as Deductible,  " +
                "(select PaidAmt as amt from ClaimDetail cdt where (cdt.LossCode ='ZGOODWLL' or cdt.LossCode =  'ZANGDWLL') and cdt.ClaimDetailID = cd.ClaimDetailID) as goodWill,  " +
                "cd.PaidAmt as claimTotal, " +
                "'Calculate in code' as checkNumber, " +
                "c.DatePaid as checkDate, " +
                "scP.scredit_card_last_digits as creditCardNumber, " +
                "'0' as pendingTotalAmount, " +
                "sc.ServiceCenterName as repairFacilityName, " +
                "sc.ServiceCenterNo as repairFacilityCode, " +
                "'TransientClaim' as transientClaim, " +
                "sc.Addr1 + ' ' + sc.Addr2 as repairFacilityAddress, " +
                "sc.City as repairFacilityCity, " +
                "sc.State as repairFacilityState, " +
                "sc.Zip as repairFacilityZip, " +
                "sc.Phone as repairFacilityPhoneNumber, " +
                "sc.EMail as repairFacilityEmailAddress,  " +
                "'' as repairFacilityFaxNumber " +
                "from ClaimDetail cd " +
                "left join Claim c on c.ClaimID = cd.ClaimID " +
                "left join ClaimPayment cp on cd.ClaimDetailID = cp.ClaimDetailID " +
                "left join ServiceCenter sc on sc.ServiceCenterID = c.ServiceCenterID " +
                "left join contract ct on ct.ContractID = c.ContractID " +
                "left join Dealer d on d.DealerID = ct.DealerID " +
                "left join UserInfo ui on ui.UserID = c.AuthBy " +
                "left join scs_auto_raa.dbo.scs_Claim_details sCD on cd.SCSClaimDetailID = sCD.idetail_id " +
                "left join ContractCode cc on cc.PlanTypeID = ct.PlanTypeID and cc.ProgramID = ct.ProgramID " +
                "left join ClaimNote cn on cn.ClaimID = cd.ClaimID and ClaimNoteID = 2 " +
                "left join scs_auto_raa.dbo.scs_claim_credit_payments scP on sCD.ipayment_id = scP.ipayment_id " +
                "left join scs_auto_raa.dbo.scs_claim_reason_status scsCRS on scsCRS.ireason_id = sCD.ireason_id " +
                "left join contractClip ccp on ccp.clipID = ct.ClipID " +
                "where c.DatePaid >= '" + dtpStartDate.Value + "' " +
                "and c.DatePaid <= '" + dtpEndDate.Value + "'" +
                "and (ct.status = 'Paid' or ct.status = 'Cancelled' or ct.status = 'EXPIRED') " +
                "and ct.InsCarrierID = 2 " +
                "and cd.RateTypeID = 1 " +
                "and not c.claimno = 'C000030105' " +
                "order by c.ClaimNo , claimSequenceNumber";


            lblProgressBar.Text = "Executing Querry";
            lblProgressBar.Refresh();
            ClaimsDBO.dboOpen(path);
            System.Data.DataTable dt = ClaimsDBO.dboInportInformation(sql);
            lblProgressBar.Text = "Processing all Arch Claims";
            lblProgressBar.Refresh();
            int counter = dt.Rows.Count;
            //lblArchAllClaims.Text = "Arch all Claims: " + counter;
            //lblArchAllClaims.Refresh();
            progressBar1.Value = 0;
            progressBar1.Maximum = counter;
            progressBar1.Minimum = 0;
            progressBar1.Visible = true;
            progressBar1.Step = 1;

            foreach (DataRow dr in dt.Rows)
            {
                progressBar1.PerformStep();
                MasterPolicy = "";
                ContractNumber = "";
                ClaimNumber = "";
                claimSequenceNumber = "";
                dateOfLoss = "";
                claimDate = "";
                ContractName = "";
                effectiveDate = "";
                contractExperationDate = "";
                vehicleType = "";
                vehicleYear = 0;
                vehicleMake = "";
                vehicleModel = "";
                VIN = "";
                closeWithoutPay = "";
                sellingDealerName = "";
                sellingDealerCode = "";
                dateClaimSentToInsuranceCarrier = "";
                dateClaimMailed = "";
                insurerApprovalNumber = "";
                claimAdjuster = "";
                serviceAdvisor = "";
                techninian = "";
                complaint = "";
                failCode = "";
                failComponent = "";
                dateDenied = "";
                denialReason = "";
                lossMileage = 0;
                claimStatus = "";
                saleMilage = 0;
                expirationMilage = 0;
                contractFormNumber = "";
                coverageCode = "";
                componmentCoverageGroup = "";
                componentRepairCorrection = "";
                dianosticTime = "";
                laborTime = 0;
                laborTotal = 0;
                PartCost = 0;
                rental = 0;
                towing = 0;
                tax = 0;
                Deductible = 0;
                goodWill = 0;
                claimTotal = 0;
                checkNumber = "";
                checkDate = "";
                creditCardNumber = 0;
                pendingTotalAmount = 0;
                repairFacilityName = "";
                repairFacilityCode = "";
                transientClaim = "";
                repairFacilityAddress = "";
                repairFacilityCity = "";
                repairFacilityState = "";
                repairFacilityZip = "";
                repairFacilityPhoneNumber = "";
                repairFacilityEmailAddress = "";
                repairFacilityFaxNumber = "";
                try
                {
                    MasterPolicy = Convert.ToString(dr["MasterPolicy"]);
                }
                catch
                {
                }
                try
                {
                    ContractNumber = Convert.ToString(dr["ContractNumber"]);
                }
                catch
                {
                }
                try
                {
                    ClaimNumber = Convert.ToString(dr["ClaimNumber"]);
                }
                catch
                {
                }
                try
                {
                    claimSequenceNumber = Convert.ToString(dr["claimSequenceNumber"]);
                }
                catch
                {
                }
                try
                {
                    dateOfLoss = Convert.ToString(dr["dateOfLoss"]);
                }
                catch
                {
                }
                try
                {
                    claimDate = Convert.ToString(dr["claimDate"]);
                }
                catch
                {
                }
                try
                {
                    ContractName = Convert.ToString(dr["ContractName"]);
                }
                catch
                {
                }
                try
                {
                    effectiveDate = Convert.ToString(dr["effectiveDate"]);
                }
                catch
                {
                }
                try
                {
                    contractExperationDate = Convert.ToString(dr["contractExperationDate"]);
                }
                catch
                {
                }
                try
                {
                    vehicleType = Convert.ToString(dr["vehicleType"]);
                }
                catch
                {
                }
                try
                {
                    vehicleYear = Convert.ToInt32(dr["vehicleYear"]);
                }
                catch
                {
                }
                try
                {
                    vehicleMake = Convert.ToString(dr["vehicleMake"]);
                }
                catch
                {
                }
                try
                {
                    vehicleModel = Convert.ToString(dr["vehicleModel"]);
                }
                catch
                {
                }
                try
                {
                    VIN = Convert.ToString(dr["VIN"]);
                }
                catch
                {
                }
                try
                {
                    closeWithoutPay = Convert.ToString(dr["closeWithoutPay"]);
                }
                catch
                {
                }
                try
                {
                    sellingDealerName = Convert.ToString(dr["sellingDealerName"]);
                }
                catch
                {
                }
                try
                {
                    sellingDealerCode = Convert.ToString(dr["sellingDealerCode"]);
                }
                catch
                {
                }
                try
                {
                    dateClaimSentToInsuranceCarrier = Convert.ToString(dr["dateClaimSentToInsuranceCarrier"]);
                }
                catch
                {
                }
                try
                {
                    dateClaimMailed = Convert.ToString(dr["dateClaimMailed"]);
                }
                catch
                {
                }
                try
                {
                    insurerApprovalNumber = Convert.ToString(dr["insurerApprovalNumber"]);
                }
                catch
                {
                }
                try
                {
                    claimAdjuster = Convert.ToString(dr["claimAdjuster"]);
                }
                catch
                {
                }
                try
                {
                    serviceAdvisor = Convert.ToString(dr["serviceAdvisor"]);
                }
                catch
                {
                }
                try
                {
                    techninian = Convert.ToString(dr["techninian"]);
                }
                catch
                {
                }
                try
                {
                    complaint = Convert.ToString(dr["complaint"]);
                }
                catch
                {
                }
                try
                {
                    failCode = Convert.ToString(dr["failCode"]);
                }
                catch
                {
                }
                try
                {
                    failComponent = Convert.ToString(dr["failComponent"]);
                }
                catch
                {
                }
                try
                {
                    dateDenied = Convert.ToString(dr["dateDenied"]);
                }
                catch
                {
                }
                try
                {
                    denialReason = Convert.ToString(dr["denialReason"]);
                }
                catch
                {
                }
                try
                {
                    lossMileage = Convert.ToDouble(dr["lossMileage"]);
                }
                catch
                {
                }
                try
                {
                    claimStatus = Convert.ToString(dr["claimStatus"]);
                }
                catch
                {
                }
                try
                {
                    saleMilage = Convert.ToDouble(dr["saleMilage"]);
                }
                catch
                {
                }
                try
                {
                    expirationMilage = Convert.ToDouble(dr["expirationMilage"]);
                }
                catch
                {
                }
                try
                {
                    contractFormNumber = Convert.ToString(dr["contractFormNumber"]);
                }
                catch
                {
                }
                try
                {
                    coverageCode = Convert.ToString(dr["coverageCode"]);
                }
                catch
                {
                }
                try
                {
                    componmentCoverageGroup = Convert.ToString(dr["componmentCoverageGroup"]);
                }
                catch
                {
                }
                try
                {
                    componentRepairCorrection = Convert.ToString(dr["componentRepairCorrection"]);
                }
                catch
                {
                }
                try
                {
                    dianosticTime = Convert.ToString(dr["dianosticTime"]);
                }
                catch
                {
                }
                try
                {
                    laborTime = Convert.ToDouble(dr["laborTime"]);
                }
                catch
                {
                }
                try
                {
                    laborTotal = Convert.ToDouble(dr["laborTotal"]);
                }
                catch
                {
                }
                try
                {
                    PartCost = Convert.ToDouble(dr["PartCost"]);
                }
                catch
                {
                }
                try
                {
                    rental = Convert.ToDouble(dr["rental"]);
                }
                catch
                {
                }
                try
                {
                    towing = Convert.ToDouble(dr["towing"]);
                }
                catch
                {
                }
                try
                {
                    tax = Convert.ToDouble(dr["tax"]);
                }
                catch
                {
                }
                try
                {
                    Deductible = Convert.ToDouble(dr["Deductible"]);
                }
                catch
                {
                }
                try
                {
                    goodWill = Convert.ToDouble(dr["goodWill"]);
                }
                catch
                {
                }
                try
                {
                    claimTotal = Convert.ToDouble(dr["claimTotal"]);
                }
                catch
                {
                }
                try
                {
                    checkNumber = Convert.ToString(dr["checkNumber"]);
                }
                catch
                {
                }
                try
                {
                    checkDate = Convert.ToString(dr["checkDate"]);
                }
                catch
                {
                }
                try
                {
                    creditCardNumber = Convert.ToInt32(dr["creditCardNumber"]);
                }
                catch
                {
                }
                try
                {
                    pendingTotalAmount = Convert.ToDouble(dr["pendingTotalAmount"]);
                }
                catch
                {
                }
                try
                {
                    repairFacilityName = Convert.ToString(dr["repairFacilityName"]);
                }
                catch
                {
                }
                try
                {
                    repairFacilityCode = Convert.ToString(dr["repairFacilityCode"]);
                }
                catch
                {
                }
                try
                {
                    transientClaim = Convert.ToString(dr["transientClaim"]);
                }
                catch
                {
                }
                try
                {
                    repairFacilityAddress = Convert.ToString(dr["repairFacilityAddress"]);
                }
                catch
                {
                }
                try
                {
                    repairFacilityCity = Convert.ToString(dr["repairFacilityCity"]);
                }
                catch
                {
                }
                try
                {
                    repairFacilityState = Convert.ToString(dr["repairFacilityState"]);
                }
                catch
                {
                }
                try
                {
                    repairFacilityZip = Convert.ToString(dr["repairFacilityZip"]);
                }
                catch
                {
                }
                try
                {
                    repairFacilityPhoneNumber = Convert.ToString(dr["repairFacilityPhoneNumber"]);
                }
                catch
                {
                }
                try
                {
                    repairFacilityEmailAddress = Convert.ToString(dr["repairFacilityEmailAddress"]);
                }
                catch
                {
                }
                try
                {
                    repairFacilityFaxNumber = Convert.ToString(dr["repairFacilityFaxNumber"]);
                }
                catch
                {
                }
                if (creditCardNumber <= 0)
                {
                    Random rando = new Random();
                    int randomNumber = 0;
                    sql = "select * from claimachrandomnumber where claimno = '" + ClaimNumber + "'";
                    System.Data.DataTable random = ClaimsDBO.dboInportInformation(sql);
                    foreach (DataRow dataRow in random.Rows)
                    {
                        try
                        {
                            randomNumber = Convert.ToInt32(dataRow["randomnumber"]);
                        }
                        catch
                        {
                            randomNumber = -1;
                        }
                    }

                    if (randomNumber <= 0)
                    {
                        bool exists = false;
                        randomNumber = rando.Next(100000000, 999999999);
                        while (!exists)
                        {
                            string check = "select * from claimachrandomnumber where RandomNumber = '" + randomNumber + "'";
                            ClaimsDBO.dboInportInformation(check);
                            exists = ClaimsDBO.hasRows();
                            if (exists)
                            {
                                randomNumber = rando.Next(100000000, 999999999);
                            }
                            else
                            {
                                exists = true;
                            }

                        }
                        checkNumber = randomNumber.ToString();
                        string insertRando = "insert into claimachrandomnumber(ClaimNo,RandomNumber) values('" + ClaimNumber + "'," + checkNumber + ")";
                        ClaimsDBO.dboAlterDBOUnsafe(insertRando);
                    }
                    else
                    {
                        checkNumber = randomNumber.ToString();
                    }

                }
                else
                {
                    checkDate = "";
                }
                if (checkNumber == "Calculate in code")
                {
                    checkNumber = "ACH";
                }
                if (laborTotal > 0)
                {
                    laborTime = laborTotal / laborTime;
                }

                if (vehicleType == "9" || vehicleType == "62")
                {
                    vehicleType = "Motorcycle";
                }
                else if (vehicleType == "63" || vehicleType == "10")
                {
                    vehicleType = "RV";
                }
                else
                {
                    vehicleType = "Car";
                }
                componmentCoverageGroup = coverageCode;
                componmentCoverageGroup = componmentCoverageGroup.ToUpper();
                if (componmentCoverageGroup.Substring(0, 2) == "AC")
                {
                    componmentCoverageGroup = "AC";
                }
                else if (componmentCoverageGroup.Substring(0, 2) == "BR")
                {
                    componmentCoverageGroup = "BRAKE";
                }
                else if (componmentCoverageGroup.Substring(0, 2) == "CO")
                {
                    componmentCoverageGroup = "COOLING";
                }
                else if (componmentCoverageGroup.Substring(0, 2) == "DA")
                {
                    componmentCoverageGroup = "DRIVETRAIN";
                }
                else if (componmentCoverageGroup.Substring(0, 2) == "EL")
                {
                    componmentCoverageGroup = "ELECTICAL";
                }
                else if (componmentCoverageGroup.Substring(0, 2) == "EN")
                {
                    componmentCoverageGroup = "ENGINE";
                }
                else if (componmentCoverageGroup.Substring(0, 2) == "FU")
                {
                    componmentCoverageGroup = "FUEL";
                }
                else if (componmentCoverageGroup.Substring(0, 2) == "HY")
                {
                    componmentCoverageGroup = "ENGINE";
                }
                else if (componmentCoverageGroup.Substring(0, 2) == "IE")
                {
                    componmentCoverageGroup = "MISC";
                }
                else if (componmentCoverageGroup.Substring(0, 2) == "IF")
                {
                    componmentCoverageGroup = "MISC";
                }
                else if (componmentCoverageGroup.Substring(0, 2) == "RR")
                {
                    componmentCoverageGroup = "MISC";
                }
                else if (componmentCoverageGroup.Substring(0, 2) == "SP")
                {
                    componmentCoverageGroup = "MISC";
                }
                else if (componmentCoverageGroup.Substring(0, 2) == "ST")
                {
                    componmentCoverageGroup = "STEERING";
                }
                else if (componmentCoverageGroup.Substring(0, 2) == "SU")
                {
                    componmentCoverageGroup = "SUSPENSION";
                }
                else if (componmentCoverageGroup.Substring(0, 1) == "T")
                {
                    componmentCoverageGroup = "TRANSMISSION";
                }

                writeOut = "";

                writeOut = writeOut + MasterPolicy + "\t";
                writeOut = writeOut + ContractNumber + "\t";
                writeOut = writeOut + ClaimNumber + "\t";
                writeOut = writeOut + claimSequenceNumber + "\t";
                writeOut = writeOut + dateOfLoss + "\t";
                writeOut = writeOut + claimDate + "\t";
                writeOut = writeOut + ContractName + "\t";
                writeOut = writeOut + effectiveDate + "\t";
                writeOut = writeOut + contractExperationDate + "\t";
                writeOut = writeOut + vehicleType + "\t";
                writeOut = writeOut + vehicleYear + "\t";
                writeOut = writeOut + vehicleMake + "\t";
                writeOut = writeOut + vehicleModel + "\t";
                writeOut = writeOut + VIN + "\t";
                writeOut = writeOut + closeWithoutPay + "\t";
                writeOut = writeOut + sellingDealerName + "\t";
                writeOut = writeOut + sellingDealerCode + "\t";
                writeOut = writeOut + dateClaimSentToInsuranceCarrier + "\t";
                writeOut = writeOut + dateClaimMailed + "\t";
                writeOut = writeOut + insurerApprovalNumber + "\t";
                writeOut = writeOut + claimAdjuster + "\t";
                writeOut = writeOut + serviceAdvisor + "\t";
                writeOut = writeOut + techninian + "\t";
                writeOut = writeOut + complaint + "\t";
                writeOut = writeOut + failCode + "\t";
                writeOut = writeOut + failComponent + "\t";
                writeOut = writeOut + dateDenied + "\t";
                writeOut = writeOut + denialReason + "\t";
                writeOut = writeOut + lossMileage + "\t";
                writeOut = writeOut + claimStatus + "\t";
                writeOut = writeOut + saleMilage + "\t";
                writeOut = writeOut + expirationMilage + "\t";
                writeOut = writeOut + contractFormNumber + "\t";
                writeOut = writeOut + coverageCode + "\t";
                writeOut = writeOut + componmentCoverageGroup + "\t";
                writeOut = writeOut + componentRepairCorrection + "\t";
                writeOut = writeOut + dianosticTime + "\t";
                writeOut = writeOut + laborTime + "\t";
                writeOut = writeOut + laborTotal + "\t";
                writeOut = writeOut + PartCost + "\t";
                writeOut = writeOut + rental + "\t";
                writeOut = writeOut + towing + "\t";
                writeOut = writeOut + tax + "\t";
                writeOut = writeOut + Deductible + "\t";
                writeOut = writeOut + goodWill + "\t";
                writeOut = writeOut + claimTotal + "\t";
                writeOut = writeOut + checkNumber + "\t";
                writeOut = writeOut + checkDate + "\t";
                writeOut = writeOut + creditCardNumber + "\t";
                writeOut = writeOut + pendingTotalAmount + "\t";
                writeOut = writeOut + repairFacilityName + "\t";
                writeOut = writeOut + repairFacilityCode + "\t";
                writeOut = writeOut + transientClaim + "\t";
                writeOut = writeOut + repairFacilityAddress + "\t";
                writeOut = writeOut + repairFacilityCity + "\t";
                writeOut = writeOut + repairFacilityState + "\t";
                writeOut = writeOut + repairFacilityZip + "\t";
                writeOut = writeOut + repairFacilityPhoneNumber + "\t";
                writeOut = writeOut + repairFacilityEmailAddress + "\t";
                writeOut = writeOut + repairFacilityFaxNumber + "\t";
                writeOut = writeOut + "\n";
                File.AppendAllText(setPath() + "/MonthEnd/" + setMonth() + "/Arch/Claims_" + setMonth() + "2.txt", writeOut);

            }
            progressBar1.Value = 0;
            lblProgressBar.Text = "";
            ClaimsDBO.dboClose();
        }

            //Old Republic
        private void oldRepublicContract()
        {
            int iFirstOrSecondRun = 0;
            while (iFirstOrSecondRun < 2)
            {
                string sLine = "";
                CSharpDBO.CSharpDBO dBO = new CSharpDBO.CSharpDBO();
                dBO.dboOpen(path);
                string difference = "";
                if (iFirstOrSecondRun == 0)
                {
                    difference = "_all_Other_states";
                }
                else
                {
                    difference = "_WA_FL,CAVSC_Ancillary_GAP";
                }
                string sFilePath = setPath() + "/MonthEnd/" + setMonth() + "/OldRep/ " + setYear() + "-" + setMonthNo() + "_Veritas_SalesOldRepublic"+ difference + ".txt";
                StreamWriter streamWriterContract = new StreamWriter(sFilePath);
                string sSQL = "select 'ORUS' as TPA,'VER551' as agentNumber, d.DealerNo as dealerno, c.ContractNo as ContractNum, c.FormNo as contractFormNo," +
                    "'' as batchNumber, '' as Filler, 'A' as contractStatus, '' as contractHolderNameTitle, c.FName as ContractHolderFirstName, " +
                    "c.LName as contractHolderLastName,'' as ContractHolderMiddleName, '' as ContractHolderSpouse, c.Addr1 as " +
                    "ContractHolderAddress1, c.City as ContractHolderCity, c.State as ContractHolderState, c.Zip as ContractHolderPostalCode, " +
                    "c.Phone as contractHolderPhoneNumber, '' as ContractHolderWorkPhoneNumber, '' as ContracHolderPhoneNumberExtenstion1, " +
                    "'' as ContractHolderPhoneNumberExtention2, '' as ContractHolderEmail, '' as Filler, c.SaleMile as contractSaleOdometer, " +
                    "'' as Filler, '' as Filler, '' as Filler, '' as Filler, Year as vehicleYear, c.VIN as vin, '' as Filler, '' as ManufacturerWarrantyTerm, " +
                    "'' as ManufacturerWarrantyMileage, c.SaleDate as contractSaleDate, c.CustomerCost as VehiclePurchasePrice, '' as Filler, '' as Filler, " +
                    "'' as Filler, '' as Filler, '' as Filler,'' as ReinsuranceID, '' as Filler,'' as Filler, '' as Filler, '' as Filler, '' as Filler, " +
                    "'' as VehicleInServiceDate, '' as Filler, '' as Filler, '' as Filler, '' as Filler, '' as noChargeBackFlag, '' as ContractEntryDate, " +
                    "'' as Filler,c.PlanTypeID as PlanCode, RateBookID as RateBookId, c.NewUsed as NewUsed,dd.DeductAmt as DeductableAmount ," +
                    "'0' as DeductibleType, c.TermMonth as ContractTermMonths, c.TermMile as TermMiles,'' as Filler, '' as Filler, " +
                    "'' as Filler, '' as Filler, '' as Filler, '' as Filler, '' as Filler, '' as Filler, '' as Filler, '' as Filler, '' as Filler, " +
                    "'' as Filler, '' as Filler, '' as Filler, '' as Filler,'' as Filler, '' as Filler, '' as Filler, '' as Filler, '' as Filler, " +
                    "'' as Filler, '' as Filler, '' as Filler, '' as Filler, 'EN' as ContractHolderLanguageCode, '' as Filler, '' as Filler, '' as Filler, " +
                    "'' as Filler, '' as Filler, '' as Filler, '' as Filler, '' as Filler, '' as Filler, '' as Filler, '' as Filler, '' as Filler, " +
                    "(select sum(amt) from contractamt where contractid = c.contractid and ratetypeid in (select RateTypeID from RateType where RateCategoryID in (4,3))) as Premium, " +
                    "'' as Filler, '' as Filler, '' as Filler,'' as Filler, '' as Filler, '' as Filler, '' as Filler, '' as Filler, " +
                    "'' as Filler, '' as Filler, '' as Filler, '' as Filler, '' as Filler, '' as Filler, '' as Filler, '' as Filler, '' as Filler, '' as Filler," +
                    "'' as Filler, '' as Filler, '' as Filler, '' as Filler, '' as Filler, '' as Filler, '' as Filler, '' as Filler, c.CustomerCost as RetailRate, " +
                    "'' as Filler, '' as Filler, '' as sLien_name,'' as slien_address_1, '' as slien_address_2,'' as sLien_city,'' as slien_state,'' as slien_zip_code," +
                    "'' as slien_phone_number, '' as slien_contact, '' as slien_federal_tax_id, '' as scontract_entry_app_name, '' as paymentOption, " +
                    "'' as Filler, '' as Filler, '' as Filler,'' as Filler, '' as Filler, '' as Filler, '' as Filler, '' as Filler, " +
                    "'' as Filler, '' as Filler, '' as Filler,'' as Filler, '' as Filler, '' as Filler, '' as Filler, '' as Filler, " +
                    "'' as Filler, '' as Filler, '' as Filler,'' as Filler, '' as Filler, '' as Filler, '' as Filler, '' as Filler, " +
                    "'' as Filler, '' as Filler, '' as Filler,'' as Filler, '' as Filler, rt3.amt as clipFee " +
                    "from contract c " +
                    "left join Agents a on a.AgentID = c.AgentsID " +
                    "left join dealer d on d.DealerID = c.DealerID " +
                    "left join Deductible dd on dd.DeductID = c.DeductID " +
                    "left join (select ContractID, sum(Amt) as amt from ContractAmt where RateTypeID = 8 group by ContractID) as rt3 on rt3.ContractID = c.ContractID " +
                    "where InsCarrierID = 4 " +
                    "and (c.status = 'Paid' or c.Status = 'Expired' or c.status = 'Cancelled') " +
                    "and c.datePaid <= '" + dtpEndDate.Value + "' " +
                    "and c.datePaid >= '" + dtpStartDate.Value + "' " +
                    "and inssaletransmissiondate is null " +
                    "and not c.DatePaid is null "; 

                if (iFirstOrSecondRun == 0)
                {
                    sSQL = sSQL + "and not c.contractid in (select ContractID from contract where InsCarrierID = 4 and state in ('WA', 'FL', 'CA') or (programid = 67))";
                    sSQL = sSQL + "and not c.contractno in (select contractno from OldRepublicContractAdd where AddOrRemove = 'R')";
                    sSQL = sSQL + "or c.contractno in (select contractNo from OldRepublicContractAdd where AddOrRemove = 'A' and Status = 'Paid' and not contractno in (select contractno from contract where InsCarrierID = 4 and state in ('WA', 'FL', 'CA') or (programid = 67)))";                    
                    //sSQL = sSQL + "and not c.ContractNo in (select ContractNo from ContractTemp) or c.ContractNo in (select ContractNo from ContractTemp3)";
                }
                else
                {
                    sSQL = sSQL + "and c.contractid in (select ContractID from contract where InsCarrierID = 4 and state in ('WA', 'FL', 'CA') or (programid = 67))";
                    sSQL = sSQL + "and not c.contractno in (select contractno from OldRepublicContractAdd where AddOrRemove = 'R')";
                    sSQL = sSQL + "or c.contractno in (select contractNo from OldRepublicContractAdd where AddOrRemove = 'A' and Status = 'Paid' and contractno in (select contractno from contract where InsCarrierID = 4 and state in ('WA', 'FL', 'CA') or (programid = 67)))";                    
                    //sSQL = sSQL + "and not c.ContractNo in (select ContractNo from ContractTemp) or c.ContractNo in (select ContractNo from ContractTemp3)";
                }



                lblProgressBar.Text = "Executing Querry";
                lblProgressBar.Refresh();
                System.Data.DataTable dataTable = dBO.dboInportInformation(sSQL);
                dBO.dboClose();
                int tracker = dataTable.Rows.Count;
                lblProgressBar.Text = "Processing Old Rep Sales";
                lblProgressBar.Refresh();
                lblContract.Text = "Paid Contracts:" + tracker;
                lblContract.Refresh();
                progressBar1.Value = 0;
                progressBar1.Maximum = tracker;
                progressBar1.Minimum = 0;
                progressBar1.Visible = true;
                progressBar1.Step = 1;

                if (dataTable.Rows.Count > 0)
                {
                    foreach (DataRow dr in dataTable.Rows)
                    {
                        progressBar1.PerformStep();
                        sLine = "";
                        try
                        {
                            sLine = dr["TPA"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//1 TPA
                        try
                        {
                            sLine = sLine + dr["agentNumber"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//2 agentNumber                    
                        try
                        {
                            sLine = sLine + dr["dealerno"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//3 DealerNo                    
                        try
                        {
                            sLine = sLine + dr["ContractNum"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//4 ContractNumber                    
                        try
                        {
                            sLine = sLine + dr["contractFormNo"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//5 Contract FormNo                    
                        try
                        {
                            sLine = sLine + dr["batchNumber"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//6 batchNumber                                        
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//7 Filler                    
                        try
                        {
                            sLine = sLine + dr["contractStatus"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//8 contractStatus                    
                        try
                        {
                            sLine = sLine + dr["contractHolderNameTitle"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//9 contractHolderNameTitle                    
                        try
                        {
                            sLine = sLine + dr["ContractHolderFirstName"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//10 ContractHolderFirstName                    
                        try
                        {
                            sLine = sLine + dr["ContractHolderLastName"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//11 ContractHolderLastName                    
                        try
                        {
                            sLine = sLine + dr["ContractHolderMiddleName"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//12 ContractHolderMiddleName                    
                        try
                        {
                            sLine = sLine + dr["ContractHolderSpouse"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//13 ContractHolderSpouse                    
                        try
                        {
                            sLine = sLine + dr["ContractHolderAddress1"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//14 ContractHolderAddress1                    
                        try
                        {
                            sLine = sLine + dr["ContractHolderCity"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//15 ContractHolderCity                    
                        try
                        {
                            sLine = sLine + dr["ContractHolderState"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//16 ContractHolderState                    
                        try
                        {
                            sLine = sLine + dr["ContractHolderPostalCode"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//17 ContractHolderPostalCode                    
                        try
                        {
                            sLine = sLine + dr["contractHolderPhoneNumber"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//18 contractHolderPhoneNumber                    
                        try
                        {
                            sLine = sLine + dr["ContractHolderWorkPhoneNumber"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//19 ContractHolderWorkPhoneNumber
                        try
                        {
                            sLine = sLine + dr["ContracHolderPhoneNumberExtenstion1"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//20 ContracHolderPhoneNumberExtenstion1                    
                        try
                        {
                            sLine = sLine + dr["ContractHolderPhoneNumberExtention2"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//21 ContractHolderPhoneNumberExtention2                    
                        try
                        {
                            sLine = sLine + dr["ContractHolderEmail"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//22 ContractHolderEmail                    
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//23 Filler                    
                        try
                        {
                            sLine = sLine + dr["contractSaleOdometer"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//24 contractSaleOdometer                    
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//25 Filler                    
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//26 Filler                    
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//27 Filler                    
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//28 Filler                    
                        try
                        {
                            sLine = sLine + dr["vehicleYear"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//29 vehicleYear                    
                        try
                        {
                            sLine = sLine + dr["vin"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//30 Vin                    
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//31 Filler                    
                        try
                        {
                            sLine = sLine + dr["ManufacturerWarrantyTerm"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//32 ManufacturerWarrantyTerm                    
                        try
                        {
                            sLine = sLine + dr["ManufacturerWarrantyMileage"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//33 ManufacturerWarrantyMileage                    
                        try
                        {
                            sLine = sLine + Convert.ToDateTime(dr["contractSaleDate"]).ToString("MM/dd/yyyy") + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//34 contractSaleDate                    
                        try
                        {
                            sLine = sLine + dr["VehiclePurchasePrice"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//35 VehiclePurchasePrice                    
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//36 Filler                    
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//37 Filler                    
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//38 Filler                    
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//39 Filler                    
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//40 Filler                    
                        try
                        {
                            sLine = sLine + dr["ReinsuranceID"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//41 ReinsuranceID                    
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//42 Filler                    
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//43 Filler                    
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//44 Filler                    
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//45 Filler                    
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//46 Filler                    
                        try
                        {
                            sLine = sLine + Convert.ToDateTime(dr["VehicleInServiceDate"]).ToString("MM/dd/yyyy") + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//47 VehicleInServiceDate                    
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//48 Filler                    
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//49 Filler                    
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//50 Filler                    
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//51 Filler                    
                        try
                        {
                            sLine = sLine + dr["noChargeBackFlag"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//52 noChargeBackFlag                    
                        try
                        {
                            sLine = sLine + Convert.ToDateTime(dr["ContractEntryDate"]).ToString("MM/dd/yyyy") + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//53 ContractEntryDate                    
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//54 Filler                    
                        try
                        {
                            sLine = sLine + dr["PlanCode"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//55 PlanCode                    
                        try
                        {
                            sLine = sLine + dr["RateBookId"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//56 RateBookId                    
                        try
                        {
                            sLine = sLine + dr["NewUsed"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//57 NewUsed                    
                        try
                        {
                            sLine = sLine + dr["DeductableAmount"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//58 DeductableAmount                    
                        try
                        {
                            sLine = sLine + dr["DeductibleType"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//59 DeductibleType                    
                        try
                        {
                            sLine = sLine + dr["ContractTermMonths"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//60 ContractTermMonths                    
                        try
                        {
                            sLine = sLine + dr["TermMiles"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//61 TermMiles                    
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//62 Filler                    
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//63 Filler                    
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//64 Filler                    
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//65 Filler                    
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//66 Filler                    
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//67 Filler                    
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//68 Filler                    
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//69 Filler                    
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//70 Filler
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//71 Filler
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//72 Filler
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//73 Filler
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//74 Filler
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//75 Filler
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//76 Filler
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//77 Filler
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//78 Filler
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//79 Filler
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//80 Filler
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//81 Filler                    
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//82 Filler                    
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//83 Filler                    
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//84 Filler
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//85 Filler
                        try
                        {
                            sLine = sLine + dr["ContractHolderLanguageCode"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//86 ContractHolderLanguageCode                    
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//87 Filler                    
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//88 Filler                    
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//89 Filler                    
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//90 Filler                    
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//91 Filler                    
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//92 Filler                    
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//93 Filler
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//94 Filler
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//95 Filler
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//96 Filler
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//97 Filler
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//98 Filler                    
                        try
                        {
                            double dPremium = Math.Round(Convert.ToDouble(dr["Premium"]), 2);
                            sLine = sLine + dPremium + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//99 Premium                    
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//100 Filler                    
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//101 Filler                    
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//102 Filler                    
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//103 Filler                    
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//104 Filler                    
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//105 Filler                    
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//106 Filler                    
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//107 Filler                    
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//108 Filler                    
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//109 Filler                    
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//110 Filler                    
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//111 Filler                    
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//112 Filler                    
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//113 Filler                    
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//114 Filler                    
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//115 Filler                    
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//116 Filler                    
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//117 Filler                    
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//118 Filler                    
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//119 Filler                    
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//120 Filler                    
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//121 Filler                    
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//122 Filler                    
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//123 Filler                    
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//124 Filler                    
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//125 Filler
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//126 Filler
                        try
                        {
                            sLine = sLine + dr["RetailRate"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//127 RetailRate                    
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//128 Filler                    
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//129 Filler                    
                        try
                        {
                            sLine = sLine + dr["sLien_name"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//130 sLien_name      
                        try
                        {
                            sLine = sLine + dr["slien_address_1"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//131 slien_address_1                    
                        try
                        {
                            sLine = sLine + dr["slien_address_2"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//132 slien_address_2                    
                        try
                        {
                            sLine = sLine + dr["sLien_city"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//133 sLien_city                    
                        try
                        {
                            sLine = sLine + dr["slien_state"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//134 slien_state                    
                        try
                        {
                            sLine = sLine + dr["slien_zip_code"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//135 slien_zip_code                    
                        try
                        {
                            sLine = sLine + dr["slien_phone_number"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//136 slien_phone_number                    
                        try
                        {
                            sLine = sLine + dr["slien_contact"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//137 slien_contact                    
                        try
                        {
                            sLine = sLine + dr["slien_federal_tax_id"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//138 slien_federal_tax_id                    
                        try
                        {
                            sLine = sLine + dr["scontract_entry_app_name"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//139 scontract_entry_app_name                    
                        try
                        {
                            sLine = sLine + dr["paymentOption"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//140 paymentOption
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//141 Filler
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//142 Filler
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//143 Filler
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//144 Filler
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//145 Filler
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//146 Filler
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//147 Filler
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//148 Filler
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//149 Filler                    
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//150 Filler
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//151 Filler
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//152 Filler
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//153 Filler
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//154 Filler
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//155 Filler
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//156 Filler
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//157 Filler
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//158 Filler
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//159 Filler
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//160 Filler
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//161 Filler
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//162 Filler
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//163 Filler
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//164 Filler
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//165 Filler
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//166 Filler
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//167 Filler
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//168 Filler
                        try
                        {
                            sLine = sLine + dr["Filler"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//169 Filler
                        try
                        {
                            sLine = sLine + dr["clipFee"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//170 clipFee
                        streamWriterContract.WriteLine(sLine);
                    }
                }
                streamWriterContract.Close();
                iFirstOrSecondRun++;
            }
            


        }
        private void oldRepublicCancel()
        {

            int iFirstOrSecondRun = 0;
            while (iFirstOrSecondRun < 2)
            {
                string sLine = "";
                CSharpDBO.CSharpDBO dBO = new CSharpDBO.CSharpDBO();
                dBO.dboOpen(path);
                string difference = "";
                if (iFirstOrSecondRun == 0)
                {
                    difference = "_all_Other_states";
                }
                else
                {
                    difference = "_WA_FL,CAVSC_Ancillary_GAP";
                }
                string sFilePath = setPath() + "/MonthEnd/" + setMonth() + "/OldRep/ " + setYear() + "-" + setMonthNo() + "_Veritas_CancelOldRepublic" + difference + ".txt";
                StreamWriter streamWriterContract = new StreamWriter(sFilePath);
                string sSQL = "select distinct 'ORUS' as TPA, c.ContractNo as contractno, cc.CancelMile, cc.CancelEffDate as contractEffCancel, " +
                    "cc.RequestDate as cancelReceivedDate, 'BR' as cancelMethodCode, 'H' as cancelReasonCode, 'N' as cancelCalcBaseCode, " +
                    "(select sum(cancelamt) from ContractAmt where RateTypeID in (select RateTypeID from RateType where RateCategoryID in(4,3)) and contractid = c.contractid) as RefundAmount," +
                    "'' as cancelNote,'7' as cancelPayeeType , " +
                    "(case when p.PayeeType like 'Dealer' then p.CompanyName else p.FName + ' ' + p.LName end) as cancelPayeeName, " +
                    "p.Addr1 as cancelPayeeaddr1, p.Addr2 as cancelPayeeaddr2, p.City as cancelPayeeCity, p.State as cancelPayeeState, " +
                    "p.Zip as cancelPayeeZipCode, 'DSC' as payMethodCode,(select CancelAmt from ContractAmt where RateTypeID = 8 and ContractID = c.contractid) as clipCancellation from Contract c " +
                    "left join ContractCancel cc on cc.ContractID = c.ContractID " +
                    "left join ContractAmt ca on ca.ContractID = c.ContractID " +
                    "left join Payee p on p.PayeeID = cc.PayeeID " +
                    "where c.Status = 'Cancelled' " +
                    "and InsCarrierID = 4 " +
                    "and cc.CancelDate <= '" + dtpEndDate.Value + "' " +
                    "and cc.CancelDate >= '" + dtpStartDate.Value + "' " +
                    "and INSCancelTransmissionDate is null " +
                    "and not c.DatePaid is null ";

                if (iFirstOrSecondRun == 0)
                {
                    sSQL = sSQL + "and not c.contractid in (select ContractID from contract where InsCarrierID = 4 and state in ('WA', 'FL', 'CA') or(programid = 67 ))";
                    sSQL = sSQL + "or c.contractno in (select contractNo from OldRepublicContractAdd where AddOrRemove = 'A' and Status = 'Cancelled' and not contractno in (select contractno from contract where InsCarrierID = 4 and state in ('WA', 'FL', 'CA') or(programid = 67))) " +
                   "and not c.contractno in (select contractno from OldRepublicContractAdd where AddOrRemove = 'R')" +
                   "group by c.contractid, c.ContractNo, cc.CancelMile, cc.CancelEffDate, cc.RequestDate,p.PayeeType,p.CompanyName,p.LName,p.FName,p.Addr1,p.Addr2,p.City,p.State,p.Zip";
                }
                else
                {
                    sSQL = sSQL + "and c.contractid in (select ContractID from contract where InsCarrierID = 4 and state in ('WA', 'FL', 'CA') or(programid = 67 ))";
                    sSQL = sSQL + "or c.contractno in (select contractNo from OldRepublicContractAdd where AddOrRemove = 'A' and Status = 'Cancelled' and contractno in (select contractno from contract where InsCarrierID = 4 and state in ('WA', 'FL', 'CA') or(programid = 67)))" +
                   "and not c.contractno in (select contractno from OldRepublicContractAdd where AddOrRemove = 'R')" +
                   "group by c.contractid, c.ContractNo, cc.CancelMile, cc.CancelEffDate, cc.RequestDate,p.PayeeType,p.CompanyName,p.LName,p.FName,p.Addr1,p.Addr2,p.City,p.State,p.Zip";
                }
                lblProgressBar.Text = "Executing Querry";
                lblProgressBar.Refresh();
                System.Data.DataTable dataTable = dBO.dboInportInformation(sSQL);
                dBO.dboClose();
                int tracker = dataTable.Rows.Count;
                lblProgressBar.Text = "Processing Old Rep Cancel";
                lblProgressBar.Refresh();
                lbCancel.Text = "Cancel Contracts:" + tracker;
                lbCancel.Refresh();
                progressBar1.Value = 0;
                progressBar1.Maximum = tracker;
                progressBar1.Minimum = 0;
                progressBar1.Visible = true;
                progressBar1.Step = 1;
                if (dataTable.Rows.Count > 0)
                {
                    foreach (DataRow dr in dataTable.Rows)
                    {
                        progressBar1.PerformStep();
                        sLine = "";
                        try
                        {
                            sLine = dr["TPA"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = "\t";
                        }//1 TPA
                        try
                        {
                            sLine = sLine + dr["contractno"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = "\t";
                        }//2 contractno
                        try
                        {
                            sLine = sLine + dr["CancelMile"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = "\t";
                        }//3 CancelMile
                        try
                        {
                            sLine = sLine + dr["contractEffCancel"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = "\t";
                        }//4 contractEffCancel
                        try
                        {
                            sLine = sLine + dr["cancelReceivedDate"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = "\t";
                        }//5 cancelReceivedDate
                        try
                        {
                            sLine = sLine + dr["cancelMethodCode"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = "\t";
                        }//6 cancelMethodCode
                        try
                        {
                            sLine = sLine + dr["cancelReasonCode"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = "\t";
                        }//7 cancelReasonCode
                        try
                        {
                            sLine = sLine + dr["cancelCalcBaseCode"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = "\t";
                        }//8 cancelCalcBaseCode
                        try
                        {
                            double dRfundAmount = Math.Round(Convert.ToDouble(dr["RefundAmount"]),2);
                            sLine = sLine + dRfundAmount.ToString() + "\t";
                        }
                        catch
                        {
                            sLine = "\t";
                        }//9 RefundAmount
                        //try
                        //{
                        //    sLine = sLine + dr["cancelNote"].ToString() + "\t";
                        //}
                        //catch
                        //{
                        //    sLine = "\t";
                        //}//10 cancelNote
                        try
                        {
                            sLine = sLine + dr["cancelPayeeType"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = "\t";
                        }//11 cancelPayeeType
                        try
                        {
                            sLine = sLine + dr["cancelPayeeName"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = "\t";
                        }//12 cancelPayeeName
                        try
                        {
                            sLine = sLine + dr["cancelPayeeaddr1"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = "\t";
                        }//13 cancelPayeeaddr1
                        try
                        {
                            sLine = sLine + dr["cancelPayeeaddr2"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = "\t";
                        }//14 cancelPayeeaddr2
                        try
                        {
                            sLine = sLine + dr["cancelPayeeCity"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = "\t";
                        }//15 cancelPayeeCity
                        try
                        {
                            sLine = sLine + dr["cancelPayeeState"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = "\t";
                        }//16 cancelPayeeState
                        try
                        {
                            sLine = sLine + dr["cancelPayeeZipCode"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = "\t";
                        }//17 cancelPayeeZipCode
                        try
                        {
                            sLine = sLine + dr["payMethodCode"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = "\t";
                        }//18 payMethodCode
                        try
                        {
                            sLine = sLine + dr["clipCancellation"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = "\t";
                        }//18 clipCancellation
                        streamWriterContract.WriteLine(sLine);
                    }
                }
                streamWriterContract.Close();
                iFirstOrSecondRun++;
            }



        }
        private void oldRepublicClaimDetail()
        {
            int iFirstOrSecondRun = 0;

            while (iFirstOrSecondRun < 2)
            {
                string sLine = "";
                CSharpDBO.CSharpDBO dBO = new CSharpDBO.CSharpDBO();
                dBO.dboOpen(path);
                string difference = "";
                if (iFirstOrSecondRun == 0)
                {
                    difference = "_all_Other_states";
                }
                else
                {
                    difference = "_WA_FL,CAVSC_Ancillary_GAP";
                }
                string sFilePath = setPath() + "/MonthEnd/" + setMonth() + "/OldRep/ " + setYear() + "-" + setMonthNo() + "_Veritas_ClaimDetailOldRepublic" + difference + ".txt";
                StreamWriter streamWriterContract = new StreamWriter(sFilePath);

                string sSQL = "select distinct cd.claimdetailid, 'ORUS' as TPA, cl.ClaimNo,(case when cd.ClaimDetailType like 'Deductible' then 'D' else 'O' end) as claimDetailType, " +
                    "cd.LossCode,'' as coveredFlagg, cd.Qty as requesedQuanity, cd.ReqCost as requestedUnitCost, cd.ReqAmt as requestedAmount, " +
                    "cd.AuthQty as authorizedQuantity,cd.AuthCost as authorizedCost, cd.AuthAmt as AuthorizedAmount, cd.TaxPer as salesTaxRate, TaxAmt as SalesTaxAmount, " +
                    "'' as salesTaxRate2, '' as salesTaxAmount2, PaidAmt as claimDetailTotalAmount,  '' as claimDetailStatusID, " +
                    "'S' as claimDetailPayeeTypeCode, cp.PayeeNo as claimDefaultPayeeNumber, cp.PayeeName as claimDetailPayeeName, cp.Addr1 as claimDetailPayeeAddr1, " +
                    "cp.Addr2 as claimDetailPayeeAddr2,  cp.City as claimDetailPayeeCity, cp.State as claimDetailPayeeState, cp.Zip as claimDetailPayeeZip,  " +
                    "'CC' as claimDetailPaymentMethodCode, cd.DatePaid as claimDetailCheckPrintDate from Claim cl " +
                    "left join ClaimDetail cd on cd.ClaimID = cl.ClaimID " +
                    "left join Contract c on c.ContractID = cl.ContractID " +
                    "left join ClaimPayee cp on cp.ClaimPayeeID = cd.ClaimPayeeID " +
                    "where InsCarrierID = 4 " +
                    "and cl.datePaid <= '" + dtpEndDate.Value + "' " +
                    "and cl.datePaid >= '" + dtpStartDate.Value + "' " +
                    "and c.INSCancelTransmissionDate is null " +
                    "and not c.DatePaid is null " +
                    "and cl.Status = 'Paid' " +
                    "and ratetypeid = 1 " +
                    "and cd.ClaimDetailStatus = 'Paid' ";

                if (iFirstOrSecondRun == 0)
                {
                    sSQL = sSQL + "and not cl.contractid in (select ContractID from contract where InsCarrierID = 4 and state in ('WA', 'FL', 'CA') or(programid = 67)) " +
                    //sSQL = sSQL + "and not cl.claimid in (select distinct claimid from claimdetail where not ClaimDetailStatus in ('Authorized','Requested','transmitted')) " +
                    "and not cl.ClaimNo in (select ClaimNo from OldRepublicClaimAdd where AddOrRemove = 'R') " +
                    "or  cd.claimdetailid in (select claimdetailid from claimdetail where claimid in ( select claimid from claim where ClaimNo in (select ClaimNo from OldRepublicClaimAdd where AddOrRemove = 'A') and Exclude = 0 and RateTypeID = 1 and not contractid in (select ContractID from contract where InsCarrierID = 4 and state in ('WA', 'FL', 'CA') or(programid = 67 )))) " +
                    "order by claimno";                    
                }
                else
                {
                    sSQL = sSQL + "and cl.contractid in (select ContractID from contract where InsCarrierID = 4 and state in ('WA', 'FL', 'CA') or(programid = 67)) " + 
                    //sSQL = sSQL + "and not cl.claimid in (select distinct claimid from claimdetail where not ClaimDetailStatus in ('Authorized','Requested','transmitted')) " +
                    "and not cl.ClaimNo in (select ClaimNo from OldRepublicClaimAdd where AddOrRemove = 'R') " +
                    "or  cd.claimdetailid in (select claimdetailid from claimdetail where claimid in ( select claimid from claim where ClaimNo in (select ClaimNo from OldRepublicClaimAdd where AddOrRemove = 'A') and Exclude = 0 and RateTypeID = 1 and contractid in (select ContractID from contract where InsCarrierID = 4 and state in ('WA', 'FL', 'CA') or(programid = 67)))) " +
                    "order by claimno";
                }
                

                lblProgressBar.Text = "Executing Querry";
                lblProgressBar.Refresh();
                System.Data.DataTable dataTable = dBO.dboInportInformation(sSQL);
                dBO.dboClose();
                int tracker = dataTable.Rows.Count;
                lblProgressBar.Text = "Processing Old Rep ClaimDetail";
                lblProgressBar.Refresh();
                lblOther.Text = "Other:" + tracker;
                lbCancel.Refresh();
                progressBar1.Value = 0;
                progressBar1.Maximum = tracker;
                progressBar1.Minimum = 0;
                progressBar1.Visible = true;
                progressBar1.Step = 1;
                if (dataTable.Rows.Count > 0)
                {
                    foreach (DataRow dr in dataTable.Rows)
                    {
                        progressBar1.PerformStep();
                        sLine = "";
                        try
                        {
                            sLine = sLine + dr["TPA"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//1 TPA
                        try
                        {
                            sLine = sLine + dr["ClaimNo"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//2 ClaimNo
                        try
                        {
                            sLine = sLine + dr["claimDetailType"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//3 claimDetailType
                        try
                        {
                            sLine = sLine + dr["LossCode"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//4 LossCode
                        try
                        {
                            sLine = sLine + dr["coveredFlagg"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//5 coveredFlagg
                        try
                        {
                            sLine = sLine + dr["requesedQuanity"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//6 requesedQuanity
                        try
                        {
                            sLine = sLine + dr["requestedUnitCost"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//7 requestedUnitCost
                        try
                        {
                            sLine = sLine + dr["requestedAmount"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//8 requestedAmount
                        try
                        {
                            sLine = sLine + dr["authorizedQuantity"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//9 authorizedQuantity
                        try
                        {
                            sLine = sLine + dr["authorizedCost"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//10 authorizedCost
                        try
                        {
                            sLine = sLine + dr["AuthorizedAmount"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//11 AuthorizedAmount
                        try
                        {
                            sLine = sLine + dr["salesTaxRate"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//12 salesTaxRate
                        try
                        {
                            sLine = sLine + dr["SalesTaxAmount"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//13 SalesTaxAmount
                        try
                        {
                            sLine = sLine + dr["salesTaxRate2"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//14 salesTaxRate2
                        try
                        {
                            sLine = sLine + dr["salesTaxAmount2"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//15 salesTaxAmount2
                        try
                        {
                            sLine = sLine + dr["claimDetailTotalAmount"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//16 claimDetailTotalAmount
                        try
                        {
                            sLine = sLine + dr["claimDetailStatusID"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//17 claimDetailStatusID
                        try
                        {
                            sLine = sLine + dr["claimDetailPayeeTypeCode"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//18 claimDetailPayeeTypeCode
                        try
                        {
                            sLine = sLine + dr["claimDefaultPayeeNumber"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//19 claimDefaultPayeeNumber
                        try
                        {
                            sLine = sLine + dr["claimDetailPayeeName"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//20 claimDetailPayeeName
                        try
                        {
                            sLine = sLine + dr["claimDetailPayeeAddr1"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//21 claimDetailPayeeAddr1
                        try
                        {
                            sLine = sLine + dr["claimDetailPayeeAddr2"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//22 claimDetailPayeeAddr2
                        try
                        {
                            sLine = sLine + dr["claimDetailPayeeCity"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//23 claimDetailPayeeCity
                        try
                        {
                            sLine = sLine + dr["claimDetailPayeeState"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//24 claimDetailPayeeState
                        try
                        {
                            sLine = sLine + dr["claimDetailPayeeZip"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//25 claimDetailPayeeZip
                        try
                        {
                            sLine = sLine + dr["claimDetailPaymentMethodCode"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//26 claimDetailPaymentMethodCode
                        try
                        {
                            sLine = sLine + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//27 claimDetailCheckPrintDate
                        try
                        {
                            sLine = sLine + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//28 ClaimDetailCheckNumber
                        try
                        {
                            sLine = sLine + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//29 ClaimPaymentReferenceNumber
                        try
                        {
                            sLine = sLine + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//30 ClaimDetailPaymentAmount
                        try
                        {
                            sLine = sLine + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//31 CustomerSatisfactionSurvayDate
                        try
                        {
                            sLine = sLine + dr["claimDetailCheckPrintDate"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//32 ClaimDetailPaidDate
                        try
                        {
                            sLine = sLine + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//33 PartSourceCode
                        try
                        {
                            sLine = sLine + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//34 TermMonths
                        try
                        {
                            sLine = sLine + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//35 TermMileage
                        streamWriterContract.WriteLine(sLine);
                    }
                }
                streamWriterContract.Close();
                iFirstOrSecondRun++;
            }
            
        }
        private void oldRepublicClaims()
        {
            int iFirstOrSecondRun = 0;

            while (iFirstOrSecondRun < 2)
            {
                string sLine = "";
                CSharpDBO.CSharpDBO dBO = new CSharpDBO.CSharpDBO();
                dBO.dboOpen(path);
                string difference = "";
                if (iFirstOrSecondRun == 0)
                {
                    difference = "_all_Other_states";
                }
                else
                {
                    difference = "_WA_FL,CAVSC_Ancillary_GAP";
                }
                string sFilePath = setPath() + "/MonthEnd/" + setMonth() + "/OldRep/ " + setYear() + "-" + setMonthNo() + "_Veritas_ClaimsOldRepublic" + difference + ".txt";
                StreamWriter streamWriterContract = new StreamWriter(sFilePath);

                string sSQL = "select distinct c.ContractNo, 'ORUS' as TPA, cl.claimno, sc.ServiceCenterNo, '' as RepairFacilityType, cl.LossDate, " +
                    "'' as repairOrderReceiptDate,  cl.LossMile as lossMile, cl.DatePaid as datePaid, 's' as ClaimDefaultPayeeTypeCode," +
                    "'' as repairOrderNumber, left(cl.Status, 1) as claimStatus,  '' as customerComplaint,'' as claimContact, cp.PayeeNo, cp.PayeeName, " +
                    "cp.Addr1 as payeeAddr1, cp.Addr2 as payeeAddr2, cp.City as payeeCity,  cp.State as payeeState, cp.Zip as payeeZip, cp.Phone as payeePhone, " +
                    "cp.Fax as payeeFax, cp.Contact as payeeContactName, 'CC' as defaulfClaimPaymentMethodCode, cl.LossCode,'' as VehicleManufacturerCode," +
                    "'' as contractHolderAlternativePhoneNumber, '' as claimCorrection, '' as causeOfLossGap from claim cl " +
                    "left join Contract c on c.ContractID = cl.ContractID " +
                    "left join ServiceCenter sc on sc.ServiceCenterID = cl.ServiceCenterID " +
                    "left join ClaimDetail cd on cd.ClaimID = cl.ClaimID " +
                    "left join ClaimPayee cp on cp.ClaimPayeeID = cd.ClaimPayeeID " +
                    "where c.InsCarrierID = 4 " +
                    "and cl.datePaid <= '" + dtpEndDate.Value + "' " +
                    "and cl.datePaid >= '" + dtpStartDate.Value + "' " +
                    "and c.INSCancelTransmissionDate is null " +
                    "and not c.DatePaid is null " +
                    "and cl.Status = 'Paid' " +
                    "and ratetypeid = 1 "
                    ;

                if (iFirstOrSecondRun == 0)
                {
                    sSQL = sSQL + "and not cl.contractid in (select ContractID from contract where InsCarrierID = 4 and state in ('WA', 'FL', 'CA') or(programid = 67)) ";
                    sSQL = sSQL + "and not cl.ClaimNo in (select ClaimNo from OldRepublicClaimAdd where AddOrRemove = 'R') " +
                    "or  cd.claimdetailid in (select claimdetailid from claimdetail where claimid in ( select claimid from claim where ClaimNo in (select ClaimNo from OldRepublicClaimAdd where AddOrRemove = 'A') and Exclude = 0 and RateTypeID = 1 and not contractid in (select ContractID from contract where InsCarrierID = 4 and state in ('WA', 'FL', 'CA') or(programid = 67)))) " +
                    "order by cl.claimno";
                }
                else
                {
                    sSQL = sSQL + "and cl.contractid in (select ContractID from contract where InsCarrierID = 4 and state in ('WA', 'FL', 'CA') or(programid = 67))";
                    sSQL = sSQL + "and not cl.ClaimNo in (select ClaimNo from OldRepublicClaimAdd where AddOrRemove = 'R') " +
                    "or cd.claimdetailid in (select claimdetailid from claimdetail where claimid in ( select claimid from claim where ClaimNo in (select ClaimNo from OldRepublicClaimAdd where AddOrRemove = 'A') and Exclude = 0 and RateTypeID = 1 and contractid in (select ContractID from contract where InsCarrierID = 4 and state in ('WA', 'FL', 'CA') or(programid = 67)))) " +
                    "order by cl.claimno";
                }
                

                lblProgressBar.Text = "Executing Querry";
                lblProgressBar.Refresh();
                System.Data.DataTable dataTable = dBO.dboInportInformation(sSQL);
                dBO.dboClose();
                int tracker = dataTable.Rows.Count;
                lblProgressBar.Text = "Processing Old Rep Claim";
                lblProgressBar.Refresh();
                lblClaim.Text = "Claims:" + tracker;
                lbCancel.Refresh();
                progressBar1.Value = 0;
                progressBar1.Maximum = tracker;
                progressBar1.Minimum = 0;
                progressBar1.Visible = true;
                progressBar1.Step = 1;
                if (dataTable.Rows.Count > 0)
                {
                    foreach (DataRow dr in dataTable.Rows)
                    {
                        progressBar1.PerformStep();
                        sLine = "";
                        try
                        {
                            sLine = sLine + dr["ContractNo"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//1 ContractNo
                        try
                        {
                            sLine = sLine + dr["TPA"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//2 TPA
                        try
                        {
                            sLine = sLine + dr["ClaimNo"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//3 ClaimNo
                        try
                        {
                            sLine = sLine + dr["ServiceCenterNo"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//4 ServiceCenterNo
                        try
                        {
                            sLine = sLine + dr["RepairFacilityType"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//5 RepairFacilityType
                        try
                        {
                            sLine = sLine + dr["LossDate"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//6 LossDate
                        try
                        {
                            sLine = sLine + dr["repairOrderReceiptDate"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//7 repairOrderReceiptDate
                        try
                        {
                            sLine = sLine + dr["LossMile"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//8 LossMile
                        try
                        {
                            sLine = sLine + dr["datePaid"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//9 datePaid
                        try
                        {
                            sLine = sLine + dr["ClaimDefaultPayeeTypeCode"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//10 ClaimDefaultPayeeTypeCode
                        try
                        {
                            sLine = sLine + dr["repairOrderNumber"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//11 repairOrderNumber
                        try
                        {
                            sLine = sLine + dr["claimStatus"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//12 claimStatus
                        try
                        {
                            sLine = sLine + dr["customerComplaint"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//13 customerComplaint
                        try
                        {
                            sLine = sLine + dr["claimContact"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//14 claimContact
                        try
                        {
                            sLine = sLine + dr["PayeeNo"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//15 PayeeNo
                        try
                        {
                            sLine = sLine + dr["PayeeName"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//16 PayeeName
                        try
                        {
                            sLine = sLine + dr["payeeAddr1"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//17 payeeAddr1
                        try
                        {
                            sLine = sLine + dr["payeeAddr2"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//18 payeeAddr2
                        try
                        {
                            sLine = sLine + dr["payeeCity"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//19 payeeCity
                        try
                        {
                            sLine = sLine + dr["payeeState"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//20 payeeState
                        try
                        {
                            sLine = sLine + dr["payeeZip"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//21 payeeZip
                        try
                        {
                            sLine = sLine + dr["payeePhone"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//22 payeePhone
                        try
                        {
                            sLine = sLine + dr["payeeFax"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//23 payeeFax
                        try
                        {
                            sLine = sLine + dr["payeeContactName"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//24 payeeContactName
                        try
                        {
                            sLine = sLine + dr["defaulfClaimPaymentMethodCode"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//25 defaulfClaimPaymentMethodCode
                        try
                        {
                            sLine = sLine + dr["LossCode"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//26 LossCode
                        try
                        {
                            sLine = sLine + dr["VehicleManufacturerCode"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//27 VehicleManufacturerCode
                        try
                        {
                            sLine = sLine + dr["contractHolderAlternativePhoneNumber"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//28 contractHolderAlternativePhoneNumber
                        try
                        {
                            sLine = sLine + dr["claimCorrection"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//29 claimCorrection
                        try
                        {
                            sLine = sLine + dr["causeOfLossGap"].ToString() + "\t";
                        }
                        catch
                        {
                            sLine = sLine + "\t";
                        }//30 causeOfLossGap
                        streamWriterContract.WriteLine(sLine);
                    }
                }
                streamWriterContract.Close();
                iFirstOrSecondRun++;
            }
            
        }
        private void oldRepublicSeller()
        {
            string sLine = "";
            CSharpDBO.CSharpDBO dBO = new CSharpDBO.CSharpDBO();
            dBO.dboOpen(path);
            StreamWriter streamWriterContract = new StreamWriter(setPath() + "/MonthEnd/" + setMonth() + "/OldRep/ " + setYear() + "-" + setMonthNo() + "_Veritas_SellerOldRepublic.txt");


            string sSQL = "select distinct 'ORUS' as TPA, 'VER551' as AgentNo,DealerNo, d.DealerName,d.Addr1,d.Addr2,d.City,d.State,d.Zip,d.Phone,'A' as status," +
                "'' as sellerFederalTaxId, '' as filler, " +
                "(case when d.DateSigned is null then '1/1/2017' else d.DateSigned end), " +
                "'' as filler, " +
                "'' as filler, '' as filler, '' as filler, '' as filler, '' as filler, '' as filler, '' as filler, '' as filler, " +
                "'' as filler, '' as filler, '' as filler, d.EMail as sellerEmail,'' as filler, '' as filler,'ACH' as sellarDefaultPaymentMethodCode, " +
                "'EN' as SellerPreferredLanguageCode, '' as filler, '' as faxNumber, '0' useCustomerState, '0' serviceCenterActiveFlag " +
                "from contract c " +
                "inner join Agents a on c.AgentsID = a.AgentID " +
                "inner join Dealer d on c.DealerID = d.DealerID" +
                " where c.InsCarrierID = 4";

            lblProgressBar.Text = "Executing Querry";
            lblProgressBar.Refresh();
            System.Data.DataTable dataTable = dBO.dboInportInformation(sSQL);
            dBO.dboClose();
            int tracker = dataTable.Rows.Count;
            lblProgressBar.Text = "Processing Old Rep Seller";
            lblProgressBar.Refresh();
            lblDealer.Text = "Dealer:" + tracker;
            lbCancel.Refresh();
            progressBar1.Value = 0;
            progressBar1.Maximum = tracker;
            progressBar1.Minimum = 0;
            progressBar1.Visible = true;
            progressBar1.Step = 1;
            if (dataTable.Rows.Count > 0)
            {
                foreach (DataRow dr in dataTable.Rows)
                {
                    progressBar1.PerformStep();
                    sLine = "";
                    try
                    {
                        sLine = sLine + dr["TPA"].ToString() + "\t";
                    }
                    catch
                    {
                        sLine = sLine + "\t";
                    }//1 TPA
                    try
                    {
                        sLine = sLine + dr["AgentNo"].ToString() + "\t";
                    }
                    catch
                    {
                        sLine = sLine + "\t";
                    }//2 AgentNo
                    try
                    {
                        sLine = sLine + dr["DealerNo"].ToString() + "\t";
                    }
                    catch
                    {
                        sLine = sLine + "\t";
                    }//3 SellerNumber
                    try
                    {
                        sLine = sLine + dr["DealerName"].ToString() + "\t";
                    }
                    catch
                    {
                        sLine = sLine + "\t";
                    }//4 AgentName
                    try
                    {
                        sLine = sLine + dr["Addr1"].ToString() + "\t";
                    }
                    catch
                    {
                        sLine = sLine + "\t";
                    }//5 Addr1
                    try
                    {
                        sLine = sLine + dr["Addr2"].ToString() + "\t";
                    }
                    catch
                    {
                        sLine = sLine + "\t";
                    }//6 Addr2
                    try
                    {
                        sLine = sLine + dr["City"].ToString() + "\t";
                    }
                    catch
                    {
                        sLine = sLine + "\t";
                    }//7 City
                    try
                    {
                        sLine = sLine + dr["State"].ToString() + "\t";
                    }
                    catch
                    {
                        sLine = sLine + "\t";
                    }//8 State
                    try
                    {
                        sLine = sLine + dr["Zip"].ToString() + "\t";
                    }
                    catch
                    {
                        sLine = sLine + "\t";
                    }//9 Zip
                    try
                    {
                        sLine = sLine + dr["Phone"].ToString() + "\t";
                    }
                    catch
                    {
                        sLine = sLine + "\t";
                    }//10 Phone
                    try
                    {
                        sLine = sLine + dr["status"].ToString() + "\t";
                    }
                    catch
                    {
                        sLine = sLine + "\t";
                    }//11 status
                    try
                    {
                        sLine = sLine + dr["sellerFederalTaxId"].ToString() + "\t";
                    }
                    catch
                    {
                        sLine = sLine + "\t";
                    }//12 sellerFederalTaxId
                    try
                    {
                        sLine = sLine + dr["claimContact"].ToString() + "\t";
                    }
                    catch
                    {
                        sLine = sLine + "\t";
                    }//13 filler
                    try
                    {
                        sLine = sLine + Convert.ToDateTime(dr["signupDate"]).ToString("MM/dd/yyyy") + "\t";
                    }
                    catch
                    {
                        sLine = sLine + "\t";
                    }//14 signupDate
                    try
                    {
                        sLine = sLine + dr["sellerInsuredState"].ToString() + "\t";
                    }
                    catch
                    {
                        sLine = sLine + "\t";
                    }//15 sellerInsuredState
                    try
                    {
                        sLine = sLine + dr["filler"].ToString() + "\t";
                    }
                    catch
                    {
                        sLine = sLine + "\t";
                    }//16 filler
                    try
                    {
                        sLine = sLine + dr["filler"].ToString() + "\t";
                    }
                    catch
                    {
                        sLine = sLine + "\t";
                    }//17 filler
                    try
                    {
                        sLine = sLine + dr["filler"].ToString() + "\t";
                    }
                    catch
                    {
                        sLine = sLine + "\t";
                    }//18 filler
                    try
                    {
                        sLine = sLine + dr["filler"].ToString() + "\t";
                    }
                    catch
                    {
                        sLine = sLine + "\t";
                    }//19 filler
                    try
                    {
                        sLine = sLine + dr["filler"].ToString() + "\t";
                    }
                    catch
                    {
                        sLine = sLine + "\t";
                    }//20 filler
                    try
                    {
                        sLine = sLine + dr["filler"].ToString() + "\t";
                    }
                    catch
                    {
                        sLine = sLine + "\t";
                    }//21 filler
                    try
                    {
                        sLine = sLine + dr["filler"].ToString() + "\t";
                    }
                    catch
                    {
                        sLine = sLine + "\t";
                    }//22 filler
                    try
                    {
                        sLine = sLine + dr["filler"].ToString() + "\t";
                    }
                    catch
                    {
                        sLine = sLine + "\t";
                    }//23 filler
                    try
                    {
                        sLine = sLine + dr["filler"].ToString() + "\t";
                    }
                    catch
                    {
                        sLine = sLine + "\t";
                    }//24 filler
                    try
                    {
                        sLine = sLine + dr["filler"].ToString() + "\t";
                    }
                    catch
                    {
                        sLine = sLine + "\t";
                    }//25 filler
                    try
                    {
                        sLine = sLine + dr["filler"].ToString() + "\t";
                    }
                    catch
                    {
                        sLine = sLine + "\t";
                    }//26 filler
                    try
                    {
                        sLine = sLine + dr["filler"].ToString() + "\t";
                    }
                    catch
                    {
                        sLine = sLine + "\t";
                    }//27 filler
                    try
                    {
                        sLine = sLine + dr["sellerEmail"].ToString() + "\t";
                    }
                    catch
                    {
                        sLine = sLine + "\t";
                    }//28 sellerEmail
                    try
                    {
                        sLine = sLine + dr["filler"].ToString() + "\t";
                    }
                    catch
                    {
                        sLine = sLine + "\t";
                    }//29 filler
                    try
                    {
                        sLine = sLine + dr["filler"].ToString() + "\t";
                    }
                    catch
                    {
                        sLine = sLine + "\t";
                    }//30 filler
                    try
                    {
                        sLine = sLine + dr["sellarDefaultPaymentMethodCode"].ToString() + "\t";
                    }
                    catch
                    {
                        sLine = sLine + "\t";
                    }//31 sellarDefaultPaymentMethodCode
                    try
                    {
                        sLine = sLine + dr["SellerPreferredLanguageCode"].ToString() + "\t";
                    }
                    catch
                    {
                        sLine = sLine + "\t";
                    }//32 SellerPreferredLanguageCode
                    try
                    {
                        sLine = sLine + dr["filler"].ToString() + "\t";
                    }
                    catch
                    {
                        sLine = sLine + "\t";
                    }//33 filler
                    try
                    {
                        sLine = sLine + dr["faxNumber"].ToString() + "\t";
                    }
                    catch
                    {
                        sLine = sLine + "\t";
                    }//34 faxNumber
                    try
                    {
                        sLine = sLine + dr["useCustomerState"].ToString() + "\t";
                    }
                    catch
                    {
                        sLine = sLine + "\t";
                    }//35 useCustomerState
                    try
                    {
                        sLine = sLine + dr["serviceCenterActiveFlag"].ToString() + "\t";
                    }
                    catch
                    {
                        sLine = sLine + "\t";
                    }//36 serviceCenterActiveFlag


                    streamWriterContract.WriteLine(sLine);
                }
            }
            streamWriterContract.Close();
        }
            //Fortega
        private void fortegaContract()
        {
            string sLine = "";
            StreamWriter streamWriterContract = new StreamWriter(setPath() + "/MonthEnd/" + setMonth() + "/Fortega/ " + setYear() + "-" + setMonthNo() + "_Veritas_ContractFortega.txt");
            CSharpDBO.CSharpDBO dBO = new CSharpDBO.CSharpDBO();
            dBO.dboOpen(path);
            string sSQL = "select 'AKMC' as TPA, 'VGPS' as agentNo, d.DealerNo, c.ContractNo,c.FormNo as contractFormNum, '' as batchNo, '' as RegisterNum,'' as filler, '' as contractHolderNameTitle, " +
                "c.FName as firstName, c.LName as lastName, '' as middleName, '' as spouse,  c.Addr1 + ' ' + case when c.Addr2 is null then '' else c.Addr2 end as address,  c.City, c.State, " +
                "c.Zip,'' as phone,'' as workNum,'' as contractHolderExtension1, '' as extension2, '' as contractHldrEmail, '' as filler, c.SaleMile as saleOdometer, '' as filler,'' as filler," +
                "'' as filler,'' as filler, c.Year as vehicleYr, c.VIN as vin, '' as filler,'' as mfgWarrantyTerm, '' as mfgMileage, c.SaleDate,'' as vehiclePurchasePrice, '' as filler,'' as filler," +
                "'' as filler,'' as VehicleAutoCode, '' as leinHolderNum, '' as reinsuranceId, '' as filler,'' as filler,'' as filler,'' as filler,'' as filler, '' as VehInSecDt, '' as filler,'' as filler," +
                "'' as filler, '' as memberId, '' as NCB, '' as contractEntryDate, '' as filler, c.PlanTypeID as planCode, c.RateBookID, NewUsed as NewUsed, dc.DeductAmt as deductibleAmount, " +
                "'1' as deductibleType, c.TermMonth as contractTermMos, c.TermMile as contractTermMileage, '' as filler,'' as filler,'' as filler,'' as filler,'' as filler,'' as filler,'' as filler," +
                "'' as filler,'' as filler,'' as filler,'' as filler,'' as filler,'' as filler,'' as filler,'' as filler,'' as filler, '' as filler,'' as filler,'' as filler,'' as filler,'' as filler," +
                "'' as filler,'' as filler,'' as filler,'EN' as language,'' as filler,'' as filler,'' as filler,'' as filler,'' as filler,'' as filler,'' as filler, '' as filler,'' as filler,'' as filler," +
                "'' as filler,'' as filler,'' as filler,'' as filler,'' as filler,'' as filler,'' as filler,'' as filler,'' as filler,'' as filler,'' as filler,'' as filler,'' as filler, '' as filler," +
                "'' as filler,'' as filler,'' as filler,'' as filler,'' as filler,'' as filler,'' as filler,'' as filler,'' as filler,'' as filler,'' as filler,'' as filler,'' as filler,'' as filler," +
                "'' as filler,'' as filler, '' as retaileRate, '' as filler,'' as filler, c.LIenHolder as sLienName, '' as slienAdrs, '' as slienAdrs2, '' as slienCity, '' as slienState, '' as slienZip," +
                "'' as slienPhone, '' as slienContract, '' as slienFedTaxId, '' as scontractEntryAppName, 'SGPY' as PymtOption, '' as DealType, '' as FinancedAmount, '' as APR, '' as Term, '' as monthlyPayment, " +
                "'' as FirstPaymentDate, '' as BalloonAmount, '' as residualAmount, '' as MSRP, '' as baseACV, '' as NADAValue, '' as AccountNumber " +
                "from contract c " +
                "Left join agents a on a.AgentID = c.AgentsID " +
                "Left join Dealer d on d.dealerid = c.dealerid " +
                "left join Deductible dc on dc.DeductID = c.DeductID " +
                "where InsCarrierID = 5 " +
                "and (c.status = 'Paid' or c.Status = 'Expired' or c.status = 'Cancelled') " +
                "and c.datePaid <= '" + dtpEndDate.Value + "' " +
                "and c.datePaid >= '" + dtpStartDate.Value + "' " +
                "and inssaletransmissiondate is null " +
                "and not c.DatePaid is null " +
                "and not c.contractno in (select contractno from fortegaContractAdd where AddOrRemove = 'R')" +
                "or c.contractno in (select contractNo from fortegaContractAdd where AddOrRemove = 'A' and Status = 'Paid') ";
            System.Data.DataTable dt = dBO.dboInportInformation(sSQL);
            dBO.dboClose();
            int tracker = dt.Rows.Count;
            lblProgressBar.Text = "Processing Fortega Seller";
            lblProgressBar.Refresh();
            lblContract.Text = "Paid Contract:" + tracker;
            lbCancel.Refresh();
            progressBar1.Value = 0;
            progressBar1.Maximum = tracker;
            progressBar1.Minimum = 0;
            progressBar1.Visible = true;
            progressBar1.Step = 1;
            foreach (DataRow dr in dt.Rows)
            {
                progressBar1.PerformStep();
                sLine = "";
                try
                {
                    sLine = sLine + dr["TPA"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//1 TPA
                try
                {
                    sLine = sLine +"VER"+dr["agentNo"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//2 agentNo
                try
                {
                    sLine = sLine + "VER" + dr["DealerNo"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//3 DealerNo
                try
                {
                    sLine = sLine + "VER" + dr["ContractNo"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//4 ContractNo
                try
                {
                    sLine = sLine + dr["contractFormNum"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//5 contractFormNum
                try
                {
                    sLine = sLine + dr["batchNo"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//6 batchNo
                try
                {
                    sLine = sLine + dr["RegisterNum"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//7 RegisterNum
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//8 filler
                try
                {
                    sLine = sLine + dr["contractHolderNameTitle"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//9 contractHolderNameTitle
                try
                {
                    sLine = sLine + dr["firstName"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//10 firstName
                try
                {
                    sLine = sLine + dr["lastName"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//11 lastName
                try
                {
                    sLine = sLine + dr["middleName"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//12 middleName
                try
                {
                    sLine = sLine + dr["spouse"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//13 spouse
                try
                {
                    sLine = sLine + dr["address"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//14 address
                try
                {
                    sLine = sLine + dr["City"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//15 City
                try
                {
                    sLine = sLine + dr["State"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//16 State
                try
                {
                    sLine = sLine + dr["Zip"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//17 Zip
                try
                {
                    sLine = sLine + dr["phone"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//18 phone
                try
                {
                    sLine = sLine + dr["workNum"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//19 workNum
                try
                {
                    sLine = sLine + dr["contractHolderExtension1"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//20 contractHolderExtension1
                try
                {
                    sLine = sLine + dr["extension2"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//21 extension2
                try
                {
                    sLine = sLine + dr["contractHldrEmail"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//22 contractHldrEmail
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//23 filler
                try
                {
                    sLine = sLine + dr["saleOdometer"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//24 saleOdometer
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//25 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//26 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//27 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//28 filler
                try
                {
                    sLine = sLine + dr["vehicleYr"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//29 vehicleYr
                try
                {
                    sLine = sLine + dr["vin"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//30 vin
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//31 filler
                try
                {
                    sLine = sLine + dr["mfgWarrantyTerm"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//32 mfgWarrantyTerm
                try
                {
                    sLine = sLine + dr["mfgMileage"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//33 mfgMileage
                try
                {
                    sLine = sLine + Convert.ToDateTime(dr["SaleDate"]).ToString("MM/dd/yyyy") + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//34 SaleDate
                try
                {
                    sLine = sLine + dr["vehiclePurchasePrice"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//35 vehiclePurchasePrice
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//36 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//37 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//38 filler
                try
                {
                    sLine = sLine + dr["VehicleAutoCode"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//39 VehicleAutoCode
                try
                {
                    sLine = sLine + dr["leinHolderNum"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//40 leinHolderNum
                try
                {
                    sLine = sLine + dr["reinsuranceId"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//41 reinsuranceId
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//42 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//43 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//44 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//45 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//46 filler
                try
                {
                    sLine = sLine + dr["VehInSecDt"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//47 VehInSecDt
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//48 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//49 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//50 filler
                try
                {
                    sLine = sLine + dr["memberId"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//51 memberId
                try
                {
                    sLine = sLine + dr["NCB"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//52 NCB
                try
                {
                    sLine = sLine + dr["contractEntryDate"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//53 contractEntryDate
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//54 filler
                try
                {
                    sLine = sLine + dr["planCode"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//55 planCode
                try
                {
                    sLine = sLine + dr["RateBookID"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//56 RateBookID
                try
                {
                    sLine = sLine + dr["NewUsed"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//57 NewUsed
                try
                {
                    sLine = sLine + dr["deductibleAmount"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//58 deductibleAmount
                try
                {
                    sLine = sLine + dr["deductibleType"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//59 deductibleType
                try
                {
                    sLine = sLine + dr["contractTermMos"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//60 contractTermMos
                try
                {
                    sLine = sLine + dr["contractTermMileage"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//61 contractTermMileage
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//62 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//63 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//64 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//65 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//66 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//67 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//68 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//69 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//70 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//71 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//72 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//73 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//74 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//75 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//76 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//77 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//78 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//79 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//80 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//81 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//82 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//83 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//84 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//85 filler
                try
                {
                    sLine = sLine + dr["language"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//86 language
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//87 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//88 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//89 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//90 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//91 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//92 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//93 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//94 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//95 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//96 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//97 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//98 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//99 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//100 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//101 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//102 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//103 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//104 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//105 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//106 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//107 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//108 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//109 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//110 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//111 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//112 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//113 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//114 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//115 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//116 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//117 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//118 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//119 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//120 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//121 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//122 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//123 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//124 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//125 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//126 filler
                try
                {
                    sLine = sLine + dr["retaileRate"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//127 retaileRate
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//128 filler
                try
                {
                    sLine = sLine + dr["filler"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//129 filler
                try
                {
                    sLine = sLine + dr["sLienName"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//130 sLienName
                try
                {
                    sLine = sLine + dr["slienAdrs"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//131 slienAdrs
                try
                {
                    sLine = sLine + dr["slienAdrs2"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//132 slienAdrs2
                try
                {
                    sLine = sLine + dr["slienCity"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//133 slienCity
                try
                {
                    sLine = sLine + dr["slienState"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//134 slienState
                try
                {
                    sLine = sLine + dr["slienZip"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//135 slienZip
                try
                {
                    sLine = sLine + dr["slienPhone"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//136 slienPhone
                try
                {
                    sLine = sLine + dr["slienContract"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//137 slienContract
                try
                {
                    sLine = sLine + dr["slienFedTaxId"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//138 slienFedTaxId
                try
                {
                    sLine = sLine + dr["scontractEntryAppName"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//139 scontractEntryAppName
                try
                {
                    sLine = sLine + dr["PymtOption"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//140 PymtOption
                try
                {
                    sLine = sLine + dr["DealType"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//141 DealType
                try
                {
                    sLine = sLine + dr["FinancedAmount"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//142 FinancedAmount
                try
                {
                    sLine = sLine + dr["APR"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//143 APR
                try
                {
                    sLine = sLine + dr["Term"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//144 Term
                try
                {
                    sLine = sLine + dr["monthlyPayment"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//145 monthlyPayment
                try
                {
                    sLine = sLine + dr["FirstPaymentDate"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//146 FirstPaymentDate
                try
                {
                    sLine = sLine + dr["BalloonAmount"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//147 BalloonAmount
                try
                {
                    sLine = sLine + dr["residualAmount"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//148 residualAmount
                try
                {
                    sLine = sLine + dr["MSRP"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//149 MSRP
                try
                {
                    sLine = sLine + dr["baseACV"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//150 baseACV
                try
                {
                    sLine = sLine + dr["NADAValue"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//151 NADAValue
                try
                {
                    sLine = sLine + dr["AccountNumber"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//152 AccountNumber
                streamWriterContract.WriteLine(sLine);
            }
            streamWriterContract.Close();
        }
        private void fortegaCancel()
        {
            string sLine = "";
            StreamWriter streamWriterContract = new StreamWriter(setPath() + "/MonthEnd/" + setMonth() + "/Fortega/ " + setYear() + "-" + setMonthNo() + "_Veritas_CancelFortega.txt");
            CSharpDBO.CSharpDBO dBO = new CSharpDBO.CSharpDBO();
            dBO.dboOpen(path);
            string sSQL = "select 'AKMC' as TPA, c.ContractNo as contractNo, " +
                "cc.CancelMile as cancelOdometer,cc.CancelEffDate as effectiveDate, " +
                "cc.CancelDate as receivedDate, " +
                "case when CancelFactor = 1 then 'FA' else 'PB' end as methodCode, " +
                "'A' as reasonCode, 'N' as claclBaseCode, round(sum(ca.CancelAmt),2 )as RefundAmount," +
                "'' as cancelNote, '1' as cancelPayeeType,'' as payee5Name, '' as payee5Add1, '' as payee5Add2, " +
                "'' as payee5City,'' as payee5state,'' as payee5Zip,'CHK' as PayMethod " +
                "from Contract c " +
                "inner join contractcancel cc on cc.ContractID = c.contractid " +
                "inner join ContractAmt ca on ca.ContractID = c.ContractID " +
                "where InsCarrierID = 5 " +
                "and (c.status = 'Cancelled') " +
                "and cc.canceldate <= '" + dtpEndDate.Value + "' " +
                "and cc.canceldate >= '" + dtpStartDate.Value + "' " +
                "and c.INSCancelTransmissionDate is null " +
                "and (c.ProgramID < 47 or c.ProgramID > 60) " +
                "and c.contractid in (select contractid from contractamt where ratetypeid = 1)" +
                "and not c.DatePaid is null " +
                "and not c.contractno in (select contractno from fortegaContractAdd where AddOrRemove = 'R')" +
                "or c.contractno in (select contractno from fortegaContractAdd where AddOrRemove = 'A' and Status = 'Cancelled') " +
                "group by c.ContractNo, cc.CancelMile,cc.CancelEffDate,cc.RequestDate, cc.CancelDate, cc.CancelFactor ";
            System.Data.DataTable dt = dBO.dboInportInformation(sSQL);
            dBO.dboClose();
            int tracker = dt.Rows.Count;
            lblProgressBar.Text = "Processing Fortega Cancels";
            lblProgressBar.Refresh();
            lbCancel.Text = "Cancel Contracts:" + tracker;
            lbCancel.Refresh();
            progressBar1.Value = 0;
            progressBar1.Maximum = tracker;
            progressBar1.Minimum = 0;
            progressBar1.Visible = true;
            progressBar1.Step = 1;
            foreach (DataRow dr in dt.Rows)
            {
                progressBar1.PerformStep();
                sLine = "";
                try
                {
                    sLine = sLine + dr["TPA"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//1 TPA
                try
                {
                    sLine = sLine + "VER" + dr["contractNo"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//2 contractNo
                try
                {
                    sLine = sLine + dr["cancelOdometer"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//3 cancelOdometer
                try
                {
                    sLine = sLine + dr["effectiveDate"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//4 effectiveDate
                try
                {
                    sLine = sLine + dr["receivedDate"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//5 receivedDate
                try
                {
                    sLine = sLine + dr["methodCode"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//6 methodCode
                try
                {
                    sLine = sLine + dr["reasonCode"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//7 reasonCode
                try
                {
                    sLine = sLine + dr["claclBaseCode"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//8 claclBaseCode
                try
                {


                    sLine = sLine + dr["RefundAmount"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//9 RefundAmount
                try
                {
                    sLine = sLine + dr["cancelNote"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//10 cancelNote
                try
                {
                    sLine = sLine + dr["cancelPayeeType"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//11 cancelPayeeType
                try
                {
                    sLine = sLine + dr["payee5Name"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//12 payee5Name
                try
                {
                    sLine = sLine + dr["payee5Add1"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//13 payee5Add1
                try
                {
                    sLine = sLine + dr["payee5Add2"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//14 payee5Add2
                try
                {
                    sLine = sLine + dr["payee5City"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//15 payee5City
                try
                {
                    sLine = sLine + dr["payee5state"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//16 payee5state
                try
                {
                    sLine = sLine + dr["payee5Zip"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//17 payee5Zip
                try
                {
                    sLine = sLine + dr["PayMethod"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//18 PayMethod
                streamWriterContract.WriteLine(sLine);
            }
            streamWriterContract.Close();
        }
        private void fortegaClaimDetail()
        {
            string sLine = "";
            StreamWriter streamWriterContract = new StreamWriter(setPath() + "/MonthEnd/" + setMonth() + "/Fortega/ " + setYear() + "-" + setMonthNo() + "_Veritas_ClaimDetailFortega.txt");
            CSharpDBO.CSharpDBO dBO = new CSharpDBO.CSharpDBO();
            dBO.dboOpen(path);
            string sSQL = "select distinct ClaimDetailID, 'AKMC' as TPA, cl.ClaimNo as claimNo, SUBSTRING(ClaimDetailType, 1, 1) as ClaimDetailType, cd.LossCode as claimLossCode, " +
                "'' as CoveredFlag, ReqQty as requestedQuantity, ReqCost as requestedItemUnitCost, ReqAmt as requestedTotalAmount, AuthQty as AuthorizedQuantity, " +
                "AuthCost as AuthorizedItemUnitCost, AuthAmt as AuthorizedTotalAmount, '' as SalesTazRate1, '' as salesTaxAmount1, '' as salesTaxRate2, '' as salesTaxAmount2, " +
                "cd.TotalAmt as claimDetailTotalAmount,  " +
                "case when cd.ClaimDetailStatus = 'Approved' then 1 " +
                "when cd.ClaimDetailStatus = 'Authorized' then 2 " +
                "when cd.ClaimDetailStatus = 'Denied' then 3 " +
                "when cd.ClaimDetailStatus = 'Paid' then 4 " +
                "when cd.ClaimDetailStatus = 'Requested' then 5 end as claimdetailStatusID, " +
                "'S' as ClaimDtailPayeeTypeCode, cp.PayeeNo as claimDefaultPayeeNumber, cp.PayeeName as claimDetailPayeeName, cp.Addr1 as claimDetailPayeeAddress1, cp.Addr2 as claimDetailPayeeAdress2, " +
                "cp.City as claimDetailPayeeCity, cp.State as claimDetailPayeeState, cp.Zip as claimDetailPayeePostalCode, 'IMP' as claimDetailPaymentMethodCode, cd.DateApprove, '0' as claimDetailCheckNumber," +
                "  '' as claimPaymentReferenceNumber,  case when cd.ClaimDetailStatus = 'Paid' then cd.PaidAmt else '' end as claimDetailPaymentAmount, '' as customerSatisfactionSurvayDate,  " +
                "case when cd.ClaimDetailStatus = 'Paid' then cd.DatePaid else '' end as claimDetailPaidDate, " +
                "'' as partSourceCode, '' as termMonths, '' as termMilage " +
                "from ClaimDetail cd " +
                "inner join claim cl on cl.ClaimID = cd.ClaimID " +
                "inner join contract c on c.ContractID = cl.ContractID " +
                "inner join ClaimPayee cp on cd.ClaimPayeeID = cp.ClaimPayeeID " +
                "where c.InsCarrierID = 5 " +
                "and cl.datePaid <= '" + dtpEndDate.Value + "' " +
                "and cl.datePaid >= '" + dtpStartDate.Value + "' " +
                "and c.INSCancelTransmissionDate is null " +
                "and not c.DatePaid is null " +
                "and cd.ratetypeid = 1 " +
                "and cd.ClaimDetailStatus = 'Paid'" +
                //"and not cl.claimid in (select distinct claimid from claimdetail where not ClaimDetailStatus in ('Authorized','Requested','transmitted')) " +
                "and not c.contractno in (select ClaimNo from fortegaClaimAdd where AddOrRemove = 'R') " +
                "or c.contractno in (select ClaimNo from fortegaClaimAdd where AddOrRemove = 'A' ) " +
                "order by cl.claimNo";
            System.Data.DataTable dt = dBO.dboInportInformation(sSQL);
            dBO.dboClose();
            int tracker = dt.Rows.Count;
            lblProgressBar.Text = "Processing Fortega Claim Details";
            lblProgressBar.Refresh();
            lblOther.Text = "Other:" + tracker;
            lbCancel.Refresh();
            progressBar1.Value = 0;
            progressBar1.Maximum = tracker;
            progressBar1.Minimum = 0;
            progressBar1.Visible = true;
            progressBar1.Step = 1;
            foreach (DataRow dr in dt.Rows)
            {
                progressBar1.PerformStep();
                sLine = "";
                try
                {
                    sLine = sLine + dr["TPA"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//1 TPA
                try
                {
                    sLine = sLine + "VER" + dr["claimNo"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//2 claimNo
                try
                {
                    sLine = sLine + dr["ClaimDetailType"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//3 ClaimDetailType
                try
                {
                    sLine = sLine + dr["claimLossCode"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//4 claimLossCode
                try
                {
                    sLine = sLine + dr["CoveredFlag"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//5 CoveredFlag
                try
                {
                    sLine = sLine + dr["requestedQuantity"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//6 requestedQuantity
                try
                {
                    sLine = sLine + dr["requestedItemUnitCost"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//7 requestedItemUnitCost
                try
                {
                    sLine = sLine + dr["requestedTotalAmount"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//8 requestedTotalAmount
                try
                {
                    sLine = sLine + dr["AuthorizedQuantity"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//9 AuthorizedQuantity
                try
                {
                    sLine = sLine + dr["AuthorizedItemUnitCost"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//10 AuthorizedItemUnitCost
                try
                {
                    sLine = sLine + dr["AuthorizedTotalAmount"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//11 AuthorizedTotalAmount
                try
                {
                    sLine = sLine + dr["SalesTazRate1"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//12 SalesTazRate1
                try
                {
                    sLine = sLine + dr["salesTaxAmount1"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//13 salesTaxAmount1
                try
                {
                    sLine = sLine + dr["salesTaxRate2"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//14 salesTaxRate2
                try
                {
                    sLine = sLine + dr["salesTaxAmount2"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//15 salesTaxAmount2
                try
                {
                    sLine = sLine + dr["claimDetailTotalAmount"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//16 claimDetailTotalAmount
                try
                {
                    sLine = sLine + dr["claimdetailStatusID"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//17 claimdetailStatusID
                try
                {
                    sLine = sLine + dr["ClaimDtailPayeeTypeCode"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//18 ClaimDtailPayeeTypeCode
                try
                {
                    sLine = sLine + dr["claimDefaultPayeeNumber"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//19 claimDefaultPayeeNumber
                try
                {
                    sLine = sLine + dr["claimDetailPayeeName"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//20 claimDetailPayeeName
                try
                {
                    sLine = sLine + dr["claimDetailPayeeAddress1"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//21 claimDetailPayeeAddress1
                try
                {
                    sLine = sLine + dr["claimDetailPayeeAdress2"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//22 claimDetailPayeeAdress2
                try
                {
                    sLine = sLine + dr["claimDetailPayeeCity"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//23 claimDetailPayeeCity
                try
                {
                    sLine = sLine + dr["claimDetailPayeeState"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//24 claimDetailPayeeState
                try
                {
                    sLine = sLine + dr["claimDetailPayeePostalCode"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//25 claimDetailPayeePostalCode
                try
                {
                    sLine = sLine + dr["claimDetailPaymentMethodCode"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//26 claimDetailPaymentMethodCode
                try
                {
                    sLine = sLine + dr["DateApprove"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//27 DateApprove
                try
                {
                    sLine = sLine + dr["claimDetailCheckNumber"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//28 claimDetailCheckNumber
                try
                {
                    sLine = sLine + dr["claimPaymentReferenceNumber"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//29 claimPaymentReferenceNumber
                try
                {
                    sLine = sLine + dr["claimDetailPaymentAmount"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//30 claimDetailPaymentAmount
                try
                {
                    sLine = sLine + dr["customerSatisfactionSurvayDate"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//31 customerSatisfactionSurvayDate
                try
                {
                    sLine = sLine + dr["claimDetailPaidDate"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//32 claimDetailPaidDate
                try
                {
                    sLine = sLine + dr["partSourceCode"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//33 partSourceCode
                try
                {
                    sLine = sLine + dr["termMonths"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//34 termMonths
                try
                {
                    sLine = sLine + dr["termMilage"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//35 termMilage
                streamWriterContract.WriteLine(sLine);
            }
            streamWriterContract.Close();
        }
        private void fortegaClaimData()
        {
            string sLine = "";
            StreamWriter streamWriterContract = new StreamWriter(setPath() + "/MonthEnd/" + setMonth() + "/Fortega/ " + setYear() + "-" + setMonthNo() + "_Veritas_ClaimDataFortega.txt");
            CSharpDBO.CSharpDBO dBO = new CSharpDBO.CSharpDBO();
            dBO.dboOpen(path);
            string sSQL = "select distinct c.ContractNo as contractNo,'AKMC' as TPA, cl.ClaimNo as claimNo,sc.ServiceCenterNo," +
                "'' as repairFacilityType,cl.LossDate as claimLossDate, '' as repairOrderRecepit, LossMile as lossOdometer, cl.DatePaid as claimPaidDate, " +
                "'S' as claimDefaultPayeeTypeCode,cl.ronumber as repairOrderNumber ,SUBSTRING(cl.Status, 1, 1) as claimStatus, '' as customerComplaint, '' as claimContact, cp.PayeeNo as claimDefaultPayeeNumber, " +
                "cp.PayeeName as claimDefaultPayeeName, cp.Addr1 as claimDefaultPayeeAdress1, cp.Addr2 as claimDefaultPayeeAdress2, cp.City as claimDefaultPayeeCity, " +
                "cp.State as claimDefaultPayeeState, cp.Zip as claimDefaultPayeePostalCode,'' as claimDefaultPayeePhoneNumber, '' as ClaimDefaultPayeeFaxNumber, " +
                "'' as claimDefaultPayeeContractName, 'IMP' as defaultClaimPaymentMethodCode, '' as lossCause, '' as vehicleManufacturerCode, '' as contractHolderAlternativePhoneNumber, " +
                "'' as claimCorrection, '' as causeOfLossGAP " +
                "from claim cl " +
                "inner join contract c on c.ContractID = cl.ContractID " +
                "inner join ServiceCenter sc on sc.ServiceCenterID = cl.ServiceCenterID " +
                "inner join ClaimDetail cd on cd.claimid = cl.claimid " +
                "inner join ClaimPayee cp on cp.ClaimPayeeID = cd.claimpayeeID " +
                "where c.InsCarrierID = 5 " +
                "and cl.datePaid <= '" + dtpEndDate.Value + "' " +
                "and cl.datePaid >= '" + dtpStartDate.Value + "' " +
                "and c.INSCancelTransmissionDate is null " +
                "and not c.DatePaid is null " +
                "and cd.ratetypeid = 1 " +                
                "and cd.ClaimDetailStatus = 'Paid'" +
                //"and not cl.claimid in (select distinct claimid from claimdetail where not ClaimDetailStatus in ('Authorized','Requested','transmitted')) " +
                "and not c.contractno in (select ClaimNo from fortegaClaimAdd where AddOrRemove = 'R') " +
                "or c.contractno in (select ClaimNo from fortegaClaimAdd where AddOrRemove = 'A' ) " +
                "order by cl.claimNo ";
            System.Data.DataTable dt = dBO.dboInportInformation(sSQL);
            dBO.dboClose();
            int tracker = dt.Rows.Count;
            lblProgressBar.Text = "Processing Fortega Claim Data";
            lblProgressBar.Refresh();
            lblClaim.Text = "Claims:" + tracker;
            lbCancel.Refresh();
            progressBar1.Value = 0;
            progressBar1.Maximum = tracker;
            progressBar1.Minimum = 0;
            progressBar1.Visible = true;
            progressBar1.Step = 1;
            foreach (DataRow dr in dt.Rows)
            {
                progressBar1.PerformStep();
                sLine = "";
                try
                {
                    sLine = sLine + "VER" + dr["contractNo"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//1 contractNo
                try
                {
                    sLine = sLine + dr["TPA"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//2 TPA                
                try
                {
                    sLine = sLine + "VER" + dr["claimNo"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//3 claimNo
                try
                {
                    sLine = sLine + dr["ServiceCenterNo"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//4 ServiceCenterNo
                try
                {
                    sLine = sLine + dr["repairFacilityType"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//5 repairFacilityType
                try
                {
                    sLine = sLine + dr["claimLossDate"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//6 claimLossDate
                try
                {
                    sLine = sLine + dr["repairOrderRecepit"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//7 repairOrderRecepit
                try
                {
                    sLine = sLine + dr["lossOdometer"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//8 lossOdometer
                try
                {
                    sLine = sLine + dr["claimPaidDate"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//9 claimPaidDate
                try
                {
                    sLine = sLine + dr["claimDefaultPayeeTypeCode"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//10 claimDefaultPayeeTypeCode
                try
                {
                    sLine = sLine + dr["repairOrderNumber"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//11 repairOrderNumber
                try
                {
                    sLine = sLine + dr["claimStatus"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//12 claimStatus
                try
                {
                    sLine = sLine + dr["customerComplaint"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//13 customerComplaint
                try
                {
                    sLine = sLine + dr["claimContact"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//14 claimContact
                try
                {
                    sLine = sLine + dr["claimDefaultPayeeNumber"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//15 claimDefaultPayeeNumber
                try
                {
                    sLine = sLine + dr["claimDefaultPayeeName"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//16 claimDefaultPayeeName
                try
                {
                    sLine = sLine + dr["claimDefaultPayeeAdress1"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//17 claimDefaultPayeeAdress1
                try
                {
                    sLine = sLine + dr["claimDefaultPayeeAdress2"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//18 claimDefaultPayeeAdress2
                try
                {
                    sLine = sLine + dr["claimDefaultPayeeCity"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//19 claimDefaultPayeeCity
                try
                {
                    sLine = sLine + dr["claimDefaultPayeeState"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//20 claimDefaultPayeeState
                try
                {
                    sLine = sLine + dr["claimDefaultPayeePostalCode"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//21 claimDefaultPayeePostalCode
                try
                {
                    sLine = sLine + dr["claimDefaultPayeePhoneNumber"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//22 claimDefaultPayeePhoneNumber
                try
                {
                    sLine = sLine + dr["ClaimDefaultPayeeFaxNumber"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//23 ClaimDefaultPayeeFaxNumber
                try
                {
                    sLine = sLine + dr["claimDefaultPayeeContractName"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//24 claimDefaultPayeeContractName
                try
                {
                    sLine = sLine + dr["defaultClaimPaymentMethodCode"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//25 defaultClaimPaymentMethodCode
                try
                {
                    sLine = sLine + dr["lossCause"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//26 lossCause
                try
                {
                    sLine = sLine + dr["vehicleManufacturerCode"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//27 vehicleManufacturerCode
                try
                {
                    sLine = sLine + dr["contractHolderAlternativePhoneNumber"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//28 contractHolderAlternativePhoneNumber
                try
                {
                    sLine = sLine + dr["claimCorrection"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//29 claimCorrection
                try
                {
                    sLine = sLine + dr["causeOfLossGAP"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//30 causeOfLossGAP
                streamWriterContract.WriteLine(sLine);
            }
            streamWriterContract.Close();
        }
        private void fortegaSeller()
        {
            string sLine = "";
            StreamWriter streamWriterContract = new StreamWriter(setPath() + "/MonthEnd/" + setMonth() + "/Fortega/ " + setYear() + "-" + setMonthNo() + "_Veritas_SellerFortega.txt");
            CSharpDBO.CSharpDBO dBO = new CSharpDBO.CSharpDBO();
            dBO.dboOpen(path);
            string sSQL = "select distinct d.DealerNo, d.DealerName, d.Addr1 + '' + case when d.Addr2 is null then '' else d.Addr2 end as address, d.City, d.State, d.Zip,c.FormNo, " +
                "case when DateSigned is null then '1/1/2020' else DateSigned end as effectiveDate " +
                "from contract c " +
                "inner join Dealer d on d.DealerID = c.DealerID " +
                "where InsCarrierID = 5";
            System.Data.DataTable dt = dBO.dboInportInformation(sSQL);
            dBO.dboClose();
            int tracker = dt.Rows.Count;
            lblProgressBar.Text = "Processing Fortega Sellers";
            lblProgressBar.Refresh();
            lblDealer.Text = "Dealer:" + tracker;
            lbCancel.Refresh();
            progressBar1.Value = 0;
            progressBar1.Maximum = tracker;
            progressBar1.Minimum = 0;
            progressBar1.Visible = true;
            progressBar1.Step = 1;
            foreach (DataRow dr in dt.Rows)
            {
                progressBar1.PerformStep();
                sLine = "";
                try
                {
                    sLine = sLine + "VER" + dr["DealerNo"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//1 DealerNo
                try
                {
                    sLine = sLine + dr["DealerName"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//2 DealerName
                try
                {
                    sLine = sLine + dr["address"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//3 address
                try
                {
                    sLine = sLine + dr["City"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//4 City
                try
                {
                    sLine = sLine + dr["State"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//5 State
                try
                {
                    sLine = sLine + dr["Zip"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//6 Zip
                try
                {
                    sLine = sLine + dr["FormNo"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//7 FormNo
                try
                {
                    sLine = sLine + dr["effectiveDate"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//8 effectiveDate

                streamWriterContract.WriteLine(sLine);
            }
            streamWriterContract.Close();
        }
        private void fortegaReserve()
        {
            string sLine = "";
            StreamWriter streamWriterContract = new StreamWriter(setPath() + "/MonthEnd/" + setMonth() + "/Fortega/ " + setYear() + "-" + setMonthNo() + "_Veritas_ReserveFortega.txt");
            CSharpDBO.CSharpDBO dBO = new CSharpDBO.CSharpDBO();
            dBO.dboOpen(path);
            string sSQL = "select distinct c.ContractNo, " +
                "rt1.amt as reserve, rt2.amt as surcharge, rt3.amt as clip from contract c " +
                "left join (select ContractID, sum(Amt) as amt from ContractAmt where RateTypeID in (select RateTypeID from RateType where RateCategoryID = 4) group by ContractID) as rt1 on rt1.ContractID = c.ContractID " +
                "left join (select ContractID, sum(Amt) as amt from ContractAmt where RateTypeID in (select RateTypeID from RateType where RateCategoryID = 3) group by ContractID) as rt2 on rt2.ContractID = c.ContractID " +
                "left join (select ContractID, sum(Amt) as amt from ContractAmt where RateTypeID = 8 group by ContractID) as rt3 on rt3.ContractID = c.ContractID" +
                " where c.InsCarrierID = 5 " +
                "and (c.status = 'Paid' or c.Status = 'Expired' or c.status = 'Cancelled') " +
                "and c.datePaid <= '" + dtpEndDate.Value + "' " +
                "and c.datePaid >= '" + dtpStartDate.Value + "' " +
                "and inssaletransmissiondate is null " +
                "and not c.DatePaid is null " +
                "or c.contractno in (select contractNo from fortegaContractAdd where AddOrRemove = 'A' and Status = 'Paid') " +
                "and not c.contractno in (select contractno from fortegaContractAdd where AddOrRemove = 'R')";
            System.Data.DataTable dt = dBO.dboInportInformation(sSQL);
            dBO.dboClose();
            int tracker = dt.Rows.Count;
            lblProgressBar.Text = "Processing Fortega Reserves";
            lblProgressBar.Refresh();                        
            progressBar1.Value = 0;
            progressBar1.Maximum = tracker;
            progressBar1.Minimum = 0;
            progressBar1.Visible = true;
            progressBar1.Step = 1;
            foreach (DataRow dr in dt.Rows)
            {
                progressBar1.PerformStep();
                sLine = "";
                try
                {
                    sLine = sLine + "VER" + dr["ContractNo"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//1 ContractNo
                try
                {
                    sLine = sLine + dr["reserve"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//2 reserve
                try
                {
                    sLine = sLine + dr["surcharge"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//3 surcharge
                try
                {
                    sLine = sLine + dr["clip"].ToString() + "\t";
                }
                catch
                {
                    sLine = sLine + "\t";
                }//4 clip

                streamWriterContract.WriteLine(sLine);
            }
            streamWriterContract.Close();


        }

        //Reset/Saved Button Controls
            //AmTrust
        private void btnResetContractsAmTrust_Click(object sender, EventArgs e)
        {
            btnClear.Enabled = false;
            btnExcel.Enabled = false;
            btnTransmit.Enabled = false;
            btnUploadTransmittedFiles.Enabled = false;

            btnClaimsResetArch.Enabled = false;
            btnClaimsSaveArch.Enabled = false;
            btnResetContractArch.Enabled = false;
            btnSaveContractsArch.Enabled = false;

            btnResetClaimsAmTrust.Enabled = false;
            btnSaveClaimsAmtrust.Enabled = false;
            btnResetContractsAmTrust.Enabled = false;
            btnSaveContractsAmtrust.Enabled = false;

            btnResetClaimsOldRep.Enabled = false;
            btnSaveClaimsOldRep.Enabled = false;
            btnSaveContractsOldRep.Enabled = false;
            btnResetContractsOldRep.Enabled = false;

            btnClaimSaveFortega.Enabled = false;
            btnContractSaveFortega.Enabled = false;
            btnResetClaimsFortega.Enabled = false;
            btnResetContractsFortega.Enabled = false;

            string caption = "";
            var result = MessageBox.Show("Are you sure you want to clear the AmTrust contracts?", caption, MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                CSharpDBO.CSharpDBO dBO = new CSharpDBO.CSharpDBO();
                dBO.dboOpen(path);
                string SQL = "truncate table AmTrustContractAdd";
                dBO.dboAlterDBOUnsafe(SQL);
                MessageBox.Show("Saved Contracts cleared");
                dBO.dboClose();
                rtbContractsPaidAddAmTrust.Clear();
                rtbCancelledContractsAddAmTrust.Clear();
                rtbRemoveContractAmTrust.Clear();
            }

            btnClear.Enabled = true;
            btnExcel.Enabled = true;
            btnTransmit.Enabled = true;
            btnUploadTransmittedFiles.Enabled = true;

            btnClaimsResetArch.Enabled = true;
            btnClaimsSaveArch.Enabled = true;
            btnResetContractArch.Enabled = true;
            btnSaveContractsArch.Enabled = true;

            btnResetClaimsAmTrust.Enabled = true;
            btnSaveClaimsAmtrust.Enabled = true;
            btnResetContractsAmTrust.Enabled = true;
            btnSaveContractsAmtrust.Enabled = true;

            btnResetClaimsOldRep.Enabled = true;
            btnSaveClaimsOldRep.Enabled = true;
            btnSaveContractsOldRep.Enabled = true;
            btnResetContractsOldRep.Enabled = true;

            btnClaimSaveFortega.Enabled = true;
            btnContractSaveFortega.Enabled = true;
            btnResetClaimsFortega.Enabled = true;
            btnResetContractsFortega.Enabled = true;
        }
        private void btnSaveContractsAmtrust_Click(object sender, EventArgs e)
        {
            btnClear.Enabled = false;
            btnExcel.Enabled = false;
            btnTransmit.Enabled = false;
            btnUploadTransmittedFiles.Enabled = false;

            btnClaimsResetArch.Enabled = false;
            btnClaimsSaveArch.Enabled = false;
            btnResetContractArch.Enabled = false;
            btnSaveContractsArch.Enabled = false;

            btnResetClaimsAmTrust.Enabled = false;
            btnSaveClaimsAmtrust.Enabled = false;
            btnResetContractsAmTrust.Enabled = false;
            btnSaveContractsAmtrust.Enabled = false;

            btnResetClaimsOldRep.Enabled = false;
            btnSaveClaimsOldRep.Enabled = false;
            btnSaveContractsOldRep.Enabled = false;
            btnResetContractsOldRep.Enabled = false;

            btnClaimSaveFortega.Enabled = false;
            btnContractSaveFortega.Enabled = false;
            btnResetClaimsFortega.Enabled = false;
            btnResetContractsFortega.Enabled = false;

            CSharpDBO.CSharpDBO dBO = new CSharpDBO.CSharpDBO();
            dBO.dboOpen(path);
            string sSQL = "";
            var linesPaid = rtbContractsPaidAddAmTrust.Text.Split('\n').ToList();
            var linesCancelled = rtbCancelledContractsAddAmTrust.Text.Split('\n').ToList();
            var linesRemove = rtbRemoveContractAmTrust.Text.Split('\n').ToList();

            int iCounter = linesPaid.Count + linesCancelled.Count + linesRemove.Count;

            lblProgressBar.Text = "Saving AmTrust Contracts";
            lblProgressBar.Refresh();
            progressBar1.Value = 0;
            progressBar1.Maximum = iCounter;
            progressBar1.Minimum = 0;
            progressBar1.Visible = true;
            progressBar1.Step = 1;



            if (linesPaid[0].Length > 1 || linesCancelled[0].Length > 1 || linesRemove[0].Length > 1)
            {
                sSQL = "truncate table AmTrustContractAdd";
                dBO.dboAlterDBOUnsafe(sSQL);
            }
            foreach (var item in linesPaid)
            {
                progressBar1.PerformStep();
                if (item.Length > 1)
                {
                    sSQL = "insert into AmTrustContractAdd(ContractNo, AddOrRemove, Status) Values('" + item + "','A','Paid')";
                    dBO.dboAlterDBOUnsafe(sSQL);
                }
            }
            foreach (var item in linesCancelled)
            {
                progressBar1.PerformStep();
                if (item.Length > 1)
                {
                    sSQL = "insert into AmTrustContractAdd(ContractNo, AddOrRemove, Status) Values('" + item + "','A','Cancelled')";
                    dBO.dboAlterDBOUnsafe(sSQL);
                }
            }
            foreach (var item in linesRemove)
            {
                progressBar1.PerformStep();
                if (item.Length > 1)
                {
                    sSQL = "insert into AmTrustContractAdd(ContractNo, AddOrRemove, Status) Values('" + item + "','R','Remove')";
                    dBO.dboAlterDBOUnsafe(sSQL);
                }
            }

            btnClear.Enabled = true;
            btnExcel.Enabled = true;
            btnTransmit.Enabled = true;
            btnUploadTransmittedFiles.Enabled = true;

            btnClaimsResetArch.Enabled = true;
            btnClaimsSaveArch.Enabled = true;
            btnResetContractArch.Enabled = true;
            btnSaveContractsArch.Enabled = true;

            btnResetClaimsAmTrust.Enabled = true;
            btnSaveClaimsAmtrust.Enabled = true;
            btnResetContractsAmTrust.Enabled = true;
            btnSaveContractsAmtrust.Enabled = true;

            btnResetClaimsOldRep.Enabled = true;
            btnSaveClaimsOldRep.Enabled = true;
            btnSaveContractsOldRep.Enabled = true;
            btnResetContractsOldRep.Enabled = true;

            btnClaimSaveFortega.Enabled = true;
            btnContractSaveFortega.Enabled = true;
            btnResetClaimsFortega.Enabled = true;
            btnResetContractsFortega.Enabled = true;

            MessageBox.Show("Save Complete");

        }
        private void btnResetClaimsAmTrust_Click(object sender, EventArgs e)
        {
            btnClear.Enabled = false;
            btnExcel.Enabled = false;
            btnTransmit.Enabled = false;
            btnUploadTransmittedFiles.Enabled = false;

            btnClaimsResetArch.Enabled = false;
            btnClaimsSaveArch.Enabled = false;
            btnResetContractArch.Enabled = false;
            btnSaveContractsArch.Enabled = false;

            btnResetClaimsAmTrust.Enabled = false;
            btnSaveClaimsAmtrust.Enabled = false;
            btnResetContractsAmTrust.Enabled = false;
            btnSaveContractsAmtrust.Enabled = false;

            btnResetClaimsOldRep.Enabled = false;
            btnSaveClaimsOldRep.Enabled = false;
            btnSaveContractsOldRep.Enabled = false;
            btnResetContractsOldRep.Enabled = false;

            btnClaimSaveFortega.Enabled = false;
            btnContractSaveFortega.Enabled = false;
            btnResetClaimsFortega.Enabled = false;
            btnResetContractsFortega.Enabled = false;

            string caption = "";
            var result = MessageBox.Show("Are you sure you want to clear the AmTrust Claims?", caption, MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                CSharpDBO.CSharpDBO dBO = new CSharpDBO.CSharpDBO();
                dBO.dboOpen(path);
                string SQL = "truncate table AmTrustClaimAdd";
                dBO.dboAlterDBOUnsafe(SQL);
                MessageBox.Show("Saved Claims cleared");
                dBO.dboClose();
                rtbAddClaimsAmTrust.Clear();
                rtbRemoveClaimsAmTrust.Clear();
            }

            btnClear.Enabled = true;
            btnExcel.Enabled = true;
            btnTransmit.Enabled = true;
            btnUploadTransmittedFiles.Enabled = true;

            btnClaimsResetArch.Enabled = true;
            btnClaimsSaveArch.Enabled = true;
            btnResetContractArch.Enabled = true;
            btnSaveContractsArch.Enabled = true;

            btnResetClaimsAmTrust.Enabled = true;
            btnSaveClaimsAmtrust.Enabled = true;
            btnResetContractsAmTrust.Enabled = true;
            btnSaveContractsAmtrust.Enabled = true;

            btnResetClaimsOldRep.Enabled = true;
            btnSaveClaimsOldRep.Enabled = true;
            btnSaveContractsOldRep.Enabled = true;
            btnResetContractsOldRep.Enabled = true;

            btnClaimSaveFortega.Enabled = true;
            btnContractSaveFortega.Enabled = true;
            btnResetClaimsFortega.Enabled = true;
            btnResetContractsFortega.Enabled = true;

        }
        private void btnSaveClaimsAmtrust_Click(object sender, EventArgs e)
        {
            btnClear.Enabled = false;
            btnExcel.Enabled = false;
            btnTransmit.Enabled = false;
            btnUploadTransmittedFiles.Enabled = false;

            btnClaimsResetArch.Enabled = false;
            btnClaimsSaveArch.Enabled = false;
            btnResetContractArch.Enabled = false;
            btnSaveContractsArch.Enabled = false;

            btnResetClaimsAmTrust.Enabled = false;
            btnSaveClaimsAmtrust.Enabled = false;
            btnResetContractsAmTrust.Enabled = false;
            btnSaveContractsAmtrust.Enabled = false;

            btnResetClaimsOldRep.Enabled = false;
            btnSaveClaimsOldRep.Enabled = false;
            btnSaveContractsOldRep.Enabled = false;
            btnResetContractsOldRep.Enabled = false;

            btnClaimSaveFortega.Enabled = false;
            btnContractSaveFortega.Enabled = false;
            btnResetClaimsFortega.Enabled = false;
            btnResetContractsFortega.Enabled = false;

            CSharpDBO.CSharpDBO dBO = new CSharpDBO.CSharpDBO();
            dBO.dboOpen(path);
            string sSQL = "";
            var linesAdd = rtbAddClaimsAmTrust.Text.Split('\n').ToList();
            var linesRemove = rtbRemoveClaimsAmTrust.Text.Split('\n').ToList();

            int iCounter = linesAdd.Count + linesRemove.Count;

            lblProgressBar.Text = "Saving AmTrust Claims";
            lblProgressBar.Refresh();
            progressBar1.Value = 0;
            progressBar1.Maximum = iCounter;
            progressBar1.Minimum = 0;
            progressBar1.Visible = true;
            progressBar1.Step = 1;

            if (linesAdd[0].Length > 1 || linesRemove[0].Length > 1)
            {
                sSQL = "truncate table AmTrustClaimAdd";
                dBO.dboAlterDBOUnsafe(sSQL);
            }
            foreach (var item in linesAdd)
            {
                progressBar1.PerformStep();
                if (item.Length > 1)
                {
                    sSQL = "insert into AmTrustClaimAdd(ClaimNo, AddOrRemove) Values('" + item + "','A')";
                    dBO.dboAlterDBOUnsafe(sSQL);
                }
            }
            foreach (var item in linesRemove)
            {
                progressBar1.PerformStep();
                if (item.Length > 1)
                {
                    sSQL = "insert into AmTrustClaimAdd(ClaimNo, AddOrRemove) Values('" + item + "','R')";
                    dBO.dboAlterDBOUnsafe(sSQL);
                }
            }

            btnClear.Enabled = true;
            btnExcel.Enabled = true;
            btnTransmit.Enabled = true;
            btnUploadTransmittedFiles.Enabled = true;

            btnClaimsResetArch.Enabled = true;
            btnClaimsSaveArch.Enabled = true;
            btnResetContractArch.Enabled = true;
            btnSaveContractsArch.Enabled = true;

            btnResetClaimsAmTrust.Enabled = true;
            btnSaveClaimsAmtrust.Enabled = true;
            btnResetContractsAmTrust.Enabled = true;
            btnSaveContractsAmtrust.Enabled = true;

            btnResetClaimsOldRep.Enabled = true;
            btnSaveClaimsOldRep.Enabled = true;
            btnSaveContractsOldRep.Enabled = true;
            btnResetContractsOldRep.Enabled = true;

            btnClaimSaveFortega.Enabled = true;
            btnContractSaveFortega.Enabled = true;
            btnResetClaimsFortega.Enabled = true;
            btnResetContractsFortega.Enabled = true;

            MessageBox.Show("Save Complete");


        }

            //Arch
        private void btnResetContractArch_Click(object sender, EventArgs e)
        {
            btnClear.Enabled = false;
            btnExcel.Enabled = false;
            btnTransmit.Enabled = false;
            btnUploadTransmittedFiles.Enabled = false;

            btnClaimsResetArch.Enabled = false;
            btnClaimsSaveArch.Enabled = false;
            btnResetContractArch.Enabled = false;
            btnSaveContractsArch.Enabled = false;

            btnResetClaimsAmTrust.Enabled = false;
            btnSaveClaimsAmtrust.Enabled = false;
            btnResetContractsAmTrust.Enabled = false;
            btnSaveContractsAmtrust.Enabled = false;

            btnResetClaimsOldRep.Enabled = false;
            btnSaveClaimsOldRep.Enabled = false;
            btnSaveContractsOldRep.Enabled = false;
            btnResetContractsOldRep.Enabled = false;

            btnClaimSaveFortega.Enabled = false;
            btnContractSaveFortega.Enabled = false;
            btnResetClaimsFortega.Enabled = false;
            btnResetContractsFortega.Enabled = false;

            string caption = "";
            var result = MessageBox.Show("Are you sure you want to clear the Arch contracts?", caption, MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                CSharpDBO.CSharpDBO dBO = new CSharpDBO.CSharpDBO();
                dBO.dboOpen(path);
                string SQL = "truncate table ArchContractAdd";
                dBO.dboAlterDBOUnsafe(SQL);
                MessageBox.Show("Saved Contracts cleared");
                dBO.dboClose();
                rtbCancelledContractsArch.Clear();
                rtbPaidContractsArch.Clear();
                rtbRemoveContractArch.Clear();
            }

            btnClear.Enabled = true;
            btnExcel.Enabled = true;
            btnTransmit.Enabled = true;
            btnUploadTransmittedFiles.Enabled = true;

            btnClaimsResetArch.Enabled = true;
            btnClaimsSaveArch.Enabled = true;
            btnResetContractArch.Enabled = true;
            btnSaveContractsArch.Enabled = true;

            btnResetClaimsAmTrust.Enabled = true;
            btnSaveClaimsAmtrust.Enabled = true;
            btnResetContractsAmTrust.Enabled = true;
            btnSaveContractsAmtrust.Enabled = true;

            btnResetClaimsOldRep.Enabled = true;
            btnSaveClaimsOldRep.Enabled = true;
            btnSaveContractsOldRep.Enabled = true;
            btnResetContractsOldRep.Enabled = true;

            btnClaimSaveFortega.Enabled = true;
            btnContractSaveFortega.Enabled = true;
            btnResetClaimsFortega.Enabled = true;
            btnResetContractsFortega.Enabled = true;
        }
        private void btnSaveContractsArch_Click(object sender, EventArgs e)
        {
            btnClear.Enabled = false;
            btnExcel.Enabled = false;
            btnTransmit.Enabled = false;
            btnUploadTransmittedFiles.Enabled = false;

            btnClaimsResetArch.Enabled = false;
            btnClaimsSaveArch.Enabled = false;
            btnResetContractArch.Enabled = false;
            btnSaveContractsArch.Enabled = false;

            btnResetClaimsAmTrust.Enabled = false;
            btnSaveClaimsAmtrust.Enabled = false;
            btnResetContractsAmTrust.Enabled = false;
            btnSaveContractsAmtrust.Enabled = false;

            btnResetClaimsOldRep.Enabled = false;
            btnSaveClaimsOldRep.Enabled = false;
            btnSaveContractsOldRep.Enabled = false;
            btnResetContractsOldRep.Enabled = false;

            btnClaimSaveFortega.Enabled = false;
            btnContractSaveFortega.Enabled = false;
            btnResetClaimsFortega.Enabled = false;
            btnResetContractsFortega.Enabled = false;

            CSharpDBO.CSharpDBO dBO = new CSharpDBO.CSharpDBO();
            dBO.dboOpen(path);
            string sSQL = "";
            var linesPaid = rtbPaidContractsArch.Text.Split('\n').ToList();
            var linesCancelled = rtbCancelledContractsArch.Text.Split('\n').ToList();
            var linesRemove = rtbRemoveContractArch.Text.Split('\n').ToList();

            int iCounter = linesPaid.Count + linesCancelled.Count + linesRemove.Count;

            lblProgressBar.Text = "Saving Arch Contracts";
            lblProgressBar.Refresh();
            progressBar1.Value = 0;
            progressBar1.Maximum = iCounter;
            progressBar1.Minimum = 0;
            progressBar1.Visible = true;
            progressBar1.Step = 1;

            if (linesPaid[0].Length > 1 || linesCancelled[0].Length > 1 || linesRemove[0].Length > 1)
            {
                sSQL = "truncate table ArchContractAdd";
                dBO.dboAlterDBOUnsafe(sSQL);
            }
            foreach (var item in linesPaid)
            {
                progressBar1.PerformStep();
                if (item.Length > 1)
                {
                    sSQL = "insert into ArchContractAdd(ContractNo, AddOrRemove, Status) Values('" + item + "','A','Paid')";
                    dBO.dboAlterDBOUnsafe(sSQL);
                }
            }
            foreach (var item in linesCancelled)
            {
                progressBar1.PerformStep();
                if (item.Length > 1)
                {
                    sSQL = "insert into ArchContractAdd(ContractNo, AddOrRemove, Status) Values('" + item + "','A','Cancelled')";
                    dBO.dboAlterDBOUnsafe(sSQL);
                }
            }
            foreach (var item in linesRemove)
            {
                progressBar1.PerformStep();
                if (item.Length > 1)
                {
                    sSQL = "insert into ArchContractAdd(ContractNo, AddOrRemove, Status) Values('" + item + "','R','Remove')";
                    dBO.dboAlterDBOUnsafe(sSQL);
                }
            }



            btnClear.Enabled = true;
            btnExcel.Enabled = true;
            btnTransmit.Enabled = true;
            btnUploadTransmittedFiles.Enabled = true;

            btnClaimsResetArch.Enabled = true;
            btnClaimsSaveArch.Enabled = true;
            btnResetContractArch.Enabled = true;
            btnSaveContractsArch.Enabled = true;

            btnResetClaimsAmTrust.Enabled = true;
            btnSaveClaimsAmtrust.Enabled = true;
            btnResetContractsAmTrust.Enabled = true;
            btnSaveContractsAmtrust.Enabled = true;

            btnResetClaimsOldRep.Enabled = true;
            btnSaveClaimsOldRep.Enabled = true;
            btnSaveContractsOldRep.Enabled = true;
            btnResetContractsOldRep.Enabled = true;

            btnClaimSaveFortega.Enabled = true;
            btnContractSaveFortega.Enabled = true;
            btnResetClaimsFortega.Enabled = true;
            btnResetContractsFortega.Enabled = true;

            MessageBox.Show("Save Complete");
        }
        private void btnClaimsResetArch_Click(object sender, EventArgs e)
        {
            btnClear.Enabled = false;
            btnExcel.Enabled = false;
            btnTransmit.Enabled = false;
            btnUploadTransmittedFiles.Enabled = false;

            btnClaimsResetArch.Enabled = false;
            btnClaimsSaveArch.Enabled = false;
            btnResetContractArch.Enabled = false;
            btnSaveContractsArch.Enabled = false;

            btnResetClaimsAmTrust.Enabled = false;
            btnSaveClaimsAmtrust.Enabled = false;
            btnResetContractsAmTrust.Enabled = false;
            btnSaveContractsAmtrust.Enabled = false;

            btnResetClaimsOldRep.Enabled = false;
            btnSaveClaimsOldRep.Enabled = false;
            btnSaveContractsOldRep.Enabled = false;
            btnResetContractsOldRep.Enabled = false;

            btnClaimSaveFortega.Enabled = false;
            btnContractSaveFortega.Enabled = false;
            btnResetClaimsFortega.Enabled = false;
            btnResetContractsFortega.Enabled = false;

            string caption = "";
            var result = MessageBox.Show("Are you sure you want to clear the Arch Claims?", caption, MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                CSharpDBO.CSharpDBO dBO = new CSharpDBO.CSharpDBO();
                dBO.dboOpen(path);
                string SQL = "truncate table ArchClaimAdd";
                dBO.dboAlterDBOUnsafe(SQL);
                MessageBox.Show("Saved Claims cleared");
                dBO.dboClose();
                rtbAddClaimsArch.Clear();
                rtbRemoveClaimsArch.Clear();
            }
            btnClear.Enabled = true;
            btnExcel.Enabled = true;
            btnTransmit.Enabled = true;
            btnUploadTransmittedFiles.Enabled = true;

            btnClaimsResetArch.Enabled = true;
            btnClaimsSaveArch.Enabled = true;
            btnResetContractArch.Enabled = true;
            btnSaveContractsArch.Enabled = true;

            btnResetClaimsAmTrust.Enabled = true;
            btnSaveClaimsAmtrust.Enabled = true;
            btnResetContractsAmTrust.Enabled = true;
            btnSaveContractsAmtrust.Enabled = true;

            btnResetClaimsOldRep.Enabled = true;
            btnSaveClaimsOldRep.Enabled = true;
            btnSaveContractsOldRep.Enabled = true;
            btnResetContractsOldRep.Enabled = true;

            btnClaimSaveFortega.Enabled = true;
            btnContractSaveFortega.Enabled = true;
            btnResetClaimsFortega.Enabled = true;
            btnResetContractsFortega.Enabled = true;
        }
        private void btnClaimsSaveArch_Click(object sender, EventArgs e)
        {
            btnClear.Enabled = false;
            btnExcel.Enabled = false;
            btnTransmit.Enabled = false;
            btnUploadTransmittedFiles.Enabled = false;

            btnClaimsResetArch.Enabled = false;
            btnClaimsSaveArch.Enabled = false;
            btnResetContractArch.Enabled = false;
            btnSaveContractsArch.Enabled = false;

            btnResetClaimsAmTrust.Enabled = false;
            btnSaveClaimsAmtrust.Enabled = false;
            btnResetContractsAmTrust.Enabled = false;
            btnSaveContractsAmtrust.Enabled = false;

            btnResetClaimsOldRep.Enabled = false;
            btnSaveClaimsOldRep.Enabled = false;
            btnSaveContractsOldRep.Enabled = false;
            btnResetContractsOldRep.Enabled = false;

            btnClaimSaveFortega.Enabled = false;
            btnContractSaveFortega.Enabled = false;
            btnResetClaimsFortega.Enabled = false;
            btnResetContractsFortega.Enabled = false;

            CSharpDBO.CSharpDBO dBO = new CSharpDBO.CSharpDBO();
            dBO.dboOpen(path);
            string sSQL = "";
            var linesAdd = rtbAddClaimsArch.Text.Split('\n').ToList();
            var linesRemove = rtbRemoveClaimsArch.Text.Split('\n').ToList();

            int iCounter = linesAdd.Count + linesRemove.Count;

            lblProgressBar.Text = "Saving Arch Claims";
            lblProgressBar.Refresh();
            progressBar1.Value = 0;
            progressBar1.Maximum = iCounter;
            progressBar1.Minimum = 0;
            progressBar1.Visible = true;
            progressBar1.Step = 1;

            if (linesAdd[0].Length > 1 || linesRemove[0].Length > 1)
            {
                sSQL = "truncate table ArchClaimAdd";
                dBO.dboAlterDBOUnsafe(sSQL);
            }
            foreach (var item in linesAdd)
            {
                progressBar1.PerformStep();
                if (item.Length > 1)
                {
                    sSQL = "insert into ArchClaimAdd(ClaimNo, AddOrRemove) Values('" + item + "','A')";
                    dBO.dboAlterDBOUnsafe(sSQL);
                }
            }
            foreach (var item in linesRemove)
            {
                progressBar1.PerformStep();
                if (item.Length > 1)
                {
                    sSQL = "insert into ArchClaimAdd(ClaimNo, AddOrRemove) Values('" + item + "','R')";
                    dBO.dboAlterDBOUnsafe(sSQL);
                }
            }
            btnClear.Enabled = true;
            btnExcel.Enabled = true;
            btnTransmit.Enabled = true;
            btnUploadTransmittedFiles.Enabled = true;

            btnClaimsResetArch.Enabled = true;
            btnClaimsSaveArch.Enabled = true;
            btnResetContractArch.Enabled = true;
            btnSaveContractsArch.Enabled = true;

            btnResetClaimsAmTrust.Enabled = true;
            btnSaveClaimsAmtrust.Enabled = true;
            btnResetContractsAmTrust.Enabled = true;
            btnSaveContractsAmtrust.Enabled = true;

            btnResetClaimsOldRep.Enabled = true;
            btnSaveClaimsOldRep.Enabled = true;
            btnSaveContractsOldRep.Enabled = true;
            btnResetContractsOldRep.Enabled = true;

            btnClaimSaveFortega.Enabled = true;
            btnContractSaveFortega.Enabled = true;
            btnResetClaimsFortega.Enabled = true;
            btnResetContractsFortega.Enabled = true;

            MessageBox.Show("Save Complete");
        }

            //OldRep
        private void btnResetContractsOldRep_Click(object sender, EventArgs e)
        {
            btnClear.Enabled = false;
            btnExcel.Enabled = false;
            btnTransmit.Enabled = false;
            btnUploadTransmittedFiles.Enabled = false;

            btnClaimsResetArch.Enabled = false;
            btnClaimsSaveArch.Enabled = false;
            btnResetContractArch.Enabled = false;
            btnSaveContractsArch.Enabled = false;

            btnResetClaimsAmTrust.Enabled = false;
            btnSaveClaimsAmtrust.Enabled = false;
            btnResetContractsAmTrust.Enabled = false;
            btnSaveContractsAmtrust.Enabled = false;

            btnResetClaimsOldRep.Enabled = false;
            btnSaveClaimsOldRep.Enabled = false;
            btnSaveContractsOldRep.Enabled = false;
            btnResetContractsOldRep.Enabled = false;

            btnClaimSaveFortega.Enabled = false;
            btnContractSaveFortega.Enabled = false;
            btnResetClaimsFortega.Enabled = false;
            btnResetContractsFortega.Enabled = false;

            string caption = "";
            var result = MessageBox.Show("Are you sure you want to clear the Old Republic contracts?", caption, MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                CSharpDBO.CSharpDBO dBO = new CSharpDBO.CSharpDBO();
                dBO.dboOpen(path);
                string SQL = "truncate table OldRepublicContractAdd";
                dBO.dboAlterDBOUnsafe(SQL);
                MessageBox.Show("Saved Contracts cleared");
                dBO.dboClose();
                rtbContractPaidAddOldRep.Clear();
                rtbContractCancelledAddOldRep.Clear();
                rtbContractRemoveOldRep.Clear();
            }
            btnClear.Enabled = true;
            btnExcel.Enabled = true;
            btnTransmit.Enabled = true;
            btnUploadTransmittedFiles.Enabled = true;

            btnClaimsResetArch.Enabled = true;
            btnClaimsSaveArch.Enabled = true;
            btnResetContractArch.Enabled = true;
            btnSaveContractsArch.Enabled = true;

            btnResetClaimsAmTrust.Enabled = true;
            btnSaveClaimsAmtrust.Enabled = true;
            btnResetContractsAmTrust.Enabled = true;
            btnSaveContractsAmtrust.Enabled = true;

            btnResetClaimsOldRep.Enabled = true;
            btnSaveClaimsOldRep.Enabled = true;
            btnSaveContractsOldRep.Enabled = true;
            btnResetContractsOldRep.Enabled = true;

            btnClaimSaveFortega.Enabled = true;
            btnContractSaveFortega.Enabled = true;
            btnResetClaimsFortega.Enabled = true;
            btnResetContractsFortega.Enabled = true;
        }
        private void btnSaveContractsOldRep_Click(object sender, EventArgs e)
        {
            btnClear.Enabled = false;
            btnExcel.Enabled = false;
            btnTransmit.Enabled = false;
            btnUploadTransmittedFiles.Enabled = false;

            btnClaimsResetArch.Enabled = false;
            btnClaimsSaveArch.Enabled = false;
            btnResetContractArch.Enabled = false;
            btnSaveContractsArch.Enabled = false;

            btnResetClaimsAmTrust.Enabled = false;
            btnSaveClaimsAmtrust.Enabled = false;
            btnResetContractsAmTrust.Enabled = false;
            btnSaveContractsAmtrust.Enabled = false;

            btnResetClaimsOldRep.Enabled = false;
            btnSaveClaimsOldRep.Enabled = false;
            btnSaveContractsOldRep.Enabled = false;
            btnResetContractsOldRep.Enabled = false;

            btnClaimSaveFortega.Enabled = false;
            btnContractSaveFortega.Enabled = false;
            btnResetClaimsFortega.Enabled = false;
            btnResetContractsFortega.Enabled = false;

            CSharpDBO.CSharpDBO dBO = new CSharpDBO.CSharpDBO();
            dBO.dboOpen(path);
            string sSQL = "";
            var linesPaid = rtbContractPaidAddOldRep.Text.Split('\n').ToList();
            var linesCancelled = rtbContractCancelledAddOldRep.Text.Split('\n').ToList();
            var linesRemove = rtbContractRemoveOldRep.Text.Split('\n').ToList();

            int iCounter = linesPaid.Count + linesCancelled.Count + linesRemove.Count;

            lblProgressBar.Text = "Saving Old Republic Contracts";
            lblProgressBar.Refresh();
            progressBar1.Value = 0;
            progressBar1.Maximum = iCounter;
            progressBar1.Minimum = 0;
            progressBar1.Visible = true;
            progressBar1.Step = 1;

            if (linesPaid[0].Length > 1 || linesCancelled[0].Length > 1 || linesRemove[0].Length > 1)
            {
                sSQL = "truncate table OldRepublicContractAdd";
                dBO.dboAlterDBOUnsafe(sSQL);
            }
            foreach (var item in linesPaid)
            {
                progressBar1.PerformStep();
                if (item.Length > 1)
                {
                    sSQL = "insert into OldRepublicContractAdd(ContractNo, AddOrRemove, Status) Values('" + item + "','A','Paid')";
                    dBO.dboAlterDBOUnsafe(sSQL);
                }
            }
            foreach (var item in linesCancelled)
            {
                progressBar1.PerformStep();
                if (item.Length > 1)
                {
                    sSQL = "insert into OldRepublicContractAdd(ContractNo, AddOrRemove, Status) Values('" + item + "','A','Cancelled')";
                    dBO.dboAlterDBOUnsafe(sSQL);
                }
            }
            foreach (var item in linesRemove)
            {
                progressBar1.PerformStep();
                if (item.Length > 1)
                {
                    sSQL = "insert into OldRepublicContractAdd(ContractNo, AddOrRemove, Status) Values('" + item + "','R','Remove')";
                    dBO.dboAlterDBOUnsafe(sSQL);
                }
            }
            btnClear.Enabled = true;
            btnExcel.Enabled = true;
            btnTransmit.Enabled = true;
            btnUploadTransmittedFiles.Enabled = true;

            btnClaimsResetArch.Enabled = true;
            btnClaimsSaveArch.Enabled = true;
            btnResetContractArch.Enabled = true;
            btnSaveContractsArch.Enabled = true;

            btnResetClaimsAmTrust.Enabled = true;
            btnSaveClaimsAmtrust.Enabled = true;
            btnResetContractsAmTrust.Enabled = true;
            btnSaveContractsAmtrust.Enabled = true;

            btnResetClaimsOldRep.Enabled = true;
            btnSaveClaimsOldRep.Enabled = true;
            btnSaveContractsOldRep.Enabled = true;
            btnResetContractsOldRep.Enabled = true;

            btnClaimSaveFortega.Enabled = true;
            btnContractSaveFortega.Enabled = true;
            btnResetClaimsFortega.Enabled = true;
            btnResetContractsFortega.Enabled = true;

            MessageBox.Show("Save Complete");
        }
        private void btnResetClaimsOldRep_Click(object sender, EventArgs e)
        {
            btnClear.Enabled = false;
            btnExcel.Enabled = false;
            btnTransmit.Enabled = false;
            btnUploadTransmittedFiles.Enabled = false;

            btnClaimsResetArch.Enabled = false;
            btnClaimsSaveArch.Enabled = false;
            btnResetContractArch.Enabled = false;
            btnSaveContractsArch.Enabled = false;

            btnResetClaimsAmTrust.Enabled = false;
            btnSaveClaimsAmtrust.Enabled = false;
            btnResetContractsAmTrust.Enabled = false;
            btnSaveContractsAmtrust.Enabled = false;

            btnResetClaimsOldRep.Enabled = false;
            btnSaveClaimsOldRep.Enabled = false;
            btnSaveContractsOldRep.Enabled = false;
            btnResetContractsOldRep.Enabled = false;

            btnClaimSaveFortega.Enabled = false;
            btnContractSaveFortega.Enabled = false;
            btnResetClaimsFortega.Enabled = false;
            btnResetContractsFortega.Enabled = false;

            string caption = "";
            var result = MessageBox.Show("Are you sure you want to clear the Old Republic Claims?", caption, MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                CSharpDBO.CSharpDBO dBO = new CSharpDBO.CSharpDBO();
                dBO.dboOpen(path);
                string SQL = "truncate table OldRepublicClaimAdd";
                dBO.dboAlterDBOUnsafe(SQL);
                MessageBox.Show("Saved Claims cleared");
                dBO.dboClose();
                rtbClaimAddOldRep.Clear();
                rtbClaimRemoveOldRep.Clear();
            }
            btnClear.Enabled = true;
            btnExcel.Enabled = true;
            btnTransmit.Enabled = true;
            btnUploadTransmittedFiles.Enabled = true;

            btnClaimsResetArch.Enabled = true;
            btnClaimsSaveArch.Enabled = true;
            btnResetContractArch.Enabled = true;
            btnSaveContractsArch.Enabled = true;

            btnResetClaimsAmTrust.Enabled = true;
            btnSaveClaimsAmtrust.Enabled = true;
            btnResetContractsAmTrust.Enabled = true;
            btnSaveContractsAmtrust.Enabled = true;

            btnResetClaimsOldRep.Enabled = true;
            btnSaveClaimsOldRep.Enabled = true;
            btnSaveContractsOldRep.Enabled = true;
            btnResetContractsOldRep.Enabled = true;

            btnClaimSaveFortega.Enabled = true;
            btnContractSaveFortega.Enabled = true;
            btnResetClaimsFortega.Enabled = true;
            btnResetContractsFortega.Enabled = true;

        }
        private void btnSaveClaimsOldRep_Click(object sender, EventArgs e)
        {
            btnClear.Enabled = false;
            btnExcel.Enabled = false;
            btnTransmit.Enabled = false;
            btnUploadTransmittedFiles.Enabled = false;

            btnClaimsResetArch.Enabled = false;
            btnClaimsSaveArch.Enabled = false;
            btnResetContractArch.Enabled = false;
            btnSaveContractsArch.Enabled = false;

            btnResetClaimsAmTrust.Enabled = false;
            btnSaveClaimsAmtrust.Enabled = false;
            btnResetContractsAmTrust.Enabled = false;
            btnSaveContractsAmtrust.Enabled = false;

            btnResetClaimsOldRep.Enabled = false;
            btnSaveClaimsOldRep.Enabled = false;
            btnSaveContractsOldRep.Enabled = false;
            btnResetContractsOldRep.Enabled = false;

            btnClaimSaveFortega.Enabled = false;
            btnContractSaveFortega.Enabled = false;
            btnResetClaimsFortega.Enabled = false;
            btnResetContractsFortega.Enabled = false;

            CSharpDBO.CSharpDBO dBO = new CSharpDBO.CSharpDBO();
            dBO.dboOpen(path);
            string sSQL = "";
            var linesAdd = rtbClaimAddOldRep.Text.Split('\n').ToList();
            var linesRemove = rtbClaimRemoveOldRep.Text.Split('\n').ToList();

            int iCounter = linesAdd.Count + linesRemove.Count;

            lblProgressBar.Text = "Saving Old Republic Claims";
            lblProgressBar.Refresh();
            progressBar1.Value = 0;
            progressBar1.Maximum = iCounter;
            progressBar1.Minimum = 0;
            progressBar1.Visible = true;
            progressBar1.Step = 1;

            if (linesAdd[0].Length > 1 || linesRemove[0].Length > 1)
            {
                sSQL = "truncate table OldRepublicClaimAdd";
                dBO.dboAlterDBOUnsafe(sSQL);
            }
            foreach (var item in linesAdd)
            {
                progressBar1.PerformStep();
                if (item.Length > 1)
                {
                    sSQL = "insert into OldRepublicClaimAdd(ClaimNo, AddOrRemove) Values('" + item + "','A')";
                    dBO.dboAlterDBOUnsafe(sSQL);
                }
            }
            foreach (var item in linesRemove)
            {
                progressBar1.PerformStep();
                if (item.Length > 1)
                {
                    sSQL = "insert into OldRepublicClaimAdd(ClaimNo, AddOrRemove) Values('" + item + "','R')";
                    dBO.dboAlterDBOUnsafe(sSQL);
                }
            }
            btnClear.Enabled = true;
            btnExcel.Enabled = true;
            btnTransmit.Enabled = true;
            btnUploadTransmittedFiles.Enabled = true;

            btnClaimsResetArch.Enabled = true;
            btnClaimsSaveArch.Enabled = true;
            btnResetContractArch.Enabled = true;
            btnSaveContractsArch.Enabled = true;

            btnResetClaimsAmTrust.Enabled = true;
            btnSaveClaimsAmtrust.Enabled = true;
            btnResetContractsAmTrust.Enabled = true;
            btnSaveContractsAmtrust.Enabled = true;

            btnResetClaimsOldRep.Enabled = true;
            btnSaveClaimsOldRep.Enabled = true;
            btnSaveContractsOldRep.Enabled = true;
            btnResetContractsOldRep.Enabled = true;

            btnClaimSaveFortega.Enabled = true;
            btnContractSaveFortega.Enabled = true;
            btnResetClaimsFortega.Enabled = true;
            btnResetContractsFortega.Enabled = true;

            MessageBox.Show("Save Complete");
        }

            //Fortega
        private void btnResetContractsFortega_Click(object sender, EventArgs e)
        {
            btnClear.Enabled = false;
            btnExcel.Enabled = false;
            btnTransmit.Enabled = false;
            btnUploadTransmittedFiles.Enabled = false;

            btnClaimsResetArch.Enabled = false;
            btnClaimsSaveArch.Enabled = false;
            btnResetContractArch.Enabled = false;
            btnSaveContractsArch.Enabled = false;

            btnResetClaimsAmTrust.Enabled = false;
            btnSaveClaimsAmtrust.Enabled = false;
            btnResetContractsAmTrust.Enabled = false;
            btnSaveContractsAmtrust.Enabled = false;

            btnResetClaimsOldRep.Enabled = false;
            btnSaveClaimsOldRep.Enabled = false;
            btnSaveContractsOldRep.Enabled = false;
            btnResetContractsOldRep.Enabled = false;

            btnClaimSaveFortega.Enabled = false;
            btnContractSaveFortega.Enabled = false;
            btnResetClaimsFortega.Enabled = false;
            btnResetContractsFortega.Enabled = false;

            string caption = "";
            var result = MessageBox.Show("Are you sure you want to clear the Fortega contracts?", caption, MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                CSharpDBO.CSharpDBO dBO = new CSharpDBO.CSharpDBO();
                dBO.dboOpen(path);
                string SQL = "truncate table fortegaContractAdd";
                dBO.dboAlterDBOUnsafe(SQL);
                MessageBox.Show("Saved Contracts cleared");
                dBO.dboClose();
                rtbCancelContractsFortega.Clear();
                rtbContractsRemoveFortega.Clear();
                rtbPaidContractsFortega.Clear();
            }

            btnClear.Enabled = true;
            btnExcel.Enabled = true;
            btnTransmit.Enabled = true;
            btnUploadTransmittedFiles.Enabled = true;

            btnClaimsResetArch.Enabled = true;
            btnClaimsSaveArch.Enabled = true;
            btnResetContractArch.Enabled = true;
            btnSaveContractsArch.Enabled = true;

            btnResetClaimsAmTrust.Enabled = true;
            btnSaveClaimsAmtrust.Enabled = true;
            btnResetContractsAmTrust.Enabled = true;
            btnSaveContractsAmtrust.Enabled = true;

            btnResetClaimsOldRep.Enabled = true;
            btnSaveClaimsOldRep.Enabled = true;
            btnSaveContractsOldRep.Enabled = true;
            btnResetContractsOldRep.Enabled = true;

            btnClaimSaveFortega.Enabled = true;
            btnContractSaveFortega.Enabled = true;
            btnResetClaimsFortega.Enabled = true;
            btnResetContractsFortega.Enabled = true;
        }
        private void btnContractSaveFortega_Click(object sender, EventArgs e)
        {
            btnClear.Enabled = false;
            btnExcel.Enabled = false;
            btnTransmit.Enabled = false;
            btnUploadTransmittedFiles.Enabled = false;

            btnClaimsResetArch.Enabled = false;
            btnClaimsSaveArch.Enabled = false;
            btnResetContractArch.Enabled = false;
            btnSaveContractsArch.Enabled = false;

            btnResetClaimsAmTrust.Enabled = false;
            btnSaveClaimsAmtrust.Enabled = false;
            btnResetContractsAmTrust.Enabled = false;
            btnSaveContractsAmtrust.Enabled = false;

            btnResetClaimsOldRep.Enabled = false;
            btnSaveClaimsOldRep.Enabled = false;
            btnSaveContractsOldRep.Enabled = false;
            btnResetContractsOldRep.Enabled = false;

            btnClaimSaveFortega.Enabled = false;
            btnContractSaveFortega.Enabled = false;
            btnResetClaimsFortega.Enabled = false;
            btnResetContractsFortega.Enabled = false;

            CSharpDBO.CSharpDBO dBO = new CSharpDBO.CSharpDBO();
            dBO.dboOpen(path);
            string sSQL = "";
            var linesPaid = rtbPaidContractsFortega.Text.Split('\n').ToList();
            var linesCancelled = rtbCancelContractsFortega.Text.Split('\n').ToList();
            var linesRemove = rtbContractsRemoveFortega.Text.Split('\n').ToList();

            int iCounter = linesPaid.Count + linesCancelled.Count + linesRemove.Count;

            lblProgressBar.Text = "Saving Fortega Contracts";
            lblProgressBar.Refresh();
            progressBar1.Value = 0;
            progressBar1.Maximum = iCounter;
            progressBar1.Minimum = 0;
            progressBar1.Visible = true;
            progressBar1.Step = 1;



            if (linesPaid[0].Length > 1 || linesCancelled[0].Length > 1 || linesRemove[0].Length > 1)
            {
                sSQL = "truncate table fortegacontractadd";
                dBO.dboAlterDBOUnsafe(sSQL);
            }
            foreach (var item in linesPaid)
            {
                progressBar1.PerformStep();
                if (item.Length > 1)
                {
                    sSQL = "insert into fortegacontractadd(ContractNo, AddOrRemove, Status) Values('" + item + "','A','Paid')";
                    dBO.dboAlterDBOUnsafe(sSQL);
                }
            }
            foreach (var item in linesCancelled)
            {
                progressBar1.PerformStep();
                if (item.Length > 1)
                {
                    sSQL = "insert into fortegacontractadd(ContractNo, AddOrRemove, Status) Values('" + item + "','A','Cancelled')";
                    dBO.dboAlterDBOUnsafe(sSQL);
                }
            }
            foreach (var item in linesRemove)
            {
                progressBar1.PerformStep();
                if (item.Length > 1)
                {
                    sSQL = "insert into fortegacontractadd(ContractNo, AddOrRemove, Status) Values('" + item + "','R','Remove')";
                    dBO.dboAlterDBOUnsafe(sSQL);
                }
            }

            btnClear.Enabled = true;
            btnExcel.Enabled = true;
            btnTransmit.Enabled = true;
            btnUploadTransmittedFiles.Enabled = true;

            btnClaimsResetArch.Enabled = true;
            btnClaimsSaveArch.Enabled = true;
            btnResetContractArch.Enabled = true;
            btnSaveContractsArch.Enabled = true;

            btnResetClaimsAmTrust.Enabled = true;
            btnSaveClaimsAmtrust.Enabled = true;
            btnResetContractsAmTrust.Enabled = true;
            btnSaveContractsAmtrust.Enabled = true;

            btnResetClaimsOldRep.Enabled = true;
            btnSaveClaimsOldRep.Enabled = true;
            btnSaveContractsOldRep.Enabled = true;
            btnResetContractsOldRep.Enabled = true;

            btnClaimSaveFortega.Enabled = true;
            btnContractSaveFortega.Enabled = true;
            btnResetClaimsFortega.Enabled = true;
            btnResetContractsFortega.Enabled = true;

            MessageBox.Show("Save Complete");
        }
        private void btnResetClaimsFortega_Click(object sender, EventArgs e)
        {
            btnClear.Enabled = false;
            btnExcel.Enabled = false;
            btnTransmit.Enabled = false;
            btnUploadTransmittedFiles.Enabled = false;

            btnClaimsResetArch.Enabled = false;
            btnClaimsSaveArch.Enabled = false;
            btnResetContractArch.Enabled = false;
            btnSaveContractsArch.Enabled = false;

            btnResetClaimsAmTrust.Enabled = false;
            btnSaveClaimsAmtrust.Enabled = false;
            btnResetContractsAmTrust.Enabled = false;
            btnSaveContractsAmtrust.Enabled = false;

            btnResetClaimsOldRep.Enabled = false;
            btnSaveClaimsOldRep.Enabled = false;
            btnSaveContractsOldRep.Enabled = false;
            btnResetContractsOldRep.Enabled = false;

            btnClaimSaveFortega.Enabled = false;
            btnContractSaveFortega.Enabled = false;
            btnResetClaimsFortega.Enabled = false;
            btnResetContractsFortega.Enabled = false;

            string caption = "";
            var result = MessageBox.Show("Are you sure you want to clear the Fortega Claims?", caption, MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                CSharpDBO.CSharpDBO dBO = new CSharpDBO.CSharpDBO();
                dBO.dboOpen(path);
                string SQL = "truncate table fortegaclaimadd";
                dBO.dboAlterDBOUnsafe(SQL);
                MessageBox.Show("Saved Claims cleared");
                dBO.dboClose();
                rtbClaimsAddFortega.Clear();
                rtbClaimsRemoveFortega.Clear();
            }           

            btnClear.Enabled = true;
            btnExcel.Enabled = true;
            btnTransmit.Enabled = true;
            btnUploadTransmittedFiles.Enabled = true;

            btnClaimsResetArch.Enabled = true;
            btnClaimsSaveArch.Enabled = true;
            btnResetContractArch.Enabled = true;
            btnSaveContractsArch.Enabled = true;

            btnResetClaimsAmTrust.Enabled = true;
            btnSaveClaimsAmtrust.Enabled = true;
            btnResetContractsAmTrust.Enabled = true;
            btnSaveContractsAmtrust.Enabled = true;

            btnResetClaimsOldRep.Enabled = true;
            btnSaveClaimsOldRep.Enabled = true;
            btnSaveContractsOldRep.Enabled = true;
            btnResetContractsOldRep.Enabled = true;

            btnClaimSaveFortega.Enabled = true;
            btnContractSaveFortega.Enabled = true;
            btnResetClaimsFortega.Enabled = true;
            btnResetContractsFortega.Enabled = true;
        }
        private void btnClaimSaveFortega_Click(object sender, EventArgs e)
        {
            btnClear.Enabled = false;
            btnExcel.Enabled = false;
            btnTransmit.Enabled = false;
            btnUploadTransmittedFiles.Enabled = false;

            btnClaimsResetArch.Enabled = false;
            btnClaimsSaveArch.Enabled = false;
            btnResetContractArch.Enabled = false;
            btnSaveContractsArch.Enabled = false;

            btnResetClaimsAmTrust.Enabled = false;
            btnSaveClaimsAmtrust.Enabled = false;
            btnResetContractsAmTrust.Enabled = false;
            btnSaveContractsAmtrust.Enabled = false;

            btnResetClaimsOldRep.Enabled = false;
            btnSaveClaimsOldRep.Enabled = false;
            btnSaveContractsOldRep.Enabled = false;
            btnResetContractsOldRep.Enabled = false;

            btnClaimSaveFortega.Enabled = false;
            btnContractSaveFortega.Enabled = false;
            btnResetClaimsFortega.Enabled = false;
            btnResetContractsFortega.Enabled = false;

            CSharpDBO.CSharpDBO dBO = new CSharpDBO.CSharpDBO();
            dBO.dboOpen(path);
            string sSQL = "";
            var linesAdd = rtbClaimsAddFortega.Text.Split('\n').ToList();
            var linesRemove = rtbClaimsRemoveFortega.Text.Split('\n').ToList();

            int iCounter = linesAdd.Count + linesRemove.Count;

            lblProgressBar.Text = "Saving Fortega Claims";
            lblProgressBar.Refresh();
            progressBar1.Value = 0;
            progressBar1.Maximum = iCounter;
            progressBar1.Minimum = 0;
            progressBar1.Visible = true;
            progressBar1.Step = 1;

            if (linesAdd[0].Length > 1 || linesRemove[0].Length > 1)
            {
                sSQL = "truncate table fortegaclaimadd";
                dBO.dboAlterDBOUnsafe(sSQL);
            }
            foreach (var item in linesAdd)
            {
                progressBar1.PerformStep();
                if (item.Length > 1)
                {
                    sSQL = "insert into fortegaclaimadd(ClaimNo, AddOrRemove) Values('" + item + "','A')";
                    dBO.dboAlterDBOUnsafe(sSQL);
                }
            }
            foreach (var item in linesRemove)
            {
                progressBar1.PerformStep();
                if (item.Length > 1)
                {
                    sSQL = "insert into fortegaclaimadd(ClaimNo, AddOrRemove) Values('" + item + "','R')";
                    dBO.dboAlterDBOUnsafe(sSQL);
                }
            }

            btnClear.Enabled = true;
            btnExcel.Enabled = true;
            btnTransmit.Enabled = true;
            btnUploadTransmittedFiles.Enabled = true;

            btnClaimsResetArch.Enabled = true;
            btnClaimsSaveArch.Enabled = true;
            btnResetContractArch.Enabled = true;
            btnSaveContractsArch.Enabled = true;

            btnResetClaimsAmTrust.Enabled = true;
            btnSaveClaimsAmtrust.Enabled = true;
            btnResetContractsAmTrust.Enabled = true;
            btnSaveContractsAmtrust.Enabled = true;

            btnResetClaimsOldRep.Enabled = true;
            btnSaveClaimsOldRep.Enabled = true;
            btnSaveContractsOldRep.Enabled = true;
            btnResetContractsOldRep.Enabled = true;

            btnClaimSaveFortega.Enabled = true;
            btnContractSaveFortega.Enabled = true;
            btnResetClaimsFortega.Enabled = true;
            btnResetContractsFortega.Enabled = true;

            MessageBox.Show("Save Complete");
        }
        

    }
}

