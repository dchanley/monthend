﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Data.SqlTypes;


namespace CSharpDBO
{
    class CSharpDBO
    {
        SqlConnection conn = new SqlConnection();
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter dataAdapter = new SqlDataAdapter();
        DataTable dataTable = new DataTable();


        public CSharpDBO() { }
        public void dboOpen(string path)
        {
            /*The purpose of this method is to open a connection to a database*/
            conn = new SqlConnection(path);
            conn.Open();
            cmd.CommandType = System.Data.CommandType.Text;
        }
        public void dboClose()
        {
            /*The purpose of this method is to close a connection to a database*/
            conn.Close();
        }
        public DataTable dboInportInformation(string sqlCommand)
        {
        /*The purpose of this method is to be able to read out information from the database using a data table
         use a foreach loop in this format to read out the data
         use foreach (DataRow dataRow in DataTable.Rows) to read in data*/
        gotoHere:
            try
            {
                cmd.CommandText = sqlCommand;
                cmd.Connection = conn;
                cmd.CommandTimeout = 60000;
                dataAdapter = new SqlDataAdapter(cmd);
                dataTable = new DataTable();
                dataAdapter.Fill(dataTable);
            }
            catch (Exception ex)
            {
                string temp = ex.Message;
                if (temp.Contains("Restore"))
                {
                    System.Threading.Thread.Sleep(60000);
                    goto gotoHere;
                }
            }


            return dataTable;
        }
        public void dboAlterDBOUnsafe(string sqlCommand)
        {
            /*The purpose of this method is to execute queries. 
            Warning: Do not use this method if you are getting information from the user.
            Use AlterDBOSafe*/
            cmd.CommandText = sqlCommand;
            cmd.Connection = conn;
            cmd.CommandTimeout = 60000;
            cmd.ExecuteNonQuery();
        }

        public bool hasRows()
        {
            /*The Purpose of this method is to verify if there is information in a database after a query is executed. Make sure to use an Alter Method first*/

            bool hasRows = true;
            DataTableReader dataTableReader = new DataTableReader(dataTable);
            if (dataTableReader.HasRows)
            {
                hasRows = true;
            }
            else
            {
                hasRows = false;
            }


            return hasRows;
        }

        public void AlterDBOSafe(string[] QueryInformation, string[] dataToBeRead, SqlDbType[] dataType, string sqlCommand)
        {
            /*The purpose of this method is to execute queries using user information safely.  
            values is the Query information. dataToBeRead is the data to be inserted into the Database
            dataType is the sql data type
            sqlCommand should be formated as such:
            insert into RedShieldAdminTest.dbo.DatabaseTest(Fname, Lname, address) values(@Fname, @Lanme, @address)*/

            cmd.CommandText = sqlCommand;
            SqlParameter[] parameter = new SqlParameter[QueryInformation.Length];

            for (int i = 0; i < QueryInformation.Length; i++)
            {
                parameter[i] = new SqlParameter();
                parameter[i].SqlDbType = dataType[i];
                parameter[i].ParameterName = QueryInformation[i];
                parameter[i].Value = dataToBeRead[i];

            }
            foreach (SqlParameter sqlParameter in parameter)
            {
                cmd.Parameters.Add(sqlParameter);
            }
            cmd.CommandTimeout = 60000;
            cmd.ExecuteNonQuery();
            cmd.Parameters.Clear();

        }


    }
}
