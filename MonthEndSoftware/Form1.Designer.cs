﻿namespace MonthEndSoftware
{
    partial class MonthEnd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnExcel = new System.Windows.Forms.Button();
            this.btnTransmit = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.rtbRemoveContractAmTrust = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.chkAMT = new System.Windows.Forms.CheckBox();
            this.chkArch = new System.Windows.Forms.CheckBox();
            this.dtpStartDate = new System.Windows.Forms.DateTimePicker();
            this.dtpEndDate = new System.Windows.Forms.DateTimePicker();
            this.lblContract = new System.Windows.Forms.Label();
            this.lbCancel = new System.Windows.Forms.Label();
            this.lblClaim = new System.Windows.Forms.Label();
            this.lblOther = new System.Windows.Forms.Label();
            this.lblDealer = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.AmTrustContractTC = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.rtbContractsPaidAddAmTrust = new System.Windows.Forms.RichTextBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.rtbCancelledContractsAddAmTrust = new System.Windows.Forms.RichTextBox();
            this.btnSaveContractsAmtrust = new System.Windows.Forms.Button();
            this.btnResetContractsAmTrust = new System.Windows.Forms.Button();
            this.btnSaveClaimsAmtrust = new System.Windows.Forms.Button();
            this.btnResetClaimsAmTrust = new System.Windows.Forms.Button();
            this.rtbAddClaimsAmTrust = new System.Windows.Forms.RichTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.rtbRemoveClaimsAmTrust = new System.Windows.Forms.RichTextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btnClaimsSaveArch = new System.Windows.Forms.Button();
            this.btnClaimsResetArch = new System.Windows.Forms.Button();
            this.btnSaveContractsArch = new System.Windows.Forms.Button();
            this.btnResetContractArch = new System.Windows.Forms.Button();
            this.tabControl3 = new System.Windows.Forms.TabControl();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.rtbPaidContractsArch = new System.Windows.Forms.RichTextBox();
            this.tabPage9 = new System.Windows.Forms.TabPage();
            this.rtbCancelledContractsArch = new System.Windows.Forms.RichTextBox();
            this.rtbAddClaimsArch = new System.Windows.Forms.RichTextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.rtbRemoveClaimsArch = new System.Windows.Forms.RichTextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.rtbRemoveContractArch = new System.Windows.Forms.RichTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.rtbContractPaidAddOldRep = new System.Windows.Forms.RichTextBox();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.rtbContractCancelledAddOldRep = new System.Windows.Forms.RichTextBox();
            this.btnSaveContractsOldRep = new System.Windows.Forms.Button();
            this.btnResetContractsOldRep = new System.Windows.Forms.Button();
            this.btnSaveClaimsOldRep = new System.Windows.Forms.Button();
            this.btnResetClaimsOldRep = new System.Windows.Forms.Button();
            this.rtbClaimAddOldRep = new System.Windows.Forms.RichTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.rtbClaimRemoveOldRep = new System.Windows.Forms.RichTextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.rtbContractRemoveOldRep = new System.Windows.Forms.RichTextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.tabPage10 = new System.Windows.Forms.TabPage();
            this.tabControl4 = new System.Windows.Forms.TabControl();
            this.tabPage11 = new System.Windows.Forms.TabPage();
            this.rtbPaidContractsFortega = new System.Windows.Forms.RichTextBox();
            this.tabPage12 = new System.Windows.Forms.TabPage();
            this.rtbCancelContractsFortega = new System.Windows.Forms.RichTextBox();
            this.btnContractSaveFortega = new System.Windows.Forms.Button();
            this.btnResetContractsFortega = new System.Windows.Forms.Button();
            this.btnClaimSaveFortega = new System.Windows.Forms.Button();
            this.btnResetClaimsFortega = new System.Windows.Forms.Button();
            this.rtbClaimsAddFortega = new System.Windows.Forms.RichTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.rtbClaimsRemoveFortega = new System.Windows.Forms.RichTextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.rtbContractsRemoveFortega = new System.Windows.Forms.RichTextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.lblProgressBar = new System.Windows.Forms.Label();
            this.chkInception = new System.Windows.Forms.CheckBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnUploadTransmittedFiles = new System.Windows.Forms.Button();
            this.chkOldRep = new System.Windows.Forms.CheckBox();
            this.chkFortega = new System.Windows.Forms.CheckBox();
            this.chkOnlyAdded = new System.Windows.Forms.CheckBox();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.AmTrustContractTC.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabControl3.SuspendLayout();
            this.tabPage8.SuspendLayout();
            this.tabPage9.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.tabPage7.SuspendLayout();
            this.tabPage10.SuspendLayout();
            this.tabControl4.SuspendLayout();
            this.tabPage11.SuspendLayout();
            this.tabPage12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnExcel
            // 
            this.btnExcel.Location = new System.Drawing.Point(6, 146);
            this.btnExcel.Name = "btnExcel";
            this.btnExcel.Size = new System.Drawing.Size(206, 23);
            this.btnExcel.TabIndex = 6;
            this.btnExcel.Text = "Excel Report";
            this.btnExcel.UseVisualStyleBackColor = true;
            this.btnExcel.Click += new System.EventHandler(this.BtnExcel_Click);
            // 
            // btnTransmit
            // 
            this.btnTransmit.Location = new System.Drawing.Point(6, 175);
            this.btnTransmit.Name = "btnTransmit";
            this.btnTransmit.Size = new System.Drawing.Size(206, 23);
            this.btnTransmit.TabIndex = 7;
            this.btnTransmit.Text = "Transmittle Files";
            this.btnTransmit.UseVisualStyleBackColor = true;
            this.btnTransmit.Click += new System.EventHandler(this.BtnTransmit_Click);
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(3, 233);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(206, 23);
            this.btnClear.TabIndex = 8;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.BtnClear_Click);
            // 
            // rtbRemoveContractAmTrust
            // 
            this.rtbRemoveContractAmTrust.Location = new System.Drawing.Point(224, 31);
            this.rtbRemoveContractAmTrust.Name = "rtbRemoveContractAmTrust";
            this.rtbRemoveContractAmTrust.Size = new System.Drawing.Size(190, 169);
            this.rtbRemoveContractAmTrust.TabIndex = 10;
            this.rtbRemoveContractAmTrust.Text = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Start Date";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "End Date";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(221, 8);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(111, 13);
            this.label4.TabIndex = 14;
            this.label4.Text = "Contracts To Remove";
            // 
            // chkAMT
            // 
            this.chkAMT.AutoSize = true;
            this.chkAMT.Location = new System.Drawing.Point(15, 88);
            this.chkAMT.Name = "chkAMT";
            this.chkAMT.Size = new System.Drawing.Size(65, 17);
            this.chkAMT.TabIndex = 34;
            this.chkAMT.Text = "AmTrust";
            this.chkAMT.UseVisualStyleBackColor = true;
            // 
            // chkArch
            // 
            this.chkArch.AutoSize = true;
            this.chkArch.Location = new System.Drawing.Point(132, 88);
            this.chkArch.Name = "chkArch";
            this.chkArch.Size = new System.Drawing.Size(48, 17);
            this.chkArch.TabIndex = 35;
            this.chkArch.Text = "Arch";
            this.chkArch.UseVisualStyleBackColor = true;
            // 
            // dtpStartDate
            // 
            this.dtpStartDate.Location = new System.Drawing.Point(64, 12);
            this.dtpStartDate.Name = "dtpStartDate";
            this.dtpStartDate.Size = new System.Drawing.Size(200, 20);
            this.dtpStartDate.TabIndex = 36;
            // 
            // dtpEndDate
            // 
            this.dtpEndDate.Location = new System.Drawing.Point(64, 48);
            this.dtpEndDate.Name = "dtpEndDate";
            this.dtpEndDate.Size = new System.Drawing.Size(200, 20);
            this.dtpEndDate.TabIndex = 37;
            // 
            // lblContract
            // 
            this.lblContract.AutoSize = true;
            this.lblContract.Location = new System.Drawing.Point(12, 267);
            this.lblContract.Name = "lblContract";
            this.lblContract.Size = new System.Drawing.Size(74, 13);
            this.lblContract.TabIndex = 40;
            this.lblContract.Text = "Paid Contract:";
            // 
            // lbCancel
            // 
            this.lbCancel.AutoSize = true;
            this.lbCancel.Location = new System.Drawing.Point(12, 286);
            this.lbCancel.Name = "lbCancel";
            this.lbCancel.Size = new System.Drawing.Size(91, 13);
            this.lbCancel.TabIndex = 41;
            this.lbCancel.Text = "Cancel Contracts:";
            // 
            // lblClaim
            // 
            this.lblClaim.AutoSize = true;
            this.lblClaim.Location = new System.Drawing.Point(12, 305);
            this.lblClaim.Name = "lblClaim";
            this.lblClaim.Size = new System.Drawing.Size(40, 13);
            this.lblClaim.TabIndex = 42;
            this.lblClaim.Text = "Claims:";
            // 
            // lblOther
            // 
            this.lblOther.AutoSize = true;
            this.lblOther.Location = new System.Drawing.Point(144, 286);
            this.lblOther.Name = "lblOther";
            this.lblOther.Size = new System.Drawing.Size(36, 13);
            this.lblOther.TabIndex = 47;
            this.lblOther.Text = "Other:";
            // 
            // lblDealer
            // 
            this.lblDealer.AutoSize = true;
            this.lblDealer.Location = new System.Drawing.Point(144, 267);
            this.lblDealer.Name = "lblDealer";
            this.lblDealer.Size = new System.Drawing.Size(41, 13);
            this.lblDealer.TabIndex = 46;
            this.lblDealer.Text = "Dealer:";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage10);
            this.tabControl1.Location = new System.Drawing.Point(284, 25);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(443, 504);
            this.tabControl1.TabIndex = 49;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.AmTrustContractTC);
            this.tabPage1.Controls.Add(this.btnSaveContractsAmtrust);
            this.tabPage1.Controls.Add(this.btnResetContractsAmTrust);
            this.tabPage1.Controls.Add(this.btnSaveClaimsAmtrust);
            this.tabPage1.Controls.Add(this.btnResetClaimsAmTrust);
            this.tabPage1.Controls.Add(this.rtbAddClaimsAmTrust);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.rtbRemoveClaimsAmTrust);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.rtbRemoveContractAmTrust);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabPage1.Size = new System.Drawing.Size(435, 478);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "AmTrust";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // AmTrustContractTC
            // 
            this.AmTrustContractTC.Controls.Add(this.tabPage3);
            this.AmTrustContractTC.Controls.Add(this.tabPage4);
            this.AmTrustContractTC.Location = new System.Drawing.Point(6, 6);
            this.AmTrustContractTC.Name = "AmTrustContractTC";
            this.AmTrustContractTC.SelectedIndex = 0;
            this.AmTrustContractTC.Size = new System.Drawing.Size(212, 205);
            this.AmTrustContractTC.TabIndex = 56;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.rtbContractsPaidAddAmTrust);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabPage3.Size = new System.Drawing.Size(204, 179);
            this.tabPage3.TabIndex = 0;
            this.tabPage3.Text = "Paid Contracts";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // rtbContractsPaidAddAmTrust
            // 
            this.rtbContractsPaidAddAmTrust.Location = new System.Drawing.Point(6, 3);
            this.rtbContractsPaidAddAmTrust.Name = "rtbContractsPaidAddAmTrust";
            this.rtbContractsPaidAddAmTrust.Size = new System.Drawing.Size(192, 168);
            this.rtbContractsPaidAddAmTrust.TabIndex = 11;
            this.rtbContractsPaidAddAmTrust.Text = "";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.rtbCancelledContractsAddAmTrust);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabPage4.Size = new System.Drawing.Size(204, 179);
            this.tabPage4.TabIndex = 1;
            this.tabPage4.Text = "Cancelled Contracts";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // rtbCancelledContractsAddAmTrust
            // 
            this.rtbCancelledContractsAddAmTrust.Location = new System.Drawing.Point(6, 3);
            this.rtbCancelledContractsAddAmTrust.Name = "rtbCancelledContractsAddAmTrust";
            this.rtbCancelledContractsAddAmTrust.Size = new System.Drawing.Size(192, 168);
            this.rtbCancelledContractsAddAmTrust.TabIndex = 12;
            this.rtbCancelledContractsAddAmTrust.Text = "";
            // 
            // btnSaveContractsAmtrust
            // 
            this.btnSaveContractsAmtrust.Location = new System.Drawing.Point(90, 217);
            this.btnSaveContractsAmtrust.Name = "btnSaveContractsAmtrust";
            this.btnSaveContractsAmtrust.Size = new System.Drawing.Size(75, 23);
            this.btnSaveContractsAmtrust.TabIndex = 22;
            this.btnSaveContractsAmtrust.Text = "Save";
            this.btnSaveContractsAmtrust.UseVisualStyleBackColor = true;
            this.btnSaveContractsAmtrust.Click += new System.EventHandler(this.btnSaveContractsAmtrust_Click);
            // 
            // btnResetContractsAmTrust
            // 
            this.btnResetContractsAmTrust.Location = new System.Drawing.Point(9, 217);
            this.btnResetContractsAmTrust.Name = "btnResetContractsAmTrust";
            this.btnResetContractsAmTrust.Size = new System.Drawing.Size(75, 23);
            this.btnResetContractsAmTrust.TabIndex = 21;
            this.btnResetContractsAmTrust.Text = "Reset";
            this.btnResetContractsAmTrust.UseVisualStyleBackColor = true;
            this.btnResetContractsAmTrust.Click += new System.EventHandler(this.btnResetContractsAmTrust_Click);
            // 
            // btnSaveClaimsAmtrust
            // 
            this.btnSaveClaimsAmtrust.Location = new System.Drawing.Point(90, 436);
            this.btnSaveClaimsAmtrust.Name = "btnSaveClaimsAmtrust";
            this.btnSaveClaimsAmtrust.Size = new System.Drawing.Size(75, 23);
            this.btnSaveClaimsAmtrust.TabIndex = 20;
            this.btnSaveClaimsAmtrust.Text = "Save";
            this.btnSaveClaimsAmtrust.UseVisualStyleBackColor = true;
            this.btnSaveClaimsAmtrust.Click += new System.EventHandler(this.btnSaveClaimsAmtrust_Click);
            // 
            // btnResetClaimsAmTrust
            // 
            this.btnResetClaimsAmTrust.Location = new System.Drawing.Point(9, 436);
            this.btnResetClaimsAmTrust.Name = "btnResetClaimsAmTrust";
            this.btnResetClaimsAmTrust.Size = new System.Drawing.Size(75, 23);
            this.btnResetClaimsAmTrust.TabIndex = 19;
            this.btnResetClaimsAmTrust.Text = "Reset";
            this.btnResetClaimsAmTrust.UseVisualStyleBackColor = true;
            this.btnResetClaimsAmTrust.Click += new System.EventHandler(this.btnResetClaimsAmTrust_Click);
            // 
            // rtbAddClaimsAmTrust
            // 
            this.rtbAddClaimsAmTrust.Location = new System.Drawing.Point(9, 261);
            this.rtbAddClaimsAmTrust.Name = "rtbAddClaimsAmTrust";
            this.rtbAddClaimsAmTrust.Size = new System.Drawing.Size(190, 169);
            this.rtbAddClaimsAmTrust.TabIndex = 15;
            this.rtbAddClaimsAmTrust.Text = "";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 242);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(71, 13);
            this.label7.TabIndex = 17;
            this.label7.Text = "Claims to Add";
            // 
            // rtbRemoveClaimsAmTrust
            // 
            this.rtbRemoveClaimsAmTrust.Location = new System.Drawing.Point(224, 261);
            this.rtbRemoveClaimsAmTrust.Name = "rtbRemoveClaimsAmTrust";
            this.rtbRemoveClaimsAmTrust.Size = new System.Drawing.Size(190, 169);
            this.rtbRemoveClaimsAmTrust.TabIndex = 16;
            this.rtbRemoveClaimsAmTrust.Text = "";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(221, 242);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(96, 13);
            this.label8.TabIndex = 18;
            this.label8.Text = "Claims To Remove";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.btnClaimsSaveArch);
            this.tabPage2.Controls.Add(this.btnClaimsResetArch);
            this.tabPage2.Controls.Add(this.btnSaveContractsArch);
            this.tabPage2.Controls.Add(this.btnResetContractArch);
            this.tabPage2.Controls.Add(this.tabControl3);
            this.tabPage2.Controls.Add(this.rtbAddClaimsArch);
            this.tabPage2.Controls.Add(this.label9);
            this.tabPage2.Controls.Add(this.rtbRemoveClaimsArch);
            this.tabPage2.Controls.Add(this.label10);
            this.tabPage2.Controls.Add(this.rtbRemoveContractArch);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabPage2.Size = new System.Drawing.Size(435, 478);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Arch";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // btnClaimsSaveArch
            // 
            this.btnClaimsSaveArch.Location = new System.Drawing.Point(89, 436);
            this.btnClaimsSaveArch.Name = "btnClaimsSaveArch";
            this.btnClaimsSaveArch.Size = new System.Drawing.Size(75, 23);
            this.btnClaimsSaveArch.TabIndex = 61;
            this.btnClaimsSaveArch.Text = "Save";
            this.btnClaimsSaveArch.UseVisualStyleBackColor = true;
            this.btnClaimsSaveArch.Click += new System.EventHandler(this.btnClaimsSaveArch_Click);
            // 
            // btnClaimsResetArch
            // 
            this.btnClaimsResetArch.Location = new System.Drawing.Point(8, 436);
            this.btnClaimsResetArch.Name = "btnClaimsResetArch";
            this.btnClaimsResetArch.Size = new System.Drawing.Size(75, 23);
            this.btnClaimsResetArch.TabIndex = 60;
            this.btnClaimsResetArch.Text = "Reset";
            this.btnClaimsResetArch.UseVisualStyleBackColor = true;
            this.btnClaimsResetArch.Click += new System.EventHandler(this.btnClaimsResetArch_Click);
            // 
            // btnSaveContractsArch
            // 
            this.btnSaveContractsArch.Location = new System.Drawing.Point(91, 217);
            this.btnSaveContractsArch.Name = "btnSaveContractsArch";
            this.btnSaveContractsArch.Size = new System.Drawing.Size(75, 23);
            this.btnSaveContractsArch.TabIndex = 59;
            this.btnSaveContractsArch.Text = "Save";
            this.btnSaveContractsArch.UseVisualStyleBackColor = true;
            this.btnSaveContractsArch.Click += new System.EventHandler(this.btnSaveContractsArch_Click);
            // 
            // btnResetContractArch
            // 
            this.btnResetContractArch.Location = new System.Drawing.Point(10, 217);
            this.btnResetContractArch.Name = "btnResetContractArch";
            this.btnResetContractArch.Size = new System.Drawing.Size(75, 23);
            this.btnResetContractArch.TabIndex = 58;
            this.btnResetContractArch.Text = "Reset";
            this.btnResetContractArch.UseVisualStyleBackColor = true;
            this.btnResetContractArch.Click += new System.EventHandler(this.btnResetContractArch_Click);
            // 
            // tabControl3
            // 
            this.tabControl3.Controls.Add(this.tabPage8);
            this.tabControl3.Controls.Add(this.tabPage9);
            this.tabControl3.Location = new System.Drawing.Point(6, 6);
            this.tabControl3.Name = "tabControl3";
            this.tabControl3.SelectedIndex = 0;
            this.tabControl3.Size = new System.Drawing.Size(212, 205);
            this.tabControl3.TabIndex = 57;
            // 
            // tabPage8
            // 
            this.tabPage8.Controls.Add(this.rtbPaidContractsArch);
            this.tabPage8.Location = new System.Drawing.Point(4, 22);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabPage8.Size = new System.Drawing.Size(204, 179);
            this.tabPage8.TabIndex = 0;
            this.tabPage8.Text = "Paid Contracts";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // rtbPaidContractsArch
            // 
            this.rtbPaidContractsArch.Location = new System.Drawing.Point(6, 3);
            this.rtbPaidContractsArch.Name = "rtbPaidContractsArch";
            this.rtbPaidContractsArch.Size = new System.Drawing.Size(192, 168);
            this.rtbPaidContractsArch.TabIndex = 11;
            this.rtbPaidContractsArch.Text = "";
            // 
            // tabPage9
            // 
            this.tabPage9.Controls.Add(this.rtbCancelledContractsArch);
            this.tabPage9.Location = new System.Drawing.Point(4, 22);
            this.tabPage9.Name = "tabPage9";
            this.tabPage9.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabPage9.Size = new System.Drawing.Size(204, 179);
            this.tabPage9.TabIndex = 1;
            this.tabPage9.Text = "Cancelled Contracts";
            this.tabPage9.UseVisualStyleBackColor = true;
            // 
            // rtbCancelledContractsArch
            // 
            this.rtbCancelledContractsArch.Location = new System.Drawing.Point(6, 3);
            this.rtbCancelledContractsArch.Name = "rtbCancelledContractsArch";
            this.rtbCancelledContractsArch.Size = new System.Drawing.Size(192, 168);
            this.rtbCancelledContractsArch.TabIndex = 12;
            this.rtbCancelledContractsArch.Text = "";
            // 
            // rtbAddClaimsArch
            // 
            this.rtbAddClaimsArch.Location = new System.Drawing.Point(9, 261);
            this.rtbAddClaimsArch.Name = "rtbAddClaimsArch";
            this.rtbAddClaimsArch.Size = new System.Drawing.Size(190, 169);
            this.rtbAddClaimsArch.TabIndex = 19;
            this.rtbAddClaimsArch.Text = "";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 242);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(71, 13);
            this.label9.TabIndex = 21;
            this.label9.Text = "Claims to Add";
            // 
            // rtbRemoveClaimsArch
            // 
            this.rtbRemoveClaimsArch.Location = new System.Drawing.Point(224, 261);
            this.rtbRemoveClaimsArch.Name = "rtbRemoveClaimsArch";
            this.rtbRemoveClaimsArch.Size = new System.Drawing.Size(190, 169);
            this.rtbRemoveClaimsArch.TabIndex = 20;
            this.rtbRemoveClaimsArch.Text = "";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(221, 242);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(96, 13);
            this.label10.TabIndex = 22;
            this.label10.Text = "Claims To Remove";
            // 
            // rtbRemoveContractArch
            // 
            this.rtbRemoveContractArch.Location = new System.Drawing.Point(224, 31);
            this.rtbRemoveContractArch.Name = "rtbRemoveContractArch";
            this.rtbRemoveContractArch.Size = new System.Drawing.Size(190, 169);
            this.rtbRemoveContractArch.TabIndex = 16;
            this.rtbRemoveContractArch.Text = "";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(221, 8);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(111, 13);
            this.label6.TabIndex = 18;
            this.label6.Text = "Contracts To Remove";
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.tabControl2);
            this.tabPage5.Controls.Add(this.btnSaveContractsOldRep);
            this.tabPage5.Controls.Add(this.btnResetContractsOldRep);
            this.tabPage5.Controls.Add(this.btnSaveClaimsOldRep);
            this.tabPage5.Controls.Add(this.btnResetClaimsOldRep);
            this.tabPage5.Controls.Add(this.rtbClaimAddOldRep);
            this.tabPage5.Controls.Add(this.label3);
            this.tabPage5.Controls.Add(this.rtbClaimRemoveOldRep);
            this.tabPage5.Controls.Add(this.label11);
            this.tabPage5.Controls.Add(this.rtbContractRemoveOldRep);
            this.tabPage5.Controls.Add(this.label12);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabPage5.Size = new System.Drawing.Size(435, 478);
            this.tabPage5.TabIndex = 2;
            this.tabPage5.Text = "Old Republic";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.tabPage6);
            this.tabControl2.Controls.Add(this.tabPage7);
            this.tabControl2.Location = new System.Drawing.Point(6, 6);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(212, 205);
            this.tabControl2.TabIndex = 67;
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.rtbContractPaidAddOldRep);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabPage6.Size = new System.Drawing.Size(204, 179);
            this.tabPage6.TabIndex = 0;
            this.tabPage6.Text = "Paid Contracts";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // rtbContractPaidAddOldRep
            // 
            this.rtbContractPaidAddOldRep.Location = new System.Drawing.Point(6, 3);
            this.rtbContractPaidAddOldRep.Name = "rtbContractPaidAddOldRep";
            this.rtbContractPaidAddOldRep.Size = new System.Drawing.Size(192, 168);
            this.rtbContractPaidAddOldRep.TabIndex = 11;
            this.rtbContractPaidAddOldRep.Text = "";
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.rtbContractCancelledAddOldRep);
            this.tabPage7.Location = new System.Drawing.Point(4, 22);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabPage7.Size = new System.Drawing.Size(204, 179);
            this.tabPage7.TabIndex = 1;
            this.tabPage7.Text = "Cancelled Contracts";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // rtbContractCancelledAddOldRep
            // 
            this.rtbContractCancelledAddOldRep.Location = new System.Drawing.Point(6, 3);
            this.rtbContractCancelledAddOldRep.Name = "rtbContractCancelledAddOldRep";
            this.rtbContractCancelledAddOldRep.Size = new System.Drawing.Size(192, 168);
            this.rtbContractCancelledAddOldRep.TabIndex = 12;
            this.rtbContractCancelledAddOldRep.Text = "";
            // 
            // btnSaveContractsOldRep
            // 
            this.btnSaveContractsOldRep.Location = new System.Drawing.Point(90, 217);
            this.btnSaveContractsOldRep.Name = "btnSaveContractsOldRep";
            this.btnSaveContractsOldRep.Size = new System.Drawing.Size(75, 23);
            this.btnSaveContractsOldRep.TabIndex = 66;
            this.btnSaveContractsOldRep.Text = "Save";
            this.btnSaveContractsOldRep.UseVisualStyleBackColor = true;
            this.btnSaveContractsOldRep.Click += new System.EventHandler(this.btnSaveContractsOldRep_Click);
            // 
            // btnResetContractsOldRep
            // 
            this.btnResetContractsOldRep.Location = new System.Drawing.Point(9, 217);
            this.btnResetContractsOldRep.Name = "btnResetContractsOldRep";
            this.btnResetContractsOldRep.Size = new System.Drawing.Size(75, 23);
            this.btnResetContractsOldRep.TabIndex = 65;
            this.btnResetContractsOldRep.Text = "Reset";
            this.btnResetContractsOldRep.UseVisualStyleBackColor = true;
            this.btnResetContractsOldRep.Click += new System.EventHandler(this.btnResetContractsOldRep_Click);
            // 
            // btnSaveClaimsOldRep
            // 
            this.btnSaveClaimsOldRep.Location = new System.Drawing.Point(90, 436);
            this.btnSaveClaimsOldRep.Name = "btnSaveClaimsOldRep";
            this.btnSaveClaimsOldRep.Size = new System.Drawing.Size(75, 23);
            this.btnSaveClaimsOldRep.TabIndex = 64;
            this.btnSaveClaimsOldRep.Text = "Save";
            this.btnSaveClaimsOldRep.UseVisualStyleBackColor = true;
            this.btnSaveClaimsOldRep.Click += new System.EventHandler(this.btnSaveClaimsOldRep_Click);
            // 
            // btnResetClaimsOldRep
            // 
            this.btnResetClaimsOldRep.Location = new System.Drawing.Point(9, 436);
            this.btnResetClaimsOldRep.Name = "btnResetClaimsOldRep";
            this.btnResetClaimsOldRep.Size = new System.Drawing.Size(75, 23);
            this.btnResetClaimsOldRep.TabIndex = 63;
            this.btnResetClaimsOldRep.Text = "Reset";
            this.btnResetClaimsOldRep.UseVisualStyleBackColor = true;
            this.btnResetClaimsOldRep.Click += new System.EventHandler(this.btnResetClaimsOldRep_Click);
            // 
            // rtbClaimAddOldRep
            // 
            this.rtbClaimAddOldRep.Location = new System.Drawing.Point(9, 261);
            this.rtbClaimAddOldRep.Name = "rtbClaimAddOldRep";
            this.rtbClaimAddOldRep.Size = new System.Drawing.Size(190, 169);
            this.rtbClaimAddOldRep.TabIndex = 59;
            this.rtbClaimAddOldRep.Text = "";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 242);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 13);
            this.label3.TabIndex = 61;
            this.label3.Text = "Claims to Add";
            // 
            // rtbClaimRemoveOldRep
            // 
            this.rtbClaimRemoveOldRep.Location = new System.Drawing.Point(224, 261);
            this.rtbClaimRemoveOldRep.Name = "rtbClaimRemoveOldRep";
            this.rtbClaimRemoveOldRep.Size = new System.Drawing.Size(190, 169);
            this.rtbClaimRemoveOldRep.TabIndex = 60;
            this.rtbClaimRemoveOldRep.Text = "";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(221, 242);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(96, 13);
            this.label11.TabIndex = 62;
            this.label11.Text = "Claims To Remove";
            // 
            // rtbContractRemoveOldRep
            // 
            this.rtbContractRemoveOldRep.Location = new System.Drawing.Point(224, 31);
            this.rtbContractRemoveOldRep.Name = "rtbContractRemoveOldRep";
            this.rtbContractRemoveOldRep.Size = new System.Drawing.Size(190, 169);
            this.rtbContractRemoveOldRep.TabIndex = 57;
            this.rtbContractRemoveOldRep.Text = "";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(221, 8);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(111, 13);
            this.label12.TabIndex = 58;
            this.label12.Text = "Contracts To Remove";
            // 
            // tabPage10
            // 
            this.tabPage10.Controls.Add(this.tabControl4);
            this.tabPage10.Controls.Add(this.btnContractSaveFortega);
            this.tabPage10.Controls.Add(this.btnResetContractsFortega);
            this.tabPage10.Controls.Add(this.btnClaimSaveFortega);
            this.tabPage10.Controls.Add(this.btnResetClaimsFortega);
            this.tabPage10.Controls.Add(this.rtbClaimsAddFortega);
            this.tabPage10.Controls.Add(this.label5);
            this.tabPage10.Controls.Add(this.rtbClaimsRemoveFortega);
            this.tabPage10.Controls.Add(this.label13);
            this.tabPage10.Controls.Add(this.rtbContractsRemoveFortega);
            this.tabPage10.Controls.Add(this.label14);
            this.tabPage10.Location = new System.Drawing.Point(4, 22);
            this.tabPage10.Name = "tabPage10";
            this.tabPage10.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabPage10.Size = new System.Drawing.Size(435, 478);
            this.tabPage10.TabIndex = 3;
            this.tabPage10.Text = "Fortega";
            this.tabPage10.UseVisualStyleBackColor = true;
            // 
            // tabControl4
            // 
            this.tabControl4.Controls.Add(this.tabPage11);
            this.tabControl4.Controls.Add(this.tabPage12);
            this.tabControl4.Location = new System.Drawing.Point(6, 6);
            this.tabControl4.Name = "tabControl4";
            this.tabControl4.SelectedIndex = 0;
            this.tabControl4.Size = new System.Drawing.Size(212, 205);
            this.tabControl4.TabIndex = 78;
            // 
            // tabPage11
            // 
            this.tabPage11.Controls.Add(this.rtbPaidContractsFortega);
            this.tabPage11.Location = new System.Drawing.Point(4, 22);
            this.tabPage11.Name = "tabPage11";
            this.tabPage11.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabPage11.Size = new System.Drawing.Size(204, 179);
            this.tabPage11.TabIndex = 0;
            this.tabPage11.Text = "Paid Contracts";
            this.tabPage11.UseVisualStyleBackColor = true;
            // 
            // rtbPaidContractsFortega
            // 
            this.rtbPaidContractsFortega.Location = new System.Drawing.Point(6, 3);
            this.rtbPaidContractsFortega.Name = "rtbPaidContractsFortega";
            this.rtbPaidContractsFortega.Size = new System.Drawing.Size(192, 168);
            this.rtbPaidContractsFortega.TabIndex = 11;
            this.rtbPaidContractsFortega.Text = "";
            // 
            // tabPage12
            // 
            this.tabPage12.Controls.Add(this.rtbCancelContractsFortega);
            this.tabPage12.Location = new System.Drawing.Point(4, 22);
            this.tabPage12.Name = "tabPage12";
            this.tabPage12.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.tabPage12.Size = new System.Drawing.Size(204, 179);
            this.tabPage12.TabIndex = 1;
            this.tabPage12.Text = "Cancelled Contracts";
            this.tabPage12.UseVisualStyleBackColor = true;
            // 
            // rtbCancelContractsFortega
            // 
            this.rtbCancelContractsFortega.Location = new System.Drawing.Point(6, 3);
            this.rtbCancelContractsFortega.Name = "rtbCancelContractsFortega";
            this.rtbCancelContractsFortega.Size = new System.Drawing.Size(192, 168);
            this.rtbCancelContractsFortega.TabIndex = 12;
            this.rtbCancelContractsFortega.Text = "";
            // 
            // btnContractSaveFortega
            // 
            this.btnContractSaveFortega.Location = new System.Drawing.Point(90, 217);
            this.btnContractSaveFortega.Name = "btnContractSaveFortega";
            this.btnContractSaveFortega.Size = new System.Drawing.Size(75, 23);
            this.btnContractSaveFortega.TabIndex = 77;
            this.btnContractSaveFortega.Text = "Save";
            this.btnContractSaveFortega.UseVisualStyleBackColor = true;
            this.btnContractSaveFortega.Click += new System.EventHandler(this.btnContractSaveFortega_Click);
            // 
            // btnResetContractsFortega
            // 
            this.btnResetContractsFortega.Location = new System.Drawing.Point(9, 217);
            this.btnResetContractsFortega.Name = "btnResetContractsFortega";
            this.btnResetContractsFortega.Size = new System.Drawing.Size(75, 23);
            this.btnResetContractsFortega.TabIndex = 76;
            this.btnResetContractsFortega.Text = "Reset";
            this.btnResetContractsFortega.UseVisualStyleBackColor = true;
            this.btnResetContractsFortega.Click += new System.EventHandler(this.btnResetContractsFortega_Click);
            // 
            // btnClaimSaveFortega
            // 
            this.btnClaimSaveFortega.Location = new System.Drawing.Point(90, 436);
            this.btnClaimSaveFortega.Name = "btnClaimSaveFortega";
            this.btnClaimSaveFortega.Size = new System.Drawing.Size(75, 23);
            this.btnClaimSaveFortega.TabIndex = 75;
            this.btnClaimSaveFortega.Text = "Save";
            this.btnClaimSaveFortega.UseVisualStyleBackColor = true;
            this.btnClaimSaveFortega.Click += new System.EventHandler(this.btnClaimSaveFortega_Click);
            // 
            // btnResetClaimsFortega
            // 
            this.btnResetClaimsFortega.Location = new System.Drawing.Point(9, 436);
            this.btnResetClaimsFortega.Name = "btnResetClaimsFortega";
            this.btnResetClaimsFortega.Size = new System.Drawing.Size(75, 23);
            this.btnResetClaimsFortega.TabIndex = 74;
            this.btnResetClaimsFortega.Text = "Reset";
            this.btnResetClaimsFortega.UseVisualStyleBackColor = true;
            this.btnResetClaimsFortega.Click += new System.EventHandler(this.btnResetClaimsFortega_Click);
            // 
            // rtbClaimsAddFortega
            // 
            this.rtbClaimsAddFortega.Location = new System.Drawing.Point(9, 261);
            this.rtbClaimsAddFortega.Name = "rtbClaimsAddFortega";
            this.rtbClaimsAddFortega.Size = new System.Drawing.Size(190, 169);
            this.rtbClaimsAddFortega.TabIndex = 70;
            this.rtbClaimsAddFortega.Text = "";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 242);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 13);
            this.label5.TabIndex = 72;
            this.label5.Text = "Claims to Add";
            // 
            // rtbClaimsRemoveFortega
            // 
            this.rtbClaimsRemoveFortega.Location = new System.Drawing.Point(224, 261);
            this.rtbClaimsRemoveFortega.Name = "rtbClaimsRemoveFortega";
            this.rtbClaimsRemoveFortega.Size = new System.Drawing.Size(190, 169);
            this.rtbClaimsRemoveFortega.TabIndex = 71;
            this.rtbClaimsRemoveFortega.Text = "";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(221, 242);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(96, 13);
            this.label13.TabIndex = 73;
            this.label13.Text = "Claims To Remove";
            // 
            // rtbContractsRemoveFortega
            // 
            this.rtbContractsRemoveFortega.Location = new System.Drawing.Point(224, 31);
            this.rtbContractsRemoveFortega.Name = "rtbContractsRemoveFortega";
            this.rtbContractsRemoveFortega.Size = new System.Drawing.Size(190, 169);
            this.rtbContractsRemoveFortega.TabIndex = 68;
            this.rtbContractsRemoveFortega.Text = "";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(221, 8);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(111, 13);
            this.label14.TabIndex = 69;
            this.label14.Text = "Contracts To Remove";
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(6, 342);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(249, 23);
            this.progressBar1.TabIndex = 50;
            // 
            // lblProgressBar
            // 
            this.lblProgressBar.AutoSize = true;
            this.lblProgressBar.Location = new System.Drawing.Point(6, 326);
            this.lblProgressBar.Name = "lblProgressBar";
            this.lblProgressBar.Size = new System.Drawing.Size(10, 13);
            this.lblProgressBar.TabIndex = 51;
            this.lblProgressBar.Text = " ";
            // 
            // chkInception
            // 
            this.chkInception.AutoSize = true;
            this.chkInception.Location = new System.Drawing.Point(284, 8);
            this.chkInception.Name = "chkInception";
            this.chkInception.Size = new System.Drawing.Size(70, 17);
            this.chkInception.TabIndex = 52;
            this.chkInception.Text = "Inception";
            this.chkInception.UseVisualStyleBackColor = true;
            this.chkInception.CheckedChanged += new System.EventHandler(this.ChkInception_CheckedChanged);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::MonthEndSoftware.Properties.Resources.Veritas_Icon;
            this.pictureBox1.Location = new System.Drawing.Point(28, 371);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(181, 185);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 54;
            this.pictureBox1.TabStop = false;
            // 
            // btnUploadTransmittedFiles
            // 
            this.btnUploadTransmittedFiles.Location = new System.Drawing.Point(6, 204);
            this.btnUploadTransmittedFiles.Name = "btnUploadTransmittedFiles";
            this.btnUploadTransmittedFiles.Size = new System.Drawing.Size(206, 23);
            this.btnUploadTransmittedFiles.TabIndex = 55;
            this.btnUploadTransmittedFiles.Text = "Upload Transmitted Files";
            this.btnUploadTransmittedFiles.UseVisualStyleBackColor = true;
            this.btnUploadTransmittedFiles.Click += new System.EventHandler(this.btnUploadTransmittedFiles_Click);
            // 
            // chkOldRep
            // 
            this.chkOldRep.AutoSize = true;
            this.chkOldRep.Location = new System.Drawing.Point(15, 111);
            this.chkOldRep.Name = "chkOldRep";
            this.chkOldRep.Size = new System.Drawing.Size(87, 17);
            this.chkOldRep.TabIndex = 56;
            this.chkOldRep.Text = "Old Republic";
            this.chkOldRep.UseVisualStyleBackColor = true;
            // 
            // chkFortega
            // 
            this.chkFortega.AutoSize = true;
            this.chkFortega.Location = new System.Drawing.Point(132, 111);
            this.chkFortega.Name = "chkFortega";
            this.chkFortega.Size = new System.Drawing.Size(62, 17);
            this.chkFortega.TabIndex = 57;
            this.chkFortega.Text = "Fortega";
            this.chkFortega.UseVisualStyleBackColor = true;
            // 
            // chkOnlyAdded
            // 
            this.chkOnlyAdded.AutoSize = true;
            this.chkOnlyAdded.Location = new System.Drawing.Point(360, 8);
            this.chkOnlyAdded.Name = "chkOnlyAdded";
            this.chkOnlyAdded.Size = new System.Drawing.Size(81, 17);
            this.chkOnlyAdded.TabIndex = 58;
            this.chkOnlyAdded.Text = "Only Added";
            this.chkOnlyAdded.UseVisualStyleBackColor = true;
            this.chkOnlyAdded.CheckedChanged += new System.EventHandler(this.chkOnlyAdded_CheckedChanged);
            // 
            // MonthEnd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(748, 587);
            this.Controls.Add(this.chkOnlyAdded);
            this.Controls.Add(this.chkFortega);
            this.Controls.Add(this.chkOldRep);
            this.Controls.Add(this.btnUploadTransmittedFiles);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.chkInception);
            this.Controls.Add(this.lblProgressBar);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.lblOther);
            this.Controls.Add(this.lblDealer);
            this.Controls.Add(this.lblClaim);
            this.Controls.Add(this.lbCancel);
            this.Controls.Add(this.lblContract);
            this.Controls.Add(this.dtpEndDate);
            this.Controls.Add(this.dtpStartDate);
            this.Controls.Add(this.chkArch);
            this.Controls.Add(this.chkAMT);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnTransmit);
            this.Controls.Add(this.btnExcel);
            this.Name = "MonthEnd";
            this.Text = " Month End";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.AmTrustContractTC.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabControl3.ResumeLayout(false);
            this.tabPage8.ResumeLayout(false);
            this.tabPage9.ResumeLayout(false);
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.tabControl2.ResumeLayout(false);
            this.tabPage6.ResumeLayout(false);
            this.tabPage7.ResumeLayout(false);
            this.tabPage10.ResumeLayout(false);
            this.tabPage10.PerformLayout();
            this.tabControl4.ResumeLayout(false);
            this.tabPage11.ResumeLayout(false);
            this.tabPage12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnExcel;
        private System.Windows.Forms.Button btnTransmit;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.RichTextBox rtbRemoveContractAmTrust;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox chkAMT;
        private System.Windows.Forms.CheckBox chkArch;
        private System.Windows.Forms.DateTimePicker dtpStartDate;
        private System.Windows.Forms.DateTimePicker dtpEndDate;
        private System.Windows.Forms.Label lblContract;
        private System.Windows.Forms.Label lbCancel;
        private System.Windows.Forms.Label lblClaim;
        private System.Windows.Forms.Label lblOther;
        private System.Windows.Forms.Label lblDealer;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.RichTextBox rtbRemoveContractArch;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label lblProgressBar;
        private System.Windows.Forms.CheckBox chkInception;
        private System.Windows.Forms.RichTextBox rtbAddClaimsAmTrust;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.RichTextBox rtbRemoveClaimsAmTrust;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.RichTextBox rtbAddClaimsArch;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.RichTextBox rtbRemoveClaimsArch;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnUploadTransmittedFiles;
        private System.Windows.Forms.Button btnSaveContractsAmtrust;
        private System.Windows.Forms.Button btnResetContractsAmTrust;
        private System.Windows.Forms.Button btnSaveClaimsAmtrust;
        private System.Windows.Forms.Button btnResetClaimsAmTrust;
        private System.Windows.Forms.TabControl AmTrustContractTC;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.RichTextBox rtbContractsPaidAddAmTrust;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.RichTextBox rtbCancelledContractsAddAmTrust;
        private System.Windows.Forms.TabControl tabControl3;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.RichTextBox rtbPaidContractsArch;
        private System.Windows.Forms.TabPage tabPage9;
        private System.Windows.Forms.RichTextBox rtbCancelledContractsArch;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.RichTextBox rtbContractPaidAddOldRep;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.RichTextBox rtbContractCancelledAddOldRep;
        private System.Windows.Forms.Button btnSaveContractsOldRep;
        private System.Windows.Forms.Button btnResetContractsOldRep;
        private System.Windows.Forms.Button btnSaveClaimsOldRep;
        private System.Windows.Forms.Button btnResetClaimsOldRep;
        private System.Windows.Forms.RichTextBox rtbClaimAddOldRep;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RichTextBox rtbClaimRemoveOldRep;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.RichTextBox rtbContractRemoveOldRep;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button btnSaveContractsArch;
        private System.Windows.Forms.Button btnResetContractArch;
        private System.Windows.Forms.Button btnClaimsSaveArch;
        private System.Windows.Forms.Button btnClaimsResetArch;
        private System.Windows.Forms.CheckBox chkOldRep;
        private System.Windows.Forms.TabPage tabPage10;
        private System.Windows.Forms.CheckBox chkFortega;
        private System.Windows.Forms.TabControl tabControl4;
        private System.Windows.Forms.TabPage tabPage11;
        private System.Windows.Forms.RichTextBox rtbPaidContractsFortega;
        private System.Windows.Forms.TabPage tabPage12;
        private System.Windows.Forms.RichTextBox rtbCancelContractsFortega;
        private System.Windows.Forms.Button btnContractSaveFortega;
        private System.Windows.Forms.Button btnResetContractsFortega;
        private System.Windows.Forms.Button btnClaimSaveFortega;
        private System.Windows.Forms.Button btnResetClaimsFortega;
        private System.Windows.Forms.RichTextBox rtbClaimsAddFortega;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.RichTextBox rtbClaimsRemoveFortega;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.RichTextBox rtbContractsRemoveFortega;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.CheckBox chkOnlyAdded;
    }
}

