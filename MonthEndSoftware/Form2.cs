﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MonthEndSoftware
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }
        public DateTime selectedDate { get; set; }

        private void btnTransmiteDate_Click(object sender, EventArgs e)
        {
            selectedDate = dtpTransmitDate.Value;
        }
    }
}
