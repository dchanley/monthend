﻿namespace MonthEndSoftware
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.dtpTransmitDate = new System.Windows.Forms.DateTimePicker();
            this.btnTransmiteDate = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(185, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "What Date Was the File Transmitted?";
            // 
            // dtpTransmitDate
            // 
            this.dtpTransmitDate.Location = new System.Drawing.Point(15, 34);
            this.dtpTransmitDate.Name = "dtpTransmitDate";
            this.dtpTransmitDate.Size = new System.Drawing.Size(200, 20);
            this.dtpTransmitDate.TabIndex = 38;
            // 
            // btnTransmiteDate
            // 
            this.btnTransmiteDate.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnTransmiteDate.Location = new System.Drawing.Point(69, 60);
            this.btnTransmiteDate.Name = "btnTransmiteDate";
            this.btnTransmiteDate.Size = new System.Drawing.Size(75, 23);
            this.btnTransmiteDate.TabIndex = 39;
            this.btnTransmiteDate.Text = "Accept";
            this.btnTransmiteDate.UseVisualStyleBackColor = true;
            this.btnTransmiteDate.Click += new System.EventHandler(this.btnTransmiteDate_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(230, 105);
            this.Controls.Add(this.btnTransmiteDate);
            this.Controls.Add(this.dtpTransmitDate);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form2";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtpTransmitDate;
        private System.Windows.Forms.Button btnTransmiteDate;
    }
}